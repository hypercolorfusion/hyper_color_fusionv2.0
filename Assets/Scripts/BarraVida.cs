using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class BarraVida : MonoBehaviour
{
    public Canvas canvas;
    public RectTransform barra;
    public Image fill;

    public Enemigo padre;
    private Vector2 offset;

    private int vidaMax;
    private int vidaActual;

    // Start is called before the first frame update

    void Start()
    {
        canvas.worldCamera = ControladorJuego.adminJuego.ObtenerCamara();
    }
    void Update()
    {
        if (padre)
            transform.position = new Vector3(padre.transform.position.x + offset.x, padre.transform.position.y + offset.y, 0f);
    }

    public void Inicializador(Vector2 _offSet, Enemigo _enemigo, float escala)
    {
        gameObject.SetActive(true);
        offset = _offSet;
        padre = _enemigo;
        padre.SetearBarraVida(this);
        RectTransform _fondoRect = barra.parent.gameObject.GetComponent<RectTransform>();
        _fondoRect.localScale = new Vector3(0.25f * escala, _fondoRect.localScale.y, _fondoRect.localScale.z);
    }

    // Update is called once per frame

    public void ActualizarColor()
    {
        fill.color = ControladorJuego.sistemaMagiaColor.ObtenerColor(padre.magiaColor);

    }
    public void ActualizarBarraVida()
    {
        fill.fillAmount = padre.resistencia / (float)padre.resistenciaMax;
    }

    public void ForzarDestruccion()
    {
        gameObject.SetActive(false);
        transform.position = Vector3.one * 10000f;
    }
}
