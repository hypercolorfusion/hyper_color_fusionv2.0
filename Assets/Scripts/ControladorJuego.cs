﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ControladorJuego
{
    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    public static Jugador jugadorAcceso;
    public static Transform posFeedBackJugador;
    public static bool mostrarMira;
    public static bool puedeDisparar = false;
    public static bool puedeCambariColorOpuesto = false;

    public static bool movimientoEspecial = false;

    public static bool magiaRoja, magiaAzul, magiaAmarilla, magiaMorada, magiaNaranja, magiaVerde, magiaNegra;
    public static int nivelBlanco, nivelRojo, nivelAzul, nivelAmarillo, nivelMorado, nivelNaranja, nivelVerde, nivelNegro = 1;

    public static float defensaBase = 2;
    public static int ataqueBase = 2;
    public static float speedBase = 1;
    public static float fireRateBase = 0.4f;
    public static int resistenciaBase = 10;

    public static bool tutorial = true;

    public static Vector3 posCheckPoint;
    public static Vector2 areaIDActual;

    #region 
    public static AdministradorJuego adminJuego;
    public static AdministradorUI adminUi;
    public static AdministradorData adminData;
    #endregion


    #region Sistemas Juego
    public static SistemaPool sistemaPool;
    public static SistemaArea sistemaArea;
    public static SistemaOleadas sistemaOleadas;
    public static SistemaExperiencia sistemaExperiencia;
    public static SistemaMagiaColor sistemaMagiaColor;
    public static SistemaMecanismosOculto sistemaMecanismosOculto;
    public static SistemaMisiones sistemaMisiones;

    #endregion

    #region Sistemas UI
    public static SistemaMapa sistemaMapa;
    public static SistemaMensajes sistemaMensajes;
    #endregion

    #region Sistemas DATA
    public static SistemaGuardado sistemaGuardado;
    #endregion
}