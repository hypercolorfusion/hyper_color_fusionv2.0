using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jugador : Unidad
{
    #region Input Teclas
    [HideInInspector]
    public List<int> ultimaTeclaX;
    [HideInInspector]
    public List<int> ultimaTeclaY;
    #endregion
    public Transform feedBackPos;

    #region Dash
    [Header("Dash/Dodge")]
    public float dashDistance;
    public float dodgeDistance;
    public float dashCooldown;
    public float friccion = 0.05f;
    Vector3 moveDir;
    [HideInInspector]
    private bool canUseDash;

    private bool canDash;
    private bool onDash;


    private float dashTimer;

    #endregion

    #region Variables Color
    [HideInInspector]
    public bool rojo, azul, amarillo = false;
    [HideInInspector]
    public int ID_magiaColor; // Publico par5a que el UI Manager lo jale
    [HideInInspector]
    public List<GameObject> balasDefensivasGeneradas;
    public float tempBalaDefensiva;
    [HideInInspector]
    public List<GameObject> anilloApagado;


    #endregion

    #region TestingVariables

    #endregion

    #region Variables Movimiento/Acciones
    [HideInInspector]
    public int x, y; // Para Movimiento del Player
    [HideInInspector]
    public bool enMovimiento;
    [Header("Enlazables")]
    public Transform mouse;
    public Transform eje, mira;
    public MiraJugador miraJugador;
    public SpriteRenderer miSprite;
    [Header("Teclas")]
    public KeyCode teclaRoja;
    public KeyCode teclaAzul;
    public KeyCode teclaAmarilla;
    [Header("Estado")]
    public bool puedeInteractuar;
    public bool inmune;

    //[Header("Activadores")]

    private int contBalaEspecial;
    [HideInInspector]
    public Portal portalActual;


    [Header("Chequeo de Estado")]
    public bool respawneando;

    [Header("Prueba")]
    public bool prueba;



    #endregion

    void Awake()
    {
        ControladorJuego.adminJuego.SetearJugador(this);
        puedeInteractuar = true;
        respawneando = false;

        ControladorJuego.sistemaExperiencia.ResetearNivelesMagia();

        if (ControladorJuego.sistemaGuardado.VerificarSlot(ControladorJuego.sistemaGuardado.slot))
            ControladorJuego.sistemaGuardado.CargarJuego();

    }

    void Start()
    {
        InicializarJugador();
    }

    public void InicializarJugador()
    {
        StopAllCoroutines();
        canDash = true;
        ControladorJuego.jugadorAcceso.miSprite.color = ControladorJuego.sistemaMagiaColor.ObtenerColor(ControladorJuego.jugadorAcceso.magiaColor);

        StartCoroutine(VelocidadDisparo());
        //ControladorJuego.adminData.SaveTransform(this.transform);
        ControladorJuego.adminUi.AjustarResistencia();

        //esto solo se activa con "Nuevo Juego"
        if (ControladorJuego.sistemaMisiones.tutorial && !ControladorJuego.sistemaGuardado.cargado)
        {
            ControladorJuego.sistemaExperiencia.GanarExperiencia(9000);
            ControladorJuego.sistemaMisiones.IniciarMisionTutorial("CAMINAR");
            ControladorJuego.sistemaMisiones.IniciarMisionTutorial("USAR_PORTAL");
            ControladorJuego.adminJuego.SetearDisparo(false);
            ControladorJuego.adminJuego.SetearMovimiemintoEspecial(false);
            ControladorJuego.adminJuego.SetearCambioColorOpuesto(false);
            ControladorJuego.sistemaMecanismosOculto.InicializarMecanismos(); //Actualizar sistemas antes de inicializarlo
            Debug.Log("Se cuentra en tutorial");
        }

        ControladorJuego.sistemaExperiencia.VerificarAtributosJugador();
        ControladorJuego.adminUi.MostrarColorPlayer(rojo, azul, amarillo);
        ControladorJuego.sistemaMapa.ActualizarMapa();
        ControladorJuego.sistemaMapa.ForzarGenerarConeccion();
        ControladorJuego.sistemaMapa.ActualizarMiniMapa(magiaColor, ControladorJuego.sistemaArea.ObtenerID_AreaJugadorActual());


        if (prueba)
        {
            ControladorJuego.sistemaMagiaColor.DesbloquarMagiaAbsoluta();
            ControladorJuego.adminJuego.SetearCambioColorOpuesto(true);
            ControladorJuego.adminJuego.SetearDisparo(true);
            ControladorJuego.puedeDisparar = true;
            ControladorJuego.adminJuego.SetearMovimiemintoEspecial(true);

            ControladorJuego.sistemaExperiencia.SubirNivelBlanco();
            ControladorJuego.sistemaExperiencia.SubirNivelBlanco();

            ControladorJuego.sistemaExperiencia.SubirNivelRojo();
            ControladorJuego.sistemaExperiencia.SubirNivelRojo();

            ControladorJuego.sistemaExperiencia.SubirNivelAzul();
            ControladorJuego.sistemaExperiencia.SubirNivelAzul();

            ControladorJuego.sistemaExperiencia.SubirNivelAmarillo();
            ControladorJuego.sistemaExperiencia.SubirNivelAmarillo();

            ControladorJuego.sistemaExperiencia.SubirNivelMorado();
            ControladorJuego.sistemaExperiencia.SubirNivelMorado();

            ControladorJuego.sistemaExperiencia.SubirNivelNaranja();
            ControladorJuego.sistemaExperiencia.SubirNivelNaranja();

            ControladorJuego.sistemaExperiencia.SubirNivelVerde();
            ControladorJuego.sistemaExperiencia.SubirNivelVerde();

            ControladorJuego.sistemaExperiencia.SubirNivelNegro();
        }
    }

    void Update()
    {
        #region Permisos para usar durante Pausa
        if (!ControladorJuego.adminUi.pausaPorMapa && !ControladorJuego.adminUi.pausaPorMensaje) Pausa();

        if (ControladorJuego.adminJuego.juegoPausado)
        {
            if (ControladorJuego.adminUi.pausaPorMapa) ControladorJuego.sistemaMapa.MapaZoom(Input.mouseScrollDelta.y);
            if (Input.GetKeyDown(KeyCode.E) && ControladorJuego.adminUi.pausaPorMensaje) ControladorJuego.sistemaMensajes.SiguienteMensaje();
            if (Input.GetKeyDown(KeyCode.Escape)) ControladorJuego.adminUi.OcultarPaneles();
            return;
        }
        #endregion

        if (Input.GetKeyDown(KeyCode.M)) ControladorJuego.sistemaMapa.EsconderMapa();

        if (puedeInteractuar)
        {
            //BORRAR ESTO LUEGO TERMINAR EL JUEGO
            if (Input.GetKeyDown(KeyCode.L)) ControladorJuego.sistemaGuardado.CargarJuego();

            if (Input.GetKeyDown(KeyCode.Q) && ControladorJuego.puedeCambariColorOpuesto) CambiarColorOpuesto();
            if (Input.GetKeyDown(KeyCode.K)) ControladorJuego.sistemaGuardado.BorrarTodosLosDatos();

            if (prueba)
            {
                if (Input.GetKeyDown(KeyCode.Keypad1)) ControladorJuego.sistemaExperiencia.SubirNivelMorado();


            }

            #region Misiones de Tutorial
            if (ControladorJuego.sistemaMisiones.tutorial)
            {
                if (ControladorJuego.sistemaMisiones.VerificarMisionInicializadaTutorial("CAMINAR") &&
                 (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D)))
                {
                    Debug.Log("Ya puedes moverte :D");
                    ControladorJuego.sistemaMisiones.CompletarMisionTutorial("CAMINAR");
                }

                if (ControladorJuego.sistemaMisiones.VerificarMisionInicializadaTutorial("USAR_PORTAL") && portalActual && Input.GetKeyDown(KeyCode.E))
                {
                    Debug.Log("Usaste tu primer portal :D");
                    ControladorJuego.sistemaMisiones.CompletarMisionTutorial("USAR_PORTAL");
                }

                if (ControladorJuego.sistemaMisiones.VerificarMisionInicializadaTutorial("DISPARAR") && !Input.GetKey(KeyCode.Mouse1))
                {
                    Debug.Log("Ya puedes disparar :D");
                    ControladorJuego.sistemaMisiones.CompletarMisionTutorial("DISPARAR");
                    ControladorJuego.adminJuego.SetearDisparo(true);
                }

                if (ControladorJuego.sistemaMisiones.VerificarMisionInicializadaTutorial("TECLA_Q") && Input.GetKeyDown(KeyCode.Q))
                {
                    ControladorJuego.sistemaMisiones.CompletarMisionTutorial("TECLA_Q");
                }

                if (magiaColor == MagiaColor.morado && ControladorJuego.sistemaMisiones.VerificarMisionInicializadaTutorial("CAMBIO_COLOR_MORADO"))
                {
                    ControladorJuego.sistemaMisiones.CompletarMisionTutorial("CAMBIO_COLOR_MORADO");
                }

            }
            #endregion

            //USAR PORTAL
            if (portalActual && Input.GetKeyDown(KeyCode.E))
            {
                if (portalActual.UsarPortal())
                {
                    ControladorJuego.sistemaArea.
                    SetearID_AreaJugadorActual(new Vector2(portalActual.destino.parent.position.x, portalActual.destino.parent.position.y));
                    if (!ControladorJuego.sistemaArea.ObtenerAreaSegunIDJugador().unicaVez)
                        ControladorJuego.sistemaOleadas.ActivarOleada(portalActual);
                }
            }

            #region Movimiento del Jugador
            Movimiento();
            //Solo para testear
            if (ultimaTeclaX.Count == 0 && ultimaTeclaY.Count == 0) enMovimiento = false;
            else enMovimiento = true;

            MovimientoSecundario();

            LookAt2D(mouse, ref eje);
            #endregion

            #region Ataque del Jugador

            if (Input.GetKeyDown(teclaRoja) && ControladorJuego.magiaRoja) PresionarColor(ref rojo);
            if (Input.GetKeyDown(teclaAzul) && ControladorJuego.magiaAzul) PresionarColor(ref azul);
            if (Input.GetKeyDown(teclaAmarilla) && ControladorJuego.magiaAmarilla) PresionarColor(ref amarillo);

            #endregion
        }
        ControladorJuego.adminUi.AjustarResistencia(); //Puede ser pasado a solo cuando colisione

    }

    #region Uso de Colores

    public void PresionarColor(ref bool _color)
    {
        _color = !_color;
        ID_magiaColor = 0;

        if (rojo) ID_magiaColor += 3;
        if (azul) ID_magiaColor += 4;
        if (amarillo) ID_magiaColor += 5;

        switch (ID_magiaColor)
        {
            case 0:
                magiaColor = MagiaColor.blanco;
                break;
            case 3:
                if (ControladorJuego.magiaRoja)
                {
                    magiaColor = MagiaColor.rojo;
                    if (ControladorJuego.tutorial &&
                     ControladorJuego.sistemaMisiones.VerificarMisionInicializadaTutorial("CAMBIO_COLOR_ROJO"))
                    {
                        ControladorJuego.sistemaMisiones.CompletarMisionTutorial("CAMBIO_COLOR_ROJO");
                        ControladorJuego.sistemaExperiencia.SubirNivelRojo();
                    }

                }
                else
                {
                    rojo = false;
                    magiaColor = MagiaColor.blanco;
                }
                break;
            case 4:
                if (ControladorJuego.magiaAzul)
                    magiaColor = MagiaColor.azul;
                else
                {
                    azul = false;
                    magiaColor = MagiaColor.blanco;
                }
                break;
            case 5:
                if (ControladorJuego.magiaAmarilla)
                    magiaColor = MagiaColor.amarillo;
                else
                {
                    amarillo = false;
                    magiaColor = MagiaColor.blanco;
                }
                break;
            case 7:
                if (ControladorJuego.magiaMorada)
                    magiaColor = MagiaColor.morado;
                else
                {
                    if (ControladorJuego.sistemaMisiones.tutorial)
                        ControladorJuego.sistemaMensajes.MostarTexto("ERRORES", "COMBINACION_COLOR", "AYUDA", true);
                    rojo = azul = false;
                    magiaColor = MagiaColor.blanco;
                }
                break;
            case 8:
                if (ControladorJuego.magiaNaranja)
                    magiaColor = MagiaColor.naranja;
                else
                {
                    if (ControladorJuego.sistemaMisiones.tutorial)
                        ControladorJuego.sistemaMensajes.MostarTexto("ERRORES", "COMBINACION_COLOR", "AYUDA", true);
                    rojo = amarillo = false;
                    magiaColor = MagiaColor.blanco;
                }
                break;
            case 9:
                if (ControladorJuego.magiaVerde)
                    magiaColor = MagiaColor.verde;
                else
                {
                    if (ControladorJuego.sistemaMisiones.tutorial)
                        ControladorJuego.sistemaMensajes.MostarTexto("ERRORES", "COMBINACION_COLOR", "AYUDA", true);
                    azul = amarillo = false;
                    magiaColor = MagiaColor.blanco;
                }
                break;
            case 12:
                if (ControladorJuego.magiaNegra)
                {
                    magiaColor = MagiaColor.negro;
                    if (ControladorJuego.sistemaMisiones.tutorial &&
                    ControladorJuego.sistemaMisiones.VerificarMisionInicializadaTutorial("CAMBIO_COLOR_NEGRO"))
                        ControladorJuego.sistemaMisiones.CompletarMisionTutorial("CAMBIO_COLOR_NEGRO");
                }
                else
                {
                    if (ControladorJuego.sistemaMisiones.tutorial)
                        ControladorJuego.sistemaMensajes.MostarTexto("ERRORES", "COMBINACION_COLOR", "AYUDA", true);
                    rojo = azul = amarillo = false;
                    magiaColor = MagiaColor.blanco;
                }
                break;
            default:
                Debug.Log("wtf");
                break;
        }
        VerificarEstadosPorColor();
        ControladorJuego.adminUi.MostrarColorPlayer(rojo, azul, amarillo);
        miSprite.color = ControladorJuego.sistemaMagiaColor.ObtenerColor(magiaColor);

        foreach (GameObject _bala in balasDefensivasGeneradas) if (!_bala.activeSelf) anilloApagado.Add(_bala);
        foreach (GameObject _bala in anilloApagado) balasDefensivasGeneradas.Remove(_bala);
        anilloApagado.Clear();
        /*
        foreach (GameObject _bala in anilloMoradoB) if (!_bala.activeSelf) anilloApagado.Add(_bala);
        foreach (GameObject _bala in anilloApagado) anilloMoradoB.Remove(_bala);
        anilloApagado.Clear();
        foreach (GameObject _bala in anilloMoradoC) if (!_bala.activeSelf) anilloApagado.Add(_bala);
        foreach (GameObject _bala in anilloApagado) anilloMoradoC.Remove(_bala);
        anilloApagado.Clear();
        */
    }

    public void CambiarColorOpuesto()
    {
        MagiaColor _guardarMagiaColor = magiaColor;
        magiaColor = ControladorJuego.sistemaMagiaColor.ObtenerMagiaColorOpuesto(magiaColor);

        switch (magiaColor)
        {

            case MagiaColor.blanco: rojo = azul = amarillo = false; break;
            case MagiaColor.rojo: rojo = true; azul = amarillo = false; break;
            case MagiaColor.azul: rojo = amarillo = false; azul = true; break;
            case MagiaColor.amarillo: rojo = azul = false; amarillo = true; break;
            case MagiaColor.morado:
                if (!ControladorJuego.magiaMorada)
                {
                    magiaColor = _guardarMagiaColor;
                    ControladorJuego.sistemaMensajes.MostarTexto("ERRORES", "COMBINACION_COLOR_OPUESTO", "AYUDA", true);
                }
                else
                {
                    rojo = azul = true;
                    amarillo = false;
                }
                break;
            case MagiaColor.naranja:
                if (!ControladorJuego.magiaNaranja)
                {
                    magiaColor = _guardarMagiaColor;
                    ControladorJuego.sistemaMensajes.MostarTexto("ERRORES", "COMBINACION_COLOR_OPUESTO", "AYUDA", true);
                }
                else
                {
                    rojo = amarillo = true;
                    azul = false;
                }
                break;
            case MagiaColor.verde:
                if (!ControladorJuego.magiaVerde)
                {
                    magiaColor = _guardarMagiaColor;
                    ControladorJuego.sistemaMensajes.MostarTexto("ERRORES", "COMBINACION_COLOR_OPUESTO", "AYUDA", true);
                }
                else
                {
                    amarillo = azul = true;
                    rojo = false;
                }
                break;
            case MagiaColor.negro:
                if (!ControladorJuego.magiaNegra)
                {
                    magiaColor = _guardarMagiaColor;
                    ControladorJuego.sistemaMensajes.MostarTexto("ERRORES", "COMBINACION_COLOR_OPUESTO", "AYUDA", true);
                }
                else
                {
                    rojo = azul = amarillo = true;
                }
                break;
        }

        VerificarEstadosPorColor();
        ControladorJuego.adminUi.MostrarColorPlayer(rojo, azul, amarillo);
        miSprite.color = ControladorJuego.sistemaMagiaColor.ObtenerColor(magiaColor);
    }

    public void SetearMagiasActivadas(bool _rojo, bool _azul, bool _amarillo)
    {
        rojo = _rojo;
        azul = _azul;
        amarillo = _amarillo;
        ControladorJuego.adminUi.MostrarColorPlayer(rojo, azul, amarillo);
    }

    public void VerificarEstadosPorColor()
    {
        VerificarFireRate();
        VerificarDefenza();
    }

    private void VerificarFireRate()
    {
        switch (magiaColor)
        {
            case MagiaColor.blanco: fireRate = ControladorJuego.fireRateBase * 1.2f; break;
            case MagiaColor.rojo: fireRate = ControladorJuego.fireRateBase * (1f - (ControladorJuego.nivelRojo * 0.05f)); break;
            case MagiaColor.azul: fireRate = ControladorJuego.fireRateBase * 1.3f; break;
            case MagiaColor.amarillo: fireRate = ControladorJuego.fireRateBase * 2f * (1f - (ControladorJuego.nivelAmarillo * 0.15f)); break;
            case MagiaColor.morado: fireRate = ControladorJuego.fireRateBase; break;
            case MagiaColor.naranja: fireRate = ControladorJuego.fireRateBase * 0.5f; break;
            case MagiaColor.verde: fireRate = ControladorJuego.fireRateBase * (1.5f - (ControladorJuego.nivelVerde * 0.1f)); break;
            case MagiaColor.negro: fireRate = ControladorJuego.fireRateBase * 0; break;
            default:
                Debug.Log("wtf");
                break;
        }
    }

    private void VerificarDefenza()
    {
        switch (magiaColor)
        {
            case MagiaColor.blanco: defensa = ControladorJuego.defensaBase; break;
            case MagiaColor.rojo: defensa = ControladorJuego.defensaBase * 0.8f; break;
            case MagiaColor.azul: defensa = ControladorJuego.defensaBase * 1.1f; break;
            case MagiaColor.amarillo: defensa = ControladorJuego.defensaBase * 0.5f; break;
            case MagiaColor.morado: defensa = ControladorJuego.defensaBase * 1.5f; break;
            case MagiaColor.naranja: defensa = ControladorJuego.defensaBase; break;
            case MagiaColor.verde: defensa = ControladorJuego.defensaBase * 0.7f; break;
            case MagiaColor.negro: defensa = ControladorJuego.defensaBase * 0.9f; break;
            default:
                Debug.Log("wtf");
                break;
        }
    }

    #endregion

    #region Colisiones Jugador
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.transform.tag == "Enemigo")
        {
            Enemigo _enemigo = other.gameObject.GetComponent<Enemigo>();

            int _valorAtaque = ControladorJuego.sistemaMagiaColor
                .CalculoAtaqueEnemigo(_enemigo.ataque, _enemigo.magiaColor, true) * 2;

            if (!inmune)
            {
                GolpeJugador(_valorAtaque);
                GetHitJugador(_valorAtaque, _enemigo.magiaColor);
                if (resistencia <= 0) GameOver();
            }
        }
    }

    #endregion

    #region Estados Jugador

    void Pausa()
    {
        if (Input.GetKeyUp(KeyCode.P))
        {
            if (ControladorJuego.adminJuego.juegoPausado) ControladorJuego.adminJuego.ResumeGame();
            else ControladorJuego.adminJuego.PauseGame();
        }
    }

    public void GameOver()
    {
        //Colocar animacion de muerte y cargar pantalla de GameOver o Menu?
        //Destroy (gameObject); // Desactivado por a hora por checkpoints
        StartCoroutine(ProhibirDisparo());
        //ControladorJuego.sistemaGuardado.CargarPlayerPosition();
        ControladorJuego.jugadorAcceso.resistencia = 50;
        respawneando = true;
        Debug.Log("Moriste por gil");
    }

    IEnumerator ProhibirDisparo()
    {
        ControladorJuego.adminJuego.SetearDisparo(false);
        yield return new WaitForSeconds(2f);
        ControladorJuego.adminJuego.SetearDisparo(true);
    }

    #endregion

    #region Movimiento

    void Movimiento()
    {
        if (onDash) return;
        /*
        if (magiaColor == MagiaColor.naranja && Input.GetMouseButton(0))
        {
            LimpiarMovmiento();
            return;
        }
        */

        #region Input Con Botones
        if (Input.GetKeyDown(KeyCode.W) || Input.GetKey(KeyCode.W))
            if (ultimaTeclaY.Count == 0) ultimaTeclaY.Add(1);

        if (Input.GetKeyDown(KeyCode.S) || Input.GetKey(KeyCode.S))
            if (ultimaTeclaY.Count == 0) ultimaTeclaY.Add(-1);

        if (Input.GetKeyDown(KeyCode.D) || Input.GetKey(KeyCode.D))
            if (ultimaTeclaX.Count == 0) ultimaTeclaX.Add(1);

        if (Input.GetKeyDown(KeyCode.A) || Input.GetKey(KeyCode.A))
            if (ultimaTeclaX.Count == 0) ultimaTeclaX.Add(-1);


        if (Input.GetKeyUp(KeyCode.W)) { ultimaTeclaY.Remove(1); }
        if (Input.GetKeyUp(KeyCode.S)) { ultimaTeclaY.Remove(-1); }
        if (Input.GetKeyUp(KeyCode.D)) { ultimaTeclaX.Remove(1); }
        if (Input.GetKeyUp(KeyCode.A)) { ultimaTeclaX.Remove(-1); }

        if (ultimaTeclaY.Count > 0) y = ultimaTeclaY[ultimaTeclaY.Count - 1]; else y = 0;
        if (ultimaTeclaX.Count > 0) x = ultimaTeclaX[ultimaTeclaX.Count - 1]; else x = 0;

        //Esto como verificacion si no se está haciendo nada y por si quieren trabar movimiendo clickeando fuera del juego
        if (!Input.anyKey)
            LimpiarMovmiento();
        #endregion

        moveDir = new Vector3(x, y, 0).normalized; // Ultima direccion para saber para donde va el dash
        transform.Translate(moveDir * Time.deltaTime * speed);
    }

    public void MovimientoSecundario()
    {
        if (magiaColor == MagiaColor.rojo || magiaColor == MagiaColor.azul || magiaColor == MagiaColor.amarillo
        || magiaColor == MagiaColor.verde || magiaColor == MagiaColor.naranja || magiaColor == MagiaColor.morado)
        {
            if (Input.GetMouseButton(1)) CalcularDash();
            if (Input.GetMouseButtonUp(1)) Dash();
        }
        else if (Input.GetMouseButtonDown(1)) NuevoDogeRoll();
    }

    public void LimpiarMovmiento()
    {
        ultimaTeclaX.Clear();
        ultimaTeclaY.Clear();
    }
    #endregion

    #region Inmunidad

    public void GolpeJugador(int _danio)
    {
        if (_danio > 0)
            StartCoroutine(InmunidadGolpe());
    }
    IEnumerator InmunidadGolpe()
    {
        StartCoroutine(ProhibirDisparo());
        inmune = true;
        SpriteRenderer _sprite = GetComponent<SpriteRenderer>();

        float _temporizador = 0f;
        while (_temporizador <= 1)
        {
            yield return new WaitForSeconds(0.1f);
            _sprite.color = new Vector4(_sprite.color.r, _sprite.color.g, _sprite.color.b, 0);
            yield return new WaitForSeconds(0.1f);
            _sprite.color = new Vector4(_sprite.color.r, _sprite.color.g, _sprite.color.b, 1);
            _temporizador += 0.2f;
        }

        inmune = false;
    }
    #endregion

    #region Dash

    public void CalcularDash()
    {
        if (canDash && ControladorJuego.movimientoEspecial) ControladorJuego.adminJuego.SetearMira(true);
    }
    public void Dash()
    {
        //Realizar una verificacion si se puede realizar el Dash por muros.
        if (!canDash) return;
        Vector3 _posible = Vector3.zero;

        foreach (Nodo _nodos in miraJugador.nodos)
        {
            canUseDash = true;
            if (_nodos.miColor == Color.red)
            {
                canUseDash = false;
                break;
            }
            _posible = _nodos.transform.position;
        }

        if (miraJugador.nodos[miraJugador.nodos.Length - 1].miColor == Color.blue)
            canUseDash = false;


        ControladorJuego.adminJuego.SetearMira(false);

        if (ControladorJuego.movimientoEspecial && canUseDash)
        {
            float _distaciaDash = dashDistance;
            transform.position = _posible;
            //ControladorJuego.adminJuego.SetearMovimiemintoEspecial(false);
            canDash = false;
            StartCoroutine(RecargaDash());
            if (ControladorJuego.sistemaMisiones.tutorial && !ControladorJuego.sistemaMisiones.VerificarMisionCompletadaTutorial("DASH")
             && ControladorJuego.sistemaMisiones.VerificarMisionInicializadaTutorial("DASH"))
            {
                Debug.Log("Aprendiste el Dash");
                ControladorJuego.sistemaMisiones.CompletarMisionTutorial("DASH");
            }
        }
    }

    IEnumerator RecargaDash()
    {
        LimpiarMovmiento();
        float _repetir = 20; //Eso para que se vea el tiempo correr en el UI sobre del dashTimer
        float _repeticion = dashCooldown / _repetir;
        dashTimer = 0;

        for (int i = 0; i < _repetir; i++)
        {
            yield return new WaitForSeconds(_repeticion);
            dashTimer += _repeticion;
            //ControladorJuego.adminUi.DisplayCooldownDash();
        }
        canDash = true;

        //ControladorJuego.adminUi.HideCooldownDash();
        //ControladorJuego.adminJuego.SetearMovimiemintoEspecial(true);
    }
    #endregion

    #region Dodge
    public void NuevoDogeRoll()
    {
        if (!canDash) return;
        if (enMovimiento && ControladorJuego.movimientoEspecial)
        {
            //Vector3 _direccion = LookAt2D (mira.position); Mirar hacia el mouse
            Vector3 _direccion = new Vector3(x, y, 0); //Mira hacia la direccion que caminas
            LimpiarMovmiento(); //Mara que resetear el X y Y a 0 y no se deslice al finalizar Dodge
            //ControladorJuego.adminJuego.SetearMovimiemintoEspecial(true);
            canDash = false;
            onDash = true;
            StartCoroutine(DuracionDodge(_direccion));
        }
    }

    IEnumerator DuracionDodge(Vector3 _direccion)
    {
        float _velocidadTemporal = dodgeDistance;
        float _exponente = 0f;

        while (_velocidadTemporal >= 0)
        {
            yield return new WaitForSeconds(0.02f);
            _exponente += friccion;
            _velocidadTemporal -= 0.1f + _exponente;
            transform.Translate(_direccion * _velocidadTemporal * 0.02f);
        }
        onDash = false;
        StartCoroutine(RecargaDogeRoll());
    }

    IEnumerator RecargaDogeRoll()
    {
        LimpiarMovmiento();
        float _repetir = 50; //Eso para que se vea el tiempo correr en el UI sobre del dodgeTimer Facil ponerlo en publico
        float _repeticion = dashCooldown / _repetir;
        dashTimer = 0;

        for (int i = 0; i < _repetir; i++)
        {
            yield return new WaitForSeconds(_repeticion);
            dashTimer += _repeticion;
            //ControladorJuego.adminUi.DisplayCooldownDodge();
        }
        canDash = true;
        //ControladorJuego.adminUi.HideCooldownDodge();
        //ControladorJuego.adminJuego.SetearMovimiemintoEspecial(true);
    }
    #endregion

    #region Disparo

    IEnumerator VelocidadDisparo()
    {
        int _cantBalas = 0;
        bool _azar = true;
        while (true)
        {
            if (magiaColor == MagiaColor.morado)
            {
                #region Habilidad Morada Pasiva
                if (magiaColor == MagiaColor.morado)
                {
                    _cantBalas = 5 + (5 * ControladorJuego.nivelMorado);
                    if (ControladorJuego.nivelMorado == 4) _cantBalas--;
                    else if (ControladorJuego.nivelMorado == 6) _cantBalas++;
                    for (int i = 0; i < _cantBalas; i++)
                    {
                        GameObject _nuevabala =
                        ControladorJuego.sistemaPool.DispararBalaMoradaJugador(transform.position, eje.rotation, 0.5f, _azar);
                        _nuevabala.transform.Rotate(Vector3.forward * (360 / _cantBalas) * i);
                        balasDefensivasGeneradas.Add(_nuevabala);
                    }

                    foreach (GameObject _bala in balasDefensivasGeneradas) if (!_bala.activeSelf) anilloApagado.Add(_bala);
                    foreach (GameObject _bala in anilloApagado) balasDefensivasGeneradas.Remove(_bala);
                    anilloApagado.Clear();
                    yield return new WaitForSeconds(tempBalaDefensiva / ((1 + ControladorJuego.nivelMorado) / 2));
                    _azar = !_azar;
                }
                #endregion
            }
            else
            {
                yield return new WaitForSeconds(fireRate);
                if (ControladorJuego.puedeDisparar && !Input.GetKey(KeyCode.Mouse1) && Input.GetKey(KeyCode.Mouse0))
                    Disparar();
            }
        }
    }

    public void Disparar()
    {
        switch (magiaColor)
        {
            //BALAS BLANCAS
            case MagiaColor.blanco:
                ControladorJuego.sistemaPool.DispararJugador(mira.position, eje.rotation);
                if (ControladorJuego.nivelBlanco >= 3)
                {
                    for (int i = 0; i < 2; i++)
                    {
                        GameObject _nuevabala = ControladorJuego.sistemaPool.DispararJugador(mira.position, eje.rotation);
                        if (i == 0) _nuevabala.transform.Rotate(Vector3.forward * 5);
                        else _nuevabala.transform.Rotate(Vector3.back * 5);
                    }
                }
                if (ControladorJuego.nivelBlanco >= 5)
                {
                    for (int i = 0; i < 2; i++)
                    {
                        GameObject _nuevabala = ControladorJuego.sistemaPool.DispararJugador(mira.position, eje.rotation);
                        if (i == 0) _nuevabala.transform.Rotate(Vector3.forward * 10);
                        else _nuevabala.transform.Rotate(Vector3.back * 10);
                    }
                }
                break;
            //BALAS ROJOS
            case MagiaColor.rojo:
                ControladorJuego.sistemaPool.DispararJugador(mira.position, eje.rotation);
                if (ControladorJuego.nivelRojo >= 6)
                {
                    for (int i = 0; i < 2; i++)
                    {
                        GameObject _nuevabala = ControladorJuego.sistemaPool.DispararJugador(mira.position, eje.rotation);
                        if (i == 0) _nuevabala.transform.Rotate(Vector3.forward * 5);
                        else _nuevabala.transform.Rotate(Vector3.back * 5);
                    }
                }
                break;
            //BALAS AZULES
            case MagiaColor.azul:
                ControladorJuego.sistemaPool.DispararJugador(mira.position, eje.rotation);
                if (ControladorJuego.nivelAzul >= 5)
                {
                    for (int i = 0; i < 2; i++)
                    {
                        GameObject _nuevabala = ControladorJuego.sistemaPool.DispararJugador(mira.position, eje.rotation);
                        if (i == 0) _nuevabala.transform.Rotate(Vector3.forward * 15);
                        else _nuevabala.transform.Rotate(Vector3.back * 15);
                    }
                }
                break;
            //BALAS Amarillos
            case MagiaColor.amarillo:
                ControladorJuego.sistemaPool.DispararJugador(mira.position, eje.rotation);
                if (ControladorJuego.nivelAmarillo >= 4)
                {
                    for (int i = 0; i < 2; i++)
                    {
                        GameObject _nuevabala = ControladorJuego.sistemaPool.DispararJugador(mira.position, eje.rotation);
                        if (i == 0) _nuevabala.transform.Rotate(Vector3.forward * 5);
                        else _nuevabala.transform.Rotate(Vector3.back * 5);
                    }
                }
                break;

            //BALA Morada ES UNA MECÁNICA DE DISPARO DIFERENTE Y AUTOMÁTICA

            //BALA Morada
            case MagiaColor.naranja:
                for (int i = 0; i < ControladorJuego.nivelNaranja; i++)
                {
                    GameObject _nuevabala = ControladorJuego.sistemaPool.DispararJugador(transform.position, eje.rotation);
                    _nuevabala.transform.Rotate(Vector3.forward * (360 / ControladorJuego.nivelNaranja) * (i + 1));
                    _nuevabala.transform.Translate(Vector3.right * i * 0.2f);
                }
                break;
            case MagiaColor.verde:
                for (int i = 0; i < ControladorJuego.nivelVerde; i++)
                    ControladorJuego.sistemaPool.DispararJugador(mira.position, eje.rotation);
                break;

            //Bala Negra

            case MagiaColor.negro:
                bool _disparado = true;
                foreach (GameObject _bala in ControladorJuego.sistemaPool.balaJugadorGrandeActiva)
                {
                    if (_bala.GetComponent<BalaJugadorGrande>().enCarga)
                    {
                        _disparado = false;
                        break;
                    }
                }
                if (_disparado)
                {
                    GameObject _nuevabala = ControladorJuego.sistemaPool.DispararJugadorBalaGrande(mira.position, eje.rotation, magiaColor);
                    _nuevabala.transform.Translate(Vector3.right);
                }

                if (ControladorJuego.nivelNegro >= 3)
                    foreach (GameObject _enemigos in ControladorJuego.sistemaOleadas.enemigosObjetivo)
                        _enemigos.GetComponent<Enemigo>().absorviendo = true;
                break;
        }

        //ControladorJuego.sistemaPool.DispararBalaPlayer(bala, mira.position, eje.rotation);
    }
    #endregion



    #region metodos de vida/daño

    public void GetHitJugador(int _valor, MagiaColor _color)
    {
        int _valorFinal = Mathf.RoundToInt(_valor / defensa);
        resistencia -= _valorFinal;
        ControladorJuego.sistemaPool.MostrarFeedBackAtaque(ControladorJuego.posFeedBackJugador.position, _color, _valorFinal, true);

    }

    public void Curar(int _valor)
    {
        resistencia += _valor;
        if (resistencia > resistenciaMax)
            resistencia = resistenciaMax;
    }

    #endregion

}