﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiraJugador : MonoBehaviour
{
    AdministradorJuego administrador; //Se necesita para evitar que el jugador apunte cuando el juego este pausado

    public bool mirarMouse;
    public Transform hijo;
    public GameObject[] direccionMira;
    public Nodo[] nodos;

    private Vector3 direccion;

    float x, y;
    float lateX, lateY;
    public List<Enemigo> enemigos;
    private Vector3 dirAbsorcion;
    public CircleCollider2D colision;

    void Start()
    {
        administrador = ControladorJuego.adminJuego;
        colision = GetComponent<CircleCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (administrador.juegoPausado)
            return;

        if (mirarMouse)
        {
            transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 1));
            hijo.position = ControladorJuego.jugadorAcceso.transform.position;
            ControladorJuego.jugadorAcceso.LookAt2D(transform, ref hijo);
            for (int i = 0; i < direccionMira.Length; i++)
            {
                direccionMira[i].transform.position = hijo.position;
                direccionMira[i].transform.Translate(Vector3.right * (i + 1) * (ControladorJuego.jugadorAcceso.dashDistance / direccionMira.Length));
            }
        }
        else
        {
            hijo.position = ControladorJuego.jugadorAcceso.transform.position;
            ControladorJuego.jugadorAcceso.LookAt2D(transform, ref hijo);
            x = ControladorJuego.jugadorAcceso.x;
            y = ControladorJuego.jugadorAcceso.y;
            if (x != 0 || y != 0)
            {
                lateX = x;
                lateY = y;
                for (int i = 0; i < direccionMira.Length; i++)
                {
                    direccionMira[i].transform.position = hijo.position;
                    direccionMira[i].transform.Translate(new Vector3(lateX, lateY, 0f) * (i + 1) * (ControladorJuego.jugadorAcceso.dashDistance / direccionMira.Length));
                }
            }
        }

        if (ControladorJuego.jugadorAcceso.magiaColor == MagiaColor.morado && ControladorJuego.nivelMorado >= 2)
        {
            if (enemigos.Count >= 1 && Input.GetMouseButton(0))
            {
                foreach (Enemigo _acceso in enemigos)
                {
                    _acceso.absorviendo = true;
                    if (ControladorJuego.nivelMorado >= 6)
                    {
                        _acceso.Paralizar();
                    }
                }
            }
            else
            {
                foreach (Enemigo _acceso in enemigos)
                {
                    _acceso.absorviendo = false;
                }
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D _other)
    {
        if (_other.transform.tag == "Enemigo")
        {
            enemigos.Add(_other.GetComponent<Enemigo>());
        }
        colision.radius = 0.1f * ControladorJuego.nivelMorado;
    }

    private void OnTriggerExit2D(Collider2D _other)
    {
        if (_other.transform.tag == "Enemigo")
        {
            enemigos.Remove(_other.GetComponent<Enemigo>());
            _other.GetComponent<Enemigo>().absorviendo = false;
        }
    }
}