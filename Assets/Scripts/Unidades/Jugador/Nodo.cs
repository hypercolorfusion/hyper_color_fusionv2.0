﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nodo : MonoBehaviour
{

    SpriteRenderer yo;
    public Color miColor;

    void Start()
    {
        miColor = Color.green;
        yo = GetComponent<SpriteRenderer>();
        yo.color = miColor;
    }

    void Update()
    {
        yo.enabled = ControladorJuego.mostrarMira;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Pared")
        {
            miColor = Color.red;
            yo.color = miColor;
        }
        else if (other.tag == "Hueco")
        {
            miColor = Color.blue;
            yo.color = miColor;
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "Pared")
        {
            miColor = Color.red;
            yo.color = miColor;
        }
        else if (other.tag == "Hueco")
        {
            miColor = Color.blue;
            yo.color = miColor;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Pared" || other.tag == "Hueco")
        {
            miColor = Color.green;
            yo.color = miColor;
        }
    }
}