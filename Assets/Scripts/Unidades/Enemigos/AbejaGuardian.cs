using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbejaGuardian : Enemigo
{
    [Header("AbejaGuardian")]
    public int cantBalas;
    private bool cercano = false;
    // Start is called before the first frame update
    void Start()
    {
        InstancearBarraVida();
        ActualizarColor();
        SetCanMove(true);
        SetLookAt(true);
        ActualizarStats(true);
        StartCoroutine(DisparoClasicoRandom(cantBalas));
        cantBalas += nivel;
    }

    // Update is called once per frame
    void Update()
    {
        if (!cercano)
        {
            if (ControladorJuego.adminJuego.DistanciaJugador(transform.position) >= 8) cercano = true;
        }
        else
        {
            if (ControladorJuego.adminJuego.DistanciaJugador(transform.position) <= 5) cercano = false;
        }
        MovimientoInteligente(cercano);
    }
}
