using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Escudo : MonoBehaviour
{
    public Vector3 colisionBalaPos;
    public SpriteRenderer miSprite;
    public MagiaColor magiaColor;
    public bool accionar = false;

    public void ColisionBala(Transform _bala)
    {
        colisionBalaPos = _bala.position;
        accionar = true;
    }

    public void ActualizarColor()
    {
        magiaColor = ControladorJuego.sistemaMagiaColor.ObtenerMagiaColorOpuestoJugador();
        miSprite.color = ControladorJuego.sistemaMagiaColor.ObtenerColor(magiaColor);
    }
}
