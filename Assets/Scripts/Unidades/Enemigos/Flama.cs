using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flama : Enemigo
{
    [Header("Flama")]
    public int cantBalasRadial;
    public GameObject sombra;
    public GameObject flamaBebe;
    public float tempSaltando = 3f;
    public float tempCreacion = 5f;
    public float tempfuego = 2.5f;

    private bool cercano = false;
    private bool saltando = false;
    // Start is called before the first frame update
    void Start()
    {
        InstancearBarraVida();
        ActualizarColor();
        SetCanMove(true);
        SetLookAt(true);
        ActualizarStats(false);
        StartCoroutine(Aviso());
        StartCoroutine(Fuego());
        sombra.SetActive(false);
    }

    // Update is called once per frame

    void Update()
    {
        if (!cercano)
        {
            if (ControladorJuego.adminJuego.DistanciaJugador(transform.position) >= 8) cercano = true;
        }
        else
        {
            if (ControladorJuego.adminJuego.DistanciaJugador(transform.position) <= 5) cercano = false;
        }
        MovimientoInteligente(cercano);
        MovimientoLateral();

        if (saltando) sombra.transform.position = ControladorJuego.jugadorAcceso.transform.position;

        if (muerto)
        {

            if (ControladorJuego.sistemaMisiones.tutorial &&
             !ControladorJuego.sistemaMisiones.VerificarMisionCompletadaTutorial("CAMBIO_COLOR_NARANJA") &&
             !ControladorJuego.sistemaMisiones.VerificarMisionInicializadaTutorial("CAMBIO_COLOR_NARANJA"))
            {
                ControladorJuego.sistemaMisiones.IniciarMisionTutorial("CAMBIO_COLOR_NARANJA");
                ControladorJuego.sistemaMagiaColor.DesbloquearMagiaNaranja();
                ControladorJuego.sistemaExperiencia.SubirNivelRojo();
                ControladorJuego.sistemaExperiencia.SubirNivelRojo();
                ControladorJuego.sistemaExperiencia.SubirNivelBlanco();
                ControladorJuego.sistemaMensajes.MostarTexto("TUTORIAL", "DERROTA_FLAMA", "AYUDA", true);
            }

            ControladorJuego.sistemaOleadas.enemigosObjetivo.Remove(gameObject);
            Destroy(gameObject);
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.transform.tag == "Pared" || other.transform.tag == "Hueco" || other.transform.tag == "Jugador")
        {
            StopAllCoroutines();
            StartCoroutine(Saltar());
        }
        else if (other.transform.tag == "Enemigo" && other.transform.GetComponent<FlamaBebe>())
        {
            FlamaBebe _componente = other.transform.GetComponent<FlamaBebe>();
            //Revisar si se cambió el color para curar / dañar
            _componente.GetHitEnemigo(ControladorJuego.sistemaMagiaColor.CalculoAtaqueJugador(_componente.resistenciaMax,
             _componente.magiaColor, ControladorJuego.sistemaMagiaColor.ObtenerMagiaColorOpuesto(_componente.magiaColor)),
              ControladorJuego.sistemaMagiaColor.ObtenerMagiaColorOpuesto(_componente.magiaColor));

            GetHitEnemigo(ControladorJuego.sistemaMagiaColor.CalculoAtaqueJugador(_componente.resistenciaMax,
             _componente.magiaColor, magiaColor), _componente.magiaColor);
        }
    }

    private void Criar()
    {
        GameObject _flamaBebe;
        _flamaBebe = Instantiate(flamaBebe, transform.position, eje.rotation);
        _flamaBebe.transform.name = "Flama Bebe";
        ControladorJuego.sistemaOleadas.AgregarEnemigosObjetivos(_flamaBebe);
    }

    IEnumerator EsperaSalto()
    {
        yield return new WaitForSeconds(10f);
        StartCoroutine(Saltar());

    }

    IEnumerator Fuego()
    {
        while (true)
        {
            yield return new WaitForSeconds(tempfuego);
            if (!saltando) ControladorJuego.sistemaPool.MostrarFuego(transform.position, false);
        }
    }

    IEnumerator Saltar()
    {
        sombra.transform.SetParent(null);
        sombra.SetActive(true);
        saltando = true;
        for (int i = 0; i < nivel * 3; i++) Criar();
        transform.position = Vector3.one * 10000f;
        yield return new WaitForSeconds(tempSaltando);
        StartCoroutine(Aviso());
    }

    IEnumerator Aviso()
    {
        saltando = false;
        yield return new WaitForSeconds(1f);
        transform.position = sombra.transform.position;
        DisparoRadialUnico(cantBalasRadial, 1f);
        sombra.transform.SetParent(transform);
        sombra.SetActive(false);
        StartCoroutine(EsperaSalto());
        StartCoroutine(Fuego());
    }
}
