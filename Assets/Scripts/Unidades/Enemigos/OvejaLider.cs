using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OvejaLider : Enemigo
{
    [Header("Oveja Lider")]

    public float tempCambio;
    public float tempProcreacion;

    public GameObject oveja;
    public List<Oveja> manada;
    private bool movimientoInteligente;

    // Start is called before the first frame update
    void Start()
    {
        InstancearBarraVida();
        ActualizarColor();
        SetCanMove(true);
        SetLookAt(true);
        StartCoroutine(CambioDeDireccion(tempCambio));
        StartCoroutine(Procrear(tempProcreacion));
        #region Cambios por nivel de la unidad
        nivel = Mathf.RoundToInt(ControladorJuego.sistemaExperiencia.nivelJugador / 10f);
        if (nivel == 0) nivel = 1;
        ActualizarStats(true);
        if (nivel != 1) tempProcreacion = tempProcreacion * (1 - (nivel / 5f));
        #endregion
    }


    // Update is called once per frame
    void Update()
    {
        movimientoInteligente = ControladorJuego.adminJuego.DistanciaJugador(transform.position) <= 5;
        if (movimientoInteligente) MovimientoInteligente(true);
        else MovimientoRecto();
    }

    public void Enojar(Oveja _oveja)
    {
        manada.Remove(_oveja);
        velocidadBonus += 0.05f;
        if (manada.Count <= 0)
        {
            CambiarColor(MagiaColor.blanco);
            velocidadBonus = 1;
        }
        foreach (Oveja _hijo in manada)
        {
            _hijo.Enojar();
        }
    }

    private void Criar()
    {
        GameObject _cria;
        _cria = Instantiate(oveja, transform.position, transform.rotation);
        _cria.transform.name = "Oveja";
        Oveja _componente = _cria.GetComponent<Oveja>();
        _componente.padre = this;
        _componente.nivel = nivel;
        manada.Add(_componente);
        CambiarColor(MagiaColor.negro);
        ControladorJuego.sistemaOleadas.AgregarEnemigosObjetivos(_cria);
    }

    public IEnumerator Procrear(float _tiempo)
    {
        while (true)
        {
            yield return new WaitForSeconds(_tiempo);
            Criar();
        }
    }


    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.transform.tag == "Pared" || other.transform.tag == "Hueco" || (other.transform.tag == "Jugador" && !movimientoInteligente))
        {
            if (other.transform.tag == "Pared") GiroChoquePared(other.transform.localPosition, Mathf.RoundToInt(transform.eulerAngles.z));
            else transform.Rotate(Vector3.forward * 180);
            eje.rotation = transform.rotation;
            velocidadBonus = velocidadBonus * 1.01f;
        }
    }
}
