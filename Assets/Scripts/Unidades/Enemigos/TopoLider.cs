using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopoLider : Enemigo
{
    [Header("Topo Lider")]
    public GameObject topo;
    public List<Topo> manada;
    public Transform hijosPos;
    public float tempLuto;
    public int cantHijos;
    public int cantBalasRadial;
    public GameObject sombra;
    private bool triste = false;



    // Start is called before the first frame update
    void Start()
    {
        InstancearBarraVida();
        ActualizarColor();
        SetLookAt(true);
        ActualizarStats(true);
        cantHijos = cantHijos + (nivel * 2 - 2);
        CambiarColorOpuesto();
        Procrear();
        StartCoroutine(DisparoVectorial(true, 1f, 0.5f, nivel * 3));
        sombra.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        LookAt2D(ControladorJuego.jugadorAcceso.transform.position);

        if (triste) return;

        if (getHit && resistencia > 0f) { getHit = false; Saltar(); }

        if (manada.Count <= 0) StartCoroutine(TiempoDuelo(tempLuto));
    }

    private void Procrear()
    {
        float _giro = 360 / cantHijos;
        for (int i = 0; i < cantHijos; i++)
        {
            // Instantiate
            GameObject _cria;
            _cria = Instantiate(topo, hijosPos.position, transform.rotation);
            _cria.transform.name = "Topo";
            Topo _componente = _cria.GetComponent<Topo>();
            _componente.SetearPadre(this);
            _componente.nivel = nivel;
            manada.Add(_componente);
            ControladorJuego.sistemaOleadas.AgregarEnemigosObjetivos(_cria);
            transform.Rotate(0f, 0f, _giro, Space.Self);
        }
        triste = false;
        CambiarColorOpuesto();
    }

    public void Saltar()
    {
        sombra.transform.SetParent(null);
        sombra.SetActive(true);
        transform.position = Vector3.one * 10000f;
        Vector3 _areapos = ControladorJuego.sistemaArea.ObtenerAreaSegunIDJugador().transform.position;
        sombra.transform.position = new Vector3(_areapos.x + (Random.Range(-13f, 13f)), _areapos.y + (Random.Range(-13f, 13f)), sombra.transform.position.z);
        while (ControladorJuego.adminJuego.DistanciaJugador(sombra.transform.position) <= 5)
        {
            sombra.transform.position = new Vector3(_areapos.x + (Random.Range(-13f, 13f)), _areapos.y + (Random.Range(-13f, 13f)), sombra.transform.position.z);
        }
        StartCoroutine(Aviso());
    }

    private void OnDestroy()
    {
        foreach (Topo _topitos in manada)
        {
            _topitos.Enojar();
        }
    }

    IEnumerator Aviso()
    {
        yield return new WaitForSeconds(1f);
        transform.position = sombra.transform.position;
        DisparoRadialUnico(cantBalasRadial, 1f);
        sombra.SetActive(false);
        sombra.transform.SetParent(transform);
    }

    IEnumerator TiempoDuelo(float _tiempoDuelo)
    {
        triste = true;
        CambiarColorOpuesto();
        yield return new WaitForSeconds(_tiempoDuelo);
        Procrear();
    }
}
