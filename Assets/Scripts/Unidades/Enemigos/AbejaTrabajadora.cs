using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbejaTrabajadora : Enemigo
{
    [Header("Abeja Trabajadora")]
    public float velocidadRotacion = 100f;


    // Start is called before the first frame update
    void Start()
    {
        InstancearBarraVida();
        ActualizarColor();
        SetCanMove(true);
        ActualizarStats(true);
    }

    // Update is called once per frame
    void Update()
    {
        MovimientoInteligenteSuavizado(velocidadRotacion);
    }
}
