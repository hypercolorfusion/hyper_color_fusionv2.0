using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Caballero : Enemigo
{
    [Header("Caballero")]
    public GameObject escudo;
    public float tempDisparoSecundario;
    private bool enojado = false;

    // Start is called before the first frame update
    void Start()
    {
        InstancearBarraVida();
        ActualizarColor();
        SetCanMove(true);
        SetLookAt(true);
        ActualizarStats(false);
        StartCoroutine(DisparoTeledirigido(1f, 100f));
        escudo.transform.SetParent(null);
        enojado = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (enojado)
        {
            MovimientoInteligente(true);
        }
        else
        {
            if (ControladorJuego.adminJuego.JugadorEnMovimiento())
                MovimientoInteligente(ControladorJuego.adminJuego.DistanciaJugador(transform.position) >= 7);

            if (resistencia <= resistenciaMax * 0.75f) Enojar();
        }

        if (muerto)
        {
            if (ControladorJuego.sistemaMisiones.tutorial &&
             !ControladorJuego.sistemaMisiones.VerificarMisionCompletadaTutorial("CAMBIO_COLOR_MORADO") &&
             !ControladorJuego.sistemaMisiones.VerificarMisionInicializadaTutorial("CAMBIO_COLOR_MORADO"))
            {

                ControladorJuego.sistemaMisiones.IniciarMisionTutorial("CAMBIO_COLOR_MORADO");
                ControladorJuego.sistemaMagiaColor.DesbloquearMagiaMorada();
                ControladorJuego.sistemaExperiencia.SubirNivelMorado();
                ControladorJuego.sistemaExperiencia.SubirNivelMorado();
                ControladorJuego.sistemaMensajes.MostarTexto("TUTORIAL", "MEZCLA_DE_MAGIA", "AYUDA", true);
            }
            ControladorJuego.sistemaOleadas.enemigosObjetivo.Remove(gameObject);
            Destroy(gameObject);
        }
    }

    private void Enojar()
    {
        enojado = true;
        defensa = defensa / 2;
        velocidadBonus = velocidadBonus * (1f + (nivel / 2f));
        ataque = Mathf.RoundToInt(ataque / 2f);
        fireRate = fireRate * 6f;
        StopAllCoroutines();
        StartCoroutine(DisparoRadialVectorial(36, true, velocidadBonus, 0.5f, nivel * 8));
        Destroy(escudo);
    }
}
