using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaballeroEscudo : Escudo
{
    public Caballero caballero;

    // Start is called before the first frame update
    // Update is called once per frame
    void Update()
    {
        if (!caballero) return;
        transform.position = caballero.transform.position;
        transform.rotation = caballero.eje.rotation;
        magiaColor = ControladorJuego.jugadorAcceso.magiaColor;
        ActualizarColor();
        if (accionar)
        {
            accionar = false;
            ControladorJuego.sistemaPool.DispararBalaEnemiga(colisionBalaPos, caballero.eje.rotation, magiaColor, caballero.ataque, 2f);
        }
    }
}
