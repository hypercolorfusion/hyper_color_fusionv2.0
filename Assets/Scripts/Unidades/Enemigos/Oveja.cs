using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Oveja : Enemigo
{
    [Header("Oveja")]
    public bool enojado;
    public float tempCambio;
    public OvejaLider padre;
    // Start is called before the first frame update
    void Start()
    {
        InstancearBarraVida();
        ActualizarColor();
        SetCanMove(true);
        StartCoroutine(CambioDeDireccion(tempCambio));
        ActualizarStats(false);
    }


    // Update is called once per frame
    void Update()
    {
        if (muerto)//No se usa OnDestroy por que manda error al cerrar el juego durante el Play y ejecutable
        {
            padre.Enojar(this);
            ControladorJuego.sistemaOleadas.enemigosObjetivo.Remove(gameObject);
            Destroy(gameObject);
        }
        MovimientoRecto();
    }

    public void Enojar()
    {
        if (enojado) return;

        enojado = true;
        StopAllCoroutines();
        if (gameObject) //evitar error de cerrar el juego 
            StartCoroutine(Apuntar());
    }

    IEnumerator Apuntar()
    {
        velocidadBonus = 0;
        yield return new WaitForSeconds(Random.Range(0.5f, 1.5f));
        LookAt2D(ControladorJuego.jugadorAcceso.transform.position);
        velocidadBonus = 2f;
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (enojado && (other.transform.tag == "Pared" || other.transform.tag == "Hueco" || other.transform.tag == "Jugador" || other.transform.tag == "Enemigo"))
        {
            StopAllCoroutines();
            StartCoroutine(CambioDeDireccion(tempCambio));
            velocidadBonus = 1;
            enojado = false;
            if (other.transform.tag == "Pared") GiroChoquePared(other.transform.localPosition, Mathf.RoundToInt(transform.eulerAngles.z));
            else transform.Rotate(Vector3.forward * 180);
            eje.rotation = transform.rotation;

        }
        else if (other.transform.tag == "Pared" || other.transform.tag == "Hueco" || other.transform.tag == "Jugador" || other.transform.tag == "Enemigo")
        {
            if (other.transform.tag == "Pared") GiroChoquePared(other.transform.localPosition, Mathf.RoundToInt(transform.eulerAngles.z));
            else transform.Rotate(Vector3.forward * 180);
            eje.rotation = transform.rotation;
        }
    }
}
