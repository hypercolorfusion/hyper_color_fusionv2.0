using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossTutorial : Enemigo
{
    [Header("Boss Tutorial")]
    public GameObject bolaNieve;


    private bool girando;
    private bool ultimaFase;
    private bool regenerando;

    public float tempCriar;
    public float cantBalas;

    public float velocidadRotacion = 50f;
    private int indiceColor;

    private IEnumerator corrutinaBalasTeledirigidas;

    // Start is called before the first frame update
    void Start()
    {

        InstancearBarraVida();
        ActualizarColor();
        InmuneFuego();
        SetCanMove(true);
        SetLookAt(true);
        girando = true;
        //ActualizarStats(false); Los boses no actualizan stats
        muerteAutomatica = ultimaFase = false;
        StartCoroutine(LluviaDeBalas());
        corrutinaBalasTeledirigidas = DisparoTeledirigido(2, 200f);
        StartCoroutine(corrutinaBalasTeledirigidas);
    }

    // Update is called once per frame
    void Update()
    {
        if (!regenerando)
        {
            switch (magiaColor)
            {
                case MagiaColor.blanco:
                    if (girando)
                        transform.Rotate(Vector3.forward * Time.deltaTime * 500);
                    else
                        MovimientoInteligente(true);
                    break;
                case MagiaColor.rojo: MovimientoInteligenteSuavizado(velocidadRotacion); break;
                case MagiaColor.azul: MovimientoRecto(); break;
                case MagiaColor.amarillo: break;
                case MagiaColor.morado: break;
                case MagiaColor.naranja: break;
                case MagiaColor.verde: break;
                case MagiaColor.negro: break;
            }

            if (!ultimaFase && resistencia <= resistenciaMax / 8)
            {
                regenerando = true;
                StopAllCoroutines();
                CambiarColor();
                StartCoroutine(Regenerar());
            }
        }
        else
        {
            transform.Rotate(Vector3.forward * Time.deltaTime * 500);
        }

        if (muerto)
        {
            /*
            if (ControladorJuego.sistemaMisiones.tutorial &&
             !ControladorJuego.sistemaMisiones.VerificarMisionCompletadaTutorial("CAMBIO_COLOR_NARANJA") &&
             !ControladorJuego.sistemaMisiones.VerificarMisionInicializadaTutorial("CAMBIO_COLOR_NARANJA"))
            {
                ControladorJuego.sistemaMisiones.IniciarMisionTutorial("CAMBIO_COLOR_NARANJA");
                ControladorJuego.sistemaMagiaColor.DesbloquearMagiaNaranja();
                ControladorJuego.sistemaExperiencia.SubirNivelRojo();
                ControladorJuego.sistemaExperiencia.SubirNivelRojo();
                ControladorJuego.sistemaExperiencia.SubirNivelBlanco();
                ControladorJuego.sistemaMensajes.MostarTexto("TUTORIAL", "DERROTA_FLAMA", "AYUDA", true);
            }

            */
            ControladorJuego.sistemaOleadas.enemigosObjetivo.Remove(gameObject);
            Destroy(gameObject);
        }
    }

    public void CambiarColor()
    { //Cambia de color cada vez que se reduce la vida
        indiceColor++;
        magiaColor = (MagiaColor)indiceColor;
        ActualizarColor();


        if (magiaColor == MagiaColor.negro)
            ultimaFase = true;

    }

    IEnumerator Incendiar()
    {
        while (magiaColor == MagiaColor.rojo)
        {
            yield return new WaitForSeconds(0.25f);
            ControladorJuego.sistemaPool.MostrarFuego(transform.position, false);
        }
    }

    IEnumerator LluviaDeBalas()
    {
        while (true)
        {
            yield return new WaitForSeconds(girando ? 5f : 10f);
            StopCoroutine(corrutinaBalasTeledirigidas);
            girando = !girando;
            corrutinaBalasTeledirigidas = DisparoTeledirigido(girando ? 2 : 1f, 200f);
            StartCoroutine(corrutinaBalasTeledirigidas);
        }
    }

    IEnumerator CriarBola()
    {
        while (magiaColor == MagiaColor.azul)
        {
            yield return new WaitForSeconds(tempCriar);
            Criar();
        }
    }

    IEnumerator Regenerar()
    {
        while (resistencia < resistenciaMax)
        {
            yield return new WaitForSeconds(0.2f);
            GetHitEnemigo(ControladorJuego.sistemaMagiaColor.CalculoAtaqueJugador(10,
             ControladorJuego.sistemaMagiaColor.ObtenerMagiaColorOpuesto(magiaColor), magiaColor), magiaColor);
            //Metodo de curar
        }
        switch (magiaColor)
        {
            case MagiaColor.rojo: resistenciaMax = 300; fireRate = .5f; velocidadBonus = 12; StartCoroutine(Incendiar()); break;
            case MagiaColor.azul: resistenciaMax = 400; fireRate = 2f; velocidadBonus = 5; StartCoroutine(CriarBola()); break;
            case MagiaColor.amarillo: break;
            case MagiaColor.morado: break;
            case MagiaColor.naranja: break;
            case MagiaColor.verde: break;
            case MagiaColor.negro: break;
        }
        regenerando = false;
        resistencia = resistenciaMax;
    }

    private void Criar()
    {
        GameObject _cria;
        _cria = Instantiate(bolaNieve, transform.position, transform.rotation);
        _cria.transform.name = "canionNieve";
        //Cambiar componente
        BolaDeNieve _componente = _cria.GetComponent<BolaDeNieve>();
        _componente.prueba = prueba;
        _componente.CambiarColor(magiaColor);
        ControladorJuego.sistemaOleadas.AgregarEnemigosObjetivos(_cria);
    }


    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.transform.tag == "Pared")
        {
            GiroChoquePared(other.transform.localPosition, Mathf.RoundToInt(transform.eulerAngles.z));
            if (MagiaColor.azul == magiaColor)
                DisparoRadialUnico(cantBalas, 0.5f);
            else if (MagiaColor.rojo == magiaColor)
                DisparoRadialUnico(cantBalas / 36, 0.5f);
        }
        else if (other.transform.tag == "Hueco" || other.transform.tag == "Jugador" || other.transform.tag == "Enemigo")
            transform.Rotate(Vector3.forward * 180);

        eje.rotation = transform.rotation;
    }
}
