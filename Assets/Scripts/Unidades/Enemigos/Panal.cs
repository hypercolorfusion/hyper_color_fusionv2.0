using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Panal : Enemigo

{
    [Header("Panal")]
    public GameObject abejas;
    public GameObject guardianes;
    public GameObject reina;
    public float tempguardianes;
    public float tempdisparoRadial;
    public int cantBalas;
    public Transform spwanPanal;

    // Start is called before the first frame update
    void Start()
    {
        InstancearBarraVida();
        ActualizarColor();
        SetCanMove(false);
        SetLookAt(false);
        ActualizarStats(false);
        StartCoroutine(Defender());
        StartCoroutine(DisparoRadialRandom(1f, cantBalas, tempdisparoRadial));

    }

    // Update is called once per frame
    void Update()
    {
        if (muerto)
        {
            GameObject _liberar;
            _liberar = Instantiate(reina, transform.position, transform.rotation);
            _liberar.transform.name = "Abeja Reina";
            ControladorJuego.sistemaOleadas.AgregarEnemigosObjetivos(_liberar);
            ControladorJuego.sistemaOleadas.enemigosObjetivo.Remove(gameObject);
            Destroy(gameObject);
        }

        if (getHit)
        {
            LiberarAbeja();
            getHit = false;
        }
    }

    private void LiberarAbeja()
    {
        GameObject _liberar;
        _liberar = Instantiate(abejas, spwanPanal.position, eje.rotation);
        _liberar.transform.name = "Abeja Trabajadora";
        ControladorJuego.sistemaOleadas.AgregarEnemigosObjetivos(_liberar);
    }

    private void LiberarDefenza()
    {
        GameObject _liberar;
        _liberar = Instantiate(guardianes, spwanPanal.position, eje.rotation);
        _liberar.transform.name = "Abeja Guardian";
        ControladorJuego.sistemaOleadas.AgregarEnemigosObjetivos(_liberar);
    }

    public IEnumerator Defender()
    {
        for (int i = 0; i < nivel + 1; i++) LiberarDefenza();

        while (true)
        {
            yield return new WaitForSeconds(tempguardianes);
            LiberarDefenza();
        }
    }
}
