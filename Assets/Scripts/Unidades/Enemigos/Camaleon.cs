using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camaleon : Enemigo
{
    [Header("Camaleon")]
    public int dirBalas;
    public float velocidadGiro;
    public bool daltonico = false;
    // Start is called before the first frame update
    void Start()
    {
        InstancearBarraVida();
        ActualizarColor();
        SetCanMove(false);
        SetLookAt(true);
        ActualizarStats(true);
        StartCoroutine(DisparoLaserMultipleAutomatico(dirBalas, transform));
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0f, 0f, Time.deltaTime * velocidadGiro * velocidadBonus, Space.Self);
        magiaColor = daltonico ?
        ControladorJuego.sistemaMagiaColor.ObtenerMagiaColorOpuestoJugador() : ControladorJuego.jugadorAcceso.magiaColor;
        ActualizarColor();
    }
}
