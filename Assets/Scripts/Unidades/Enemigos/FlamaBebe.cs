using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlamaBebe : Enemigo
{
    //[Header("Flama Bebe")]
    // Start is called before the first frame update
    void Start()
    {
        InstancearBarraVida();
        ActualizarColor();
        SetCanMove(true);
        ActualizarStats(true);
        transform.Rotate(Vector3.forward * Random.Range(0f, 360f));
    }


    // Update is called once per frame
    void Update()
    {
        MovimientoRecto();
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.transform.tag == "Pared")
            GiroChoquePared(other.transform.localPosition, Mathf.RoundToInt(transform.eulerAngles.z));
        else if (other.transform.tag == "Hueco" || other.transform.tag == "Jugador" || other.transform.tag == "Enemigo")
            transform.Rotate(Vector3.forward * 180);

        eje.rotation = transform.rotation;
    }
}
