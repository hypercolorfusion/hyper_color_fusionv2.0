﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo : Unidad
{
    [Header("Basico Enemigo")]
    public Transform eje;
    public Transform spawnBala;
    public SpriteRenderer miSprite;

    private bool lookAt;
    private bool canMove;


    private Vector3 direccion;
    public float velocidadBonus = 1f;
    public int exp;
    public int nivel = 1;
    public bool inmuneFuego = false;
    public bool prueba = false;

    #region Estados ocultos
    [HideInInspector]
    public bool relantizado = false;
    [HideInInspector]
    public bool congelado = false;
    [HideInInspector]
    public bool absorviendo = false;
    [HideInInspector]
    public bool getHit = false;
    //[HideInInspector]
    [HideInInspector]
    public bool muerto = false;
    [HideInInspector]
    public bool muerteAutomatica = false;
    #endregion

    #region Barra de Vida
    [HideInInspector]
    public BarraVida barraVida;

    [Header("Barra de vida")]
    public Vector2 barraVidaOffSet;
    #endregion




    public void ActualizarStats(bool _valor)
    {
        muerteAutomatica = _valor;
        resistenciaMax = resistenciaMax * nivel;
        resistencia = resistencia * nivel;
        ataque = ataque * nivel;
        exp = exp * nivel * nivel;
        if (nivel != 1)
        {
            defensa = defensa * (1 + (nivel / 2.5f));
            speed = speed * (1 + (nivel / 5f));
            fireRate = fireRate * (1 - (nivel / 10f));
        }
    }

    public void ActualizarColor()
    {
        miSprite.color = ControladorJuego.sistemaMagiaColor.ObtenerColor(magiaColor);
        if (!prueba) barraVida.ActualizarColor();
    }

    public void CambiarColor(MagiaColor _color)
    {
        magiaColor = _color;
        ActualizarColor();
    }

    public void CambiarColorOpuesto()
    {
        CambiarColor(ControladorJuego.sistemaMagiaColor.ObtenerMagiaColorOpuesto(magiaColor));
    }

    public void InstancearBarraVida()
    {
        ControladorJuego.sistemaPool.MostrarBarraVida(barraVidaOffSet, this, resistenciaMax / 100f);
    }

    public void SetearBarraVida(BarraVida _barravida)
    {
        if (!prueba)
        {
            barraVida = _barravida;
            barraVida.padre = this;
            barraVida.ActualizarBarraVida();
        }
    }

    #region Metodos de Verificacion

    public void VerificarJugador()
    {
        if (ControladorJuego.jugadorAcceso && lookAt)
        {
            LookAt2D(ControladorJuego.jugadorAcceso.transform, ref eje);
            direccion = (ControladorJuego.jugadorAcceso.transform.position - transform.position).normalized;
        }
    }

    public void VerificarJugadorInteligente()
    {
        if (ControladorJuego.jugadorAcceso && lookAt)
        {
            LookAt2DSmart(ControladorJuego.jugadorAcceso.transform, ref eje);
            direccion = (ControladorJuego.jugadorAcceso.transform.position - transform.position).normalized;
        }
    }

    public void SetLookAt(bool _value) { lookAt = _value; }
    public void SetCanMove(bool _value) { canMove = _value; }
    public void InmuneFuego() { inmuneFuego = true; }

    #endregion

    #region Ataques con Disparo

    public IEnumerator DisparoClasico()
    {
        while (true)
        {
            yield return new WaitForSeconds(fireRate * (relantizado ? 2 : 1));
            ControladorJuego.sistemaPool.DispararBalaEnemiga(spawnBala.position, eje.rotation, magiaColor, ataque, 1f);
        }
    }

    public IEnumerator DisparoClasicoRandom(int _cantBalas)
    {
        while (true)
        {
            yield return new WaitForSeconds(fireRate * (relantizado ? 2 : 1));
            float _random = 0;
            for (int i = 0; i < _cantBalas; i++)
            {
                _random = Random.Range(-10f, 10f);
                eje.Rotate(0f, 0f, _random, Space.Self);
                ControladorJuego.sistemaPool.DispararBalaEnemiga(spawnBala.position, eje.rotation, magiaColor, ataque, 1f);
                eje.Rotate(0f, 0f, -_random, Space.Self);
                yield return new WaitForSeconds(0.4f);
            }
        }
    }

    public void DisparoTeledirigidoUnico(float _velocidadBonus, float _velocidadRotacion)
    {
        bool _random = Random.Range(0, 2) == 1;
        eje.Rotate(0f, 0f, _random ? 90f : -90f, Space.Self);
        ControladorJuego.sistemaPool.DispararBalaEnemigaTeledirigido(transform.position, eje.rotation, magiaColor, ataque, _velocidadBonus, _velocidadRotacion);
        eje.Rotate(0f, 0f, _random ? -90f : 90f, Space.Self);
    }

    public IEnumerator DisparoTeledirigido(float _velocidadBonus, float _velocidadRotacion)
    {
        while (true)
        {
            yield return new WaitForSeconds(fireRate * (relantizado ? 2 : 1));
            DisparoTeledirigidoUnico(_velocidadBonus, _velocidadRotacion);
        }
    }

    public void DisparoReboteUnico(float _velovidadBonus)
    {
        ControladorJuego.sistemaPool.DispararBalaEnemigoRebote(transform.position, eje.rotation, magiaColor, ataque, _velovidadBonus, nivel);
    }
    public IEnumerator DisparoRebote(float _velovidadBonus)
    {

        while (true)
        {
            yield return new WaitForSeconds(fireRate * (relantizado ? 2 : 1));
            DisparoReboteUnico(_velovidadBonus);
        }
    }

    public IEnumerator DisparoVectorial(bool _inteligente, float _velocidadRotacion, float _tiempoEspera, int _giros)
    {
        while (true)
        {
            yield return new WaitForSeconds(fireRate * (relantizado ? 2 : 1));
            DisparoVectorailUnico(_inteligente, _velocidadRotacion, _tiempoEspera, _giros);
        }
    }

    public IEnumerator DisparoEspecial(float _tiempo)
    {
        while (true)
        {
            yield return new WaitForSeconds(fireRate * (relantizado ? 2 : 1));
            ControladorJuego.sistemaPool.DispararBalaGrandeEnemigo(spawnBala.position, eje.rotation, magiaColor, ataque, 1f);
        }
    }
    public void DisparoVectorailUnico(bool _inteligente, float _velocidadRotacion,
     float _tiempoEspera, int _giros)
    {
        ControladorJuego.sistemaPool.DispararBalaEnemigoVectorial(spawnBala.position, eje.rotation, magiaColor, ataque,
         _inteligente, _velocidadRotacion, _tiempoEspera, _giros, 1f);
    }

    public void DisparoRadialUnico(float _cantidad, float _espacios, float _velocidadBonus)
    {

        for (int i = 0; i < _cantidad; i++)
        {
            ControladorJuego.sistemaPool.DispararBalaEnemiga(spawnBala.position, eje.rotation, magiaColor, ataque, _velocidadBonus);
            eje.Rotate(0f, 0f, _espacios, Space.Self);
        }
    }

    public void DisparoRadialUnico(float _cantidad, float _velocidadBonus)
    {
        float _espacios = 360 / _cantidad;
        for (int i = 0; i < _cantidad; i++)
        {
            ControladorJuego.sistemaPool.DispararBalaEnemiga(spawnBala.position, eje.rotation, magiaColor, ataque, _velocidadBonus);
            eje.Rotate(0f, 0f, _espacios, Space.Self);
        }
    }

    public void DisparoRadialVectorailUnico(float _cantidad, bool _inteligente, float _velocidadBonus,
     float _tiempoEspera, int _giros)
    {
        float _espacios = 360 / _cantidad;
        for (int i = 0; i < _cantidad; i++)
        {
            ControladorJuego.sistemaPool.DispararBalaEnemigoVectorial(spawnBala.position, eje.rotation, magiaColor, ataque,
         _inteligente, _velocidadBonus, _tiempoEspera, _giros, 1f);
            eje.Rotate(0f, 0f, _espacios, Space.Self);
        }
    }

    public IEnumerator DisparoRadialVectorial(float _cantidad, bool _inteligente, float _velocidadBonus,
     float _tiempoEspera, int _giros)
    {
        float _espacios = 360 / _cantidad;
        while (true)
        {
            yield return new WaitForSeconds(fireRate * (relantizado ? 2 : 1));
            DisparoRadialVectorailUnico(_cantidad, _inteligente, _velocidadBonus, _tiempoEspera, _giros);
        }
    }

    public IEnumerator DisparoRadial(float _cantidad)
    {
        float _espacios = 360 / _cantidad;
        while (true)
        {
            yield return new WaitForSeconds(fireRate);
            DisparoRadialUnico(_cantidad, _espacios);
        }
    }

    public IEnumerator DisparoRadialIrregular(float _cantidad, float _velocidadAtaque)
    {
        int _random;
        while (true)
        {
            lookAt = false;
            for (int i = 0; i < _cantidad; i++)
            {
                _random = Random.Range(-5, 16);
                ControladorJuego.sistemaPool.DispararBalaEnemiga(spawnBala.position, eje.rotation, magiaColor, ataque, 1f);
                eje.Rotate(0f, 0f, _random, Space.Self);
                yield return new WaitForSeconds(_velocidadAtaque);
            }
            lookAt = true;
            yield return new WaitForSeconds(fireRate);
        }
    }

    public IEnumerator DisparoRadialRandom(float _velocidadBonus, int _cantidad)
    {
        int _random;
        if (lookAt)
        {
            while (true)
            {
                lookAt = false;
                for (int i = 0; i < _cantidad; i++)
                {
                    _random = Random.Range(0, 361);
                    ControladorJuego.sistemaPool.DispararBalaEnemiga(spawnBala.position, eje.rotation, magiaColor, ataque, _velocidadBonus);
                    eje.Rotate(0f, 0f, _random, Space.Self);
                    yield return new WaitForSeconds(0.05f);
                }
                lookAt = true;
                yield return new WaitForSeconds(fireRate * (relantizado ? 2 : 1));
            }
        }
        else
        {
            while (true)
            {
                for (int i = 0; i < _cantidad; i++)
                {
                    _random = Random.Range(0, 361);
                    ControladorJuego.sistemaPool.DispararBalaEnemiga(spawnBala.position, eje.rotation, magiaColor, ataque, 1f);
                    eje.Rotate(0f, 0f, _random, Space.Self);
                    yield return new WaitForSeconds(0.05f);
                }
                yield return new WaitForSeconds(fireRate * (relantizado ? 2 : 1));
            }
        }
    }

    public IEnumerator DisparoRadialRandom(float _velocidadBonus, int _cantidad, float _fireRate)
    {
        int _random;
        if (lookAt)
        {
            while (true)
            {
                lookAt = false;
                for (int i = 0; i < _cantidad; i++)
                {
                    _random = Random.Range(0, 361);
                    ControladorJuego.sistemaPool.DispararBalaEnemiga(spawnBala.position, eje.rotation, magiaColor, ataque, _velocidadBonus);
                    eje.Rotate(0f, 0f, _random, Space.Self);
                    yield return new WaitForSeconds(0.05f);
                }
                lookAt = true;
                yield return new WaitForSeconds(_fireRate);
            }
        }
        else
        {
            while (true)
            {
                for (int i = 0; i < _cantidad; i++)
                {
                    _random = Random.Range(0, 361);
                    ControladorJuego.sistemaPool.DispararBalaEnemiga(spawnBala.position, eje.rotation, magiaColor, ataque, 1f);
                    eje.Rotate(0f, 0f, _random, Space.Self);
                    yield return new WaitForSeconds(0.05f);
                }
                yield return new WaitForSeconds(_fireRate);
            }
        }
    }


    public IEnumerator DisparoLaserInteligente(float _tiempoEspera, float _tiempoLaser, float _velocidadLaser)
    {
        bool stop = false;
        float _randomFireRate;
        int _disparosAzar;
        yield return new WaitForSeconds(_tiempoEspera);

        if (fireRate <= 1)
            fireRate = 2;

        while (true)
        {

            if (!stop)
            { //Disparos random al jugador
                for (int i = 0; i < 3; i++)
                {
                    _disparosAzar = Random.Range(3, 6);
                    for (int j = 0; j < _disparosAzar; j++)
                    {
                        _randomFireRate = Random.Range(1, fireRate * 10) / 100f;
                        ControladorJuego.sistemaPool.DispararBalaEnemiga(spawnBala.position, eje.rotation, magiaColor, ataque, 1f);
                        yield return new WaitForSeconds(_randomFireRate);
                    }
                    yield return new WaitForSeconds(_tiempoEspera);
                }
                stop = !stop;

            }
            else
            { //Disparo Laser al jugador
                for (int i = 0; i < _tiempoLaser * 100; i++)
                {
                    ControladorJuego.sistemaPool.DispararBalaEnemiga(spawnBala.position, eje.rotation, magiaColor, ataque, 1f);
                    yield return new WaitForSeconds(_velocidadLaser);
                }
                stop = !stop;
                yield return new WaitForSeconds(_tiempoEspera);
            }
        }
    }

    public IEnumerator DisparoLaserRadial(float _tiempoEspera, float _tiempoLaser, float _velocidadLaser, float _velocidadGiro)
    {
        bool stop = false;
        float _randomFireRate;
        int _disparosAzar;
        yield return new WaitForSeconds(_tiempoEspera);

        if (fireRate <= 1)
            fireRate = 2;

        while (true)
        {

            if (!stop)
            { //Disparos random al jugador
                for (int i = 0; i < 3; i++)
                {
                    _disparosAzar = Random.Range(3, 6);
                    for (int j = 0; j < _disparosAzar; j++)
                    {
                        _randomFireRate = Random.Range(1, fireRate * 10) / 100f;
                        //Debug.Log (_randomFireRate);
                        ControladorJuego.sistemaPool.DispararBalaEnemiga(spawnBala.position, eje.rotation, magiaColor, ataque, 1f);
                        yield return new WaitForSeconds(_randomFireRate);
                    }
                    yield return new WaitForSeconds(_tiempoEspera);
                }
                stop = !stop;

            }
            else
            { //Disparo Laser al jugador
                lookAt = false;
                for (int i = 0; i < _tiempoLaser * 100; i++)
                {
                    transform.Rotate(Vector3.forward * _velocidadGiro);
                    ControladorJuego.sistemaPool.DispararBalaEnemiga(spawnBala.position, eje.rotation, magiaColor, ataque, 1f);
                    yield return new WaitForSeconds(_velocidadLaser);
                }
                lookAt = true;
                stop = !stop;
                yield return new WaitForSeconds(_tiempoEspera);
            }
        }
    }

    public void DisparoLaserMultiple(float _numeroLaser, Transform _eje)
    {
        float _angulo = 360 / _numeroLaser;
        for (int i = 0; i < _numeroLaser; i++)
        {
            _eje.Rotate(Vector3.forward * _angulo);
            ControladorJuego.sistemaPool.DispararBalaEnemiga(_eje.position, _eje.rotation, magiaColor, ataque, 1f);
        }
    }

    public void DisparoLaserMultiple(float _numeroLaser, Transform _eje, float _angulo)
    {
        for (int i = 0; i < _numeroLaser; i++)
        {
            _eje.Rotate(Vector3.forward * _angulo);
            ControladorJuego.sistemaPool.DispararBalaEnemiga(_eje.position, _eje.rotation, magiaColor, ataque, 1f);
        }
    }

    public IEnumerator DisparoLaserMultipleAutomatico(float _numeroLaser, Transform _eje)
    {
        float _angulo = 360 / _numeroLaser;
        while (true)
        {
            yield return new WaitForSeconds(fireRate * (relantizado ? 2 : 1));
            DisparoLaserMultiple(_numeroLaser, _eje, _angulo);
        }
    }

    #endregion

    #region Patrones de Movimiento

    public IEnumerator AtaqueDash(float _espera, float _dashDistance, float _dashCooldown, float _friccion)
    {
        float _velocidadTemporal = _dashDistance;
        float _exponente = 0f;
        yield return new WaitForSeconds(_espera * 0.8f);
        canMove = false;
        yield return new WaitForSeconds(_espera * 0.2f);
        lookAt = false;
        while (_velocidadTemporal >= 0)
        {
            yield return new WaitForSeconds(0.02f);
            _exponente += _friccion;
            _velocidadTemporal -= _exponente;
            if (!congelado)
                transform.Translate(direccion * _velocidadTemporal * velocidadBonus * 0.02f);
        }
        StartCoroutine(RecargaDash(_espera, _dashDistance, _dashCooldown, _friccion));
    }

    public IEnumerator RecargaDash(float _espera, float _dashDistance, float _dashCooldown, float _friccion)
    {
        lookAt = true;
        canMove = true;
        yield return new WaitForSeconds(_dashCooldown);
        StartCoroutine(AtaqueDash(_espera, _dashDistance, _dashCooldown, _friccion));
    }

    public IEnumerator CambioDeDireccion(float _tiempopProemdio)
    {
        float _tiempoRandom = 0;
        while (true)
        {
            transform.Rotate(Vector3.forward * Random.Range(0, 360));
            _tiempoRandom = Random.Range(_tiempopProemdio / 2f, _tiempopProemdio * 2f);
            eje.rotation = transform.rotation;
            yield return new WaitForSeconds(_tiempoRandom);
        }
    }

    public void MovimientoRecto()
    {
        if (canMove && !congelado)
            transform.Translate(Vector3.right * Time.deltaTime * speed * velocidadBonus);

        if (canMove && absorviendo)
            MovimientoAbsorviendo();
    }

    public void MovimientoLateral()
    {
        if (canMove && !congelado)
            transform.Translate(eje.up * Time.deltaTime * speed * velocidadBonus);

        if (canMove && absorviendo)
            MovimientoAbsorviendo();
    }

    public void MovimientoAbsorviendo()
    {
        direccion = (ControladorJuego.jugadorAcceso.transform.position - transform.position).normalized;
        if (ControladorJuego.jugadorAcceso.magiaColor == MagiaColor.morado)
            transform.Translate(direccion * Time.deltaTime * (ControladorJuego.nivelMorado / 1.5f), Space.World);
        else if (ControladorJuego.jugadorAcceso.magiaColor == MagiaColor.negro)
            transform.Translate(direccion * Time.deltaTime * (ControladorJuego.nivelNegro / 3f), Space.World);
    }

    public void MovimientoInteligente(bool _valor)
    {
        transform.rotation = new Quaternion(0, 0, 0, 0f);
        VerificarJugador();
        if (canMove && !congelado)
            transform.Translate(direccion * Time.deltaTime * speed * velocidadBonus * (_valor ? 1 : -1));
    }

    //No necesita el look at
    public void MovimientoInteligenteSuavizado(float _velocidadRotacion)
    {
        if (canMove && !congelado)
        {
            Vector3 myLocation = transform.position;
            Vector3 targetLocation = ControladorJuego.jugadorAcceso.transform.position;
            myLocation.z = targetLocation.z; // ensure there is no 3D rotation by aligning Z position

            // vector from this object towards the target location
            Vector3 vectorToTarget = (targetLocation - myLocation).normalized;
            // rotate that vector by 90 degrees around the Z axis
            Vector3 rotatedVectorToTarget = Quaternion.Euler(0, 0, 90) * vectorToTarget;

            // get the rotation that points the Z axis forward, and the Y axis 90 degrees away from the target
            // (resulting in the X axis facing the target)
            Quaternion targetRotation = Quaternion.LookRotation(forward: Vector3.forward, upwards: rotatedVectorToTarget);

            // changed this from a lerp to a RotateTowards because you were supplying a "speed" not an interpolation value
            transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, _velocidadRotacion * Time.deltaTime);
            transform.Translate(Vector3.right * Time.deltaTime * speed * velocidadBonus);
        }
    }

    public void MovimientoInteligenteSeguimiento(Transform _objetivo)
    {

        LookAt2D(_objetivo, ref eje);
        direccion = (_objetivo.position - transform.position).normalized;
        if (canMove && !congelado)
            transform.Translate(eje.transform.right * Time.deltaTime * speed * velocidadBonus);
    }

    public void GiroChoquePared(Vector2 _posPared, int _anguloRedondeado)
    {
        int _anguloRotar = 0;
        int _anguloObjetivo = 0;
        int _anguloRedondeadoTemp = _anguloRedondeado;

        if (_posPared.x != 0)
        {
            //Chocó con la pared de la derecha
            if (_posPared.x > 0)
            {
                if (_anguloRedondeado == 360 || _anguloRedondeado == 0)
                    _anguloRotar = 180;
                else
                {
                    _anguloObjetivo = 180 - _anguloRedondeado;
                    _anguloRotar = _anguloObjetivo - _anguloRedondeado;
                }
            }
            //Chocó con la pared de la izquierda
            else
            {
                if (_anguloRedondeado == 180)
                    _anguloRotar = 180;
                else
                {

                    _anguloRedondeadoTemp = 90 - _anguloRedondeado;
                    _anguloObjetivo = 90 + _anguloRedondeadoTemp;
                    _anguloRotar = _anguloObjetivo - _anguloRedondeado;
                }
            }
        }
        else if (_posPared.y != 0)
        {
            //Chocó con la pared de arriba
            if (_posPared.y > 0)
            {
                if (_anguloRedondeado == 90)
                    _anguloRotar = 180;
                else
                {
                    _anguloRedondeadoTemp = 180 - _anguloRedondeado;
                    _anguloObjetivo = 180 + _anguloRedondeadoTemp;
                    _anguloRotar = _anguloObjetivo - _anguloRedondeado;
                }
            }
            //Chocó con la pared de la abajo
            else
            {
                if (_anguloRedondeado == 270)
                    _anguloRotar = 180;
                else
                {
                    _anguloRedondeadoTemp = 180 - _anguloRedondeado;
                    _anguloObjetivo = 180 + _anguloRedondeadoTemp;
                    _anguloRotar = _anguloObjetivo - _anguloRedondeado;
                }
            }
        }
        transform.Rotate(Vector3.forward * _anguloRotar);
    }
    #endregion


    #region metodos de vida/daño
    public void GetHitEnemigo(int _valor, MagiaColor _color)
    {
        int danio = Mathf.RoundToInt(_valor / defensa);
        if (danio > 0) getHit = true;
        resistencia -= danio;
        ControladorJuego.sistemaPool.MostrarFeedBackAtaque(transform.position, _color, danio, false);
        if (!prueba) barraVida.ActualizarBarraVida();
        ComprobarVida(_color);
    }


    // Verificar de probablidades de que el enemigo haga algo o suceda algo al morir
    public void ComprobarVida(MagiaColor _color)
    {
        if (resistencia <= 0 && !muerto)
        {
            int _random = 0;
            if (_color == MagiaColor.blanco)
            {
                _random = Random.Range(0, 101);
                switch (ControladorJuego.nivelBlanco)
                {
                    case 1:
                        if (_random <= 5) ControladorJuego.jugadorAcceso.Curar(Mathf.RoundToInt(resistenciaMax * 0.10f)); break;
                    case 2:
                        if (_random <= 10) ControladorJuego.jugadorAcceso.Curar(Mathf.RoundToInt(resistenciaMax * 0.10f)); break;
                    case 3:
                        if (_random <= 10) ControladorJuego.jugadorAcceso.Curar(Mathf.RoundToInt(resistenciaMax * 0.12f)); break;
                    case 4:
                        if (_random <= 15) ControladorJuego.jugadorAcceso.Curar(Mathf.RoundToInt(resistenciaMax * 0.12f)); break;
                    case 5:
                        if (_random <= 15) ControladorJuego.jugadorAcceso.Curar(Mathf.RoundToInt(resistenciaMax * 0.15f)); break;
                    default:
                        if (_random <= 20) ControladorJuego.jugadorAcceso.Curar(Mathf.RoundToInt(resistenciaMax * 0.15f)); break;
                }
            }
            else if (_color == MagiaColor.verde)
            {
                _random = Random.Range(0, 101);
                switch (ControladorJuego.nivelVerde)
                {
                    case 1:
                        if (_random <= 15) ControladorJuego.jugadorAcceso.Curar(Mathf.RoundToInt(resistenciaMax * 0.05f)); break;
                    case 2:
                        if (_random <= 20) ControladorJuego.jugadorAcceso.Curar(Mathf.RoundToInt(resistenciaMax * 0.10f)); break;
                    case 3:
                        if (_random <= 25) ControladorJuego.jugadorAcceso.Curar(Mathf.RoundToInt(resistenciaMax * 0.15f)); break;
                    case 4:
                        if (_random <= 30) ControladorJuego.jugadorAcceso.Curar(Mathf.RoundToInt(resistenciaMax * 0.20f)); break;
                    case 5:
                        if (_random <= 35) ControladorJuego.jugadorAcceso.Curar(Mathf.RoundToInt(resistenciaMax * 0.25f)); break;
                    default:
                        if (_random <= 40) ControladorJuego.jugadorAcceso.Curar(Mathf.RoundToInt(resistenciaMax * 0.30f)); break;
                }
            }
            if (!prueba) ControladorJuego.sistemaPool.OcultarBarraVida(barraVida.gameObject);
            ControladorJuego.sistemaExperiencia.GanarExperiencia(exp);
            if (muerteAutomatica)
            {
                ControladorJuego.sistemaOleadas.enemigosObjetivo.Remove(gameObject);
                Destroy(gameObject);
            }

            muerto = true;
        }
    }
    #endregion

    #region ESTADOS

    public void Relantizar()
    {
        velocidadBonus -= 0.1f;
        if (velocidadBonus <= 0.3f) velocidadBonus = 0.3f;
        StartCoroutine(TiempoRelantizado());
    }
    public void Paralizar()
    {
        if (!congelado) StartCoroutine(TiempoParalizado());
    }

    IEnumerator TiempoRelantizado()
    {
        relantizado = true;
        yield return new WaitForSeconds(5f);
        velocidadBonus += 0.1f;
        if (velocidadBonus >= 1f)
        {
            velocidadBonus = 1f;
            relantizado = false;
        }
    }

    IEnumerator TiempoParalizado()
    {
        congelado = true;
        yield return new WaitForSeconds(5f);
        congelado = false;
        for (int i = 0; i < 3; i++) Relantizar();
    }

    #endregion
}