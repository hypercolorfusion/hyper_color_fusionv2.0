﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoDash : Enemigo
{

    public float waitingTime;
    public float dashCooldown;
    public float friccion;

    public float dashDistancia;

    //Variable Privadas

    void Start()
    {
        SetLookAt(true);
        SetCanMove(true);
        StartCoroutine(AtaqueDash(waitingTime, dashDistancia, dashCooldown, friccion));
        ActualizarColor();
    }

    // Update is called once per frame
    void Update()
    {
        //ComprobarVida ();
        MovimientoInteligente(true);
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        StopAllCoroutines();
        StartCoroutine(AtaqueDash(waitingTime, dashDistancia, dashCooldown, friccion));
        SetLookAt(true);
        SetCanMove(true);
    }
}