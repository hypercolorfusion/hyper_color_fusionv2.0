﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoLaser : Enemigo
{

    public float _tiempoLaser = 2f;
    public float _tiempoEspera = 1f;
    public float _velocidadLaser = 0.001f;
    public float _velocidadGiro = 1f;

    public bool inteligente;

    void Start()
    {
        SetLookAt(true);
        if (inteligente)
            StartCoroutine(DisparoLaserInteligente(_tiempoEspera, _tiempoLaser, _velocidadLaser));
        else
            StartCoroutine(DisparoLaserRadial(_tiempoEspera, _tiempoLaser, _velocidadLaser, _velocidadGiro));
        ActualizarColor();
    }

    // Update is called once per frame
    void Update()
    {
        //esto debe calcularlo la bala o al momento de colision
        VerificarJugador();
    }
}