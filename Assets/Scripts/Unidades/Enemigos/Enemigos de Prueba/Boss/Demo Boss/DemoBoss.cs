﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoBoss : Enemigo
{

    [Header("Boss")]
    public Transform centro;
    public float tempRecuperarCuerpo;
    public float cantLaser;

    public List<DemoBossCuerpo> cuerpos;
    private float fireRateReal;
    private float cantCuerpoInicial;
    public bool frenesi;

    // Start is called before the first frame update
    void Start()
    {
        SetCanMove(true);
        SetLookAt(true);
        ActualizarColor();
        StartCoroutine(DisparoEspecial(fireRate));
        StartCoroutine(RecuperarCuerpo());
        fireRateReal = fireRate;
        cantCuerpoInicial = cuerpos.Count;
        frenesi = false;
    }

    // Update is called once per frame
    void Update()
    {
        MovimientoInteligente(true);
    }

    public void PerderCuerpo()
    {
        fireRate = fireRateReal + cuerpos.Count - cantCuerpoInicial;
        StopAllCoroutines();
        if (cuerpos.Count <= 2)
        {
            CambiarColor(MagiaColor.negro);
            frenesi = true;
            StartCoroutine(Separacion());
        }
        else
        {
            foreach (DemoBossCuerpo _cuerpo in cuerpos)
            {
                _cuerpo.Seguir();
            }
        }

        if (frenesi)
        {
            speed = 3;
            StartCoroutine(DisparoLaserMultipleAutomatico(cantLaser, centro));
        }
        else
            StartCoroutine(RecuperarCuerpo());

        StartCoroutine(DisparoEspecial(fireRate));
    }

    IEnumerator Separacion()
    {
        for (int i = cuerpos.Count; i > 0; i--)
        {
            yield return new WaitForSeconds(5f);
            cuerpos[i - 1].Separarse();
            Debug.Log("holi");
        }

    }

    IEnumerator RecuperarCuerpo()
    {
        while (!frenesi)
        {
            yield return new WaitForSeconds(tempRecuperarCuerpo);
            foreach (DemoBossCuerpo _cuerpo in cuerpos)
            {
                _cuerpo.Seguir();
            }
        }
    }
}