﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoBossCuerpo : Enemigo
{
    // Start is called before the first frame update
    [Header("Boss")]
    public DemoBoss cabeza;
    public Transform padre;
    public Transform canon;
    public bool canAttack;
    public float distSeparacion;

    private bool separado;
    private bool activateAttack;
    private bool regresando;

    void Start()
    {
        SetCanMove(true);
        SetLookAt(true);
        ActualizarColor();
        separado = false;
        transform.SetParent(null);
        activateAttack = false;
        regresando = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!padre)
        {
            Destroy(gameObject);
            return;
        }

        MovimientoInteligenteSeguimiento(padre);
        if (!separado)
        {
            if (regresando)
            {
                if (distSeparacion > Vector3.Distance(padre.position, transform.position))
                {
                    regresando = false;
                }
            }
            else
            {
                if (distSeparacion < Vector3.Distance(padre.position, transform.position) && !cabeza.frenesi)
                {
                    Separarse();
                }
            }
        }
        else
        {
            if (canAttack && !activateAttack)
            {

                canon.gameObject.SetActive(true);
                eje.Rotate(Vector3.forward * Random.Range(0, 360));
                StartCoroutine(DisparoClasico());
                activateAttack = true;
            }
        }
    }

    public void Separarse()
    {
        SetCanMove(false);
        SetLookAt(false);
        separado = true;
    }

    public void Seguir()
    {
        canon.gameObject.SetActive(false);
        regresando = true;
        separado = false;
        activateAttack = false;
        SetLookAt(true);
        SetCanMove(true);
        StopAllCoroutines();
    }
    void OnDestroy()
    {
        cabeza.cuerpos.Remove(this);
        cabeza.PerderCuerpo();
    }
}