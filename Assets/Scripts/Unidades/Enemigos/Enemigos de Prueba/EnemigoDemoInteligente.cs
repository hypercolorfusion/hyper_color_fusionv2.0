﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoDemoInteligente : Enemigo
{
    // Start is called before the first frame update


    void Start()
    {
        StartCoroutine(DisparoClasico());
        ActualizarColor();
    }

    // Update is called once per frame
    void Update()
    {
        //esto debe calcularlo la bala o al momento de colision
        VerificarJugadorInteligente();
    }

}