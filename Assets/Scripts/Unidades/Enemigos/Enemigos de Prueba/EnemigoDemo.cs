﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoDemo : Enemigo
{
    // Start is called before the first frame update

    public GameObject bala;
    public GameObject balaGrande;

    void Start()
    {
        StartCoroutine(DisparoClasico());
        StartCoroutine(DisparoEspecial(2f));
        ActualizarColor();
    }

    // Update is called once per frame
    void Update()
    {
        //esto debe calcularlo la bala o al momento de colision
        VerificarJugador();
    }

}