﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoDisparoEspecial : Enemigo
{
    public bool inteligente;

    void Start()
    {
        StartCoroutine(DisparoVectorial(inteligente, 1f, 2f, 4));
        ActualizarColor();
    }

    // Update is called once per frame
    void Update()
    {
        //esto debe calcularlo la bala o al momento de colision
        VerificarJugador();
    }
}