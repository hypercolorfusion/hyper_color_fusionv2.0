﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoBasico : Enemigo

{
    public bool movimientoInteligente;
    public float tiempoMedia;

    // Start is called before the first frame update
    void Start()
    {
        ActualizarColor();
        SetCanMove(true);
        SetLookAt(true);
        if (!movimientoInteligente)
            StartCoroutine(CambioDeDireccion(tiempoMedia));

    }

    // Update is called once per frame
    void Update()
    {
        if (movimientoInteligente)
            MovimientoInteligente(true);
        else
            MovimientoRecto();
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.transform.tag == "Pared" || other.transform.tag == "Hueco")
        {
            transform.Rotate(Vector3.forward * 180);
        }
    }
}