﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoRadial : Enemigo
{

    public int cantidadBalas;
    public float _velocidadAtaque = 0.02f;

    [Header("Disparo Random?")]
    public bool random;
    public bool otroRandom;

    void Start()
    {
        SetLookAt(true);
        if (!random)
        {
            StartCoroutine(DisparoRadial(cantidadBalas));
        }
        else
        {
            if (!otroRandom)
                StartCoroutine(DisparoRadialRandom(1f, cantidadBalas));
            else
                StartCoroutine(DisparoRadialIrregular(cantidadBalas, _velocidadAtaque));
        }
        ActualizarColor();
    }

    // Update is called once per frame
    void Update()
    {
        VerificarJugador();
    }
}