using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbejaReina : Enemigo
{

    [Header("Abeja Reina")]
    public float tempCambio;
    // Start is called before the first frame update
    void Start()
    {
        InstancearBarraVida();
        ActualizarColor();
        SetCanMove(true);
        SetLookAt(true);
        StartCoroutine(CambioDeDireccion(tempCambio));
        StartCoroutine(DisparoRebote(0.5f));
        ActualizarStats(false);
    }

    // Update is called once per frame
    void Update()
    {
        VerificarJugadorInteligente();
        MovimientoRecto();

        if (muerto)
        {
            if (ControladorJuego.sistemaMisiones.tutorial &&
             !ControladorJuego.sistemaMisiones.VerificarMisionCompletadaTutorial("CAMBIO_COLOR_VERDE") &&
             !ControladorJuego.sistemaMisiones.VerificarMisionInicializadaTutorial("CAMBIO_COLOR_VERDE"))
            {
                ControladorJuego.sistemaMisiones.IniciarMisionTutorial("CAMBIO_COLOR_VERDE");
                ControladorJuego.sistemaMagiaColor.DesbloquearMagiaVerde();
                ControladorJuego.sistemaExperiencia.SubirNivelVerde();
                ControladorJuego.sistemaExperiencia.SubirNivelVerde();
                ControladorJuego.sistemaExperiencia.SubirNivelBlanco();
                ControladorJuego.sistemaMensajes.MostarTexto("TUTORIAL", "DERROTA_ABEJA", "AYUDA", true);

            }
            ControladorJuego.sistemaOleadas.enemigosObjetivo.Remove(gameObject);
            Destroy(gameObject);
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.transform.tag == "Pared" || other.transform.tag == "Hueco" || other.transform.tag == "Jugador" || other.transform.tag == "Enemigo")
        {
            if (other.transform.tag == "Pared") GiroChoquePared(other.transform.localPosition, Mathf.RoundToInt(transform.eulerAngles.z));
            else transform.Rotate(Vector3.forward * 180);
            eje.rotation = transform.rotation;
        }
    }
}
