using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Topo : Enemigo
{

    [HideInInspector]
    public TopoLider padre;
    [Header("Topo")]
    public int cantBalasRadial;
    public GameObject sombra;

    private bool enojado = false;

    // Start is called before the first frame update
    void Start()
    {
        InstancearBarraVida();
        ActualizarColor();
        SetCanMove(false);
        SetLookAt(true);
        ActualizarStats(false);
        StartCoroutine(DisparoVectorial(false, 1f, 0.5f, nivel + 2));
        sombra.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (muerto) //No se usa OnDestroy por que manda error al cerrar el juego durante el Play y ejecutable
        {
            ControladorJuego.sistemaOleadas.enemigosObjetivo.Remove(gameObject);
            padre.manada.Remove(this);
            Destroy(gameObject);
        }

        LookAt2D(ControladorJuego.jugadorAcceso.transform.position);
        if (getHit && !enojado && !muerto)
        {
            getHit = false;
            Saltar();
        }
    }
    public void SetearPadre(TopoLider _padre) { padre = _padre; }
    public void Enojar()
    {
        enojado = true;
    }

    public void Saltar()
    {
        sombra.transform.SetParent(null);
        sombra.SetActive(true);
        transform.position = Vector3.one * 10000f;
        Vector3 _areapos = ControladorJuego.sistemaArea.ObtenerAreaSegunIDJugador().transform.position;
        sombra.transform.position = new Vector3(_areapos.x + (Random.Range(-13f, 13f)), _areapos.y + (Random.Range(-13f, 13f)), sombra.transform.position.z);
        while (ControladorJuego.adminJuego.DistanciaJugador(sombra.transform.position) <= 2)
        {
            sombra.transform.position = new Vector3(_areapos.x + (Random.Range(-13f, 13f)), _areapos.y + (Random.Range(-13f, 13f)), sombra.transform.position.z);
        }
        StartCoroutine(Aviso());
    }

    IEnumerator Aviso()
    {
        yield return new WaitForSeconds(1f);
        transform.position = sombra.transform.position;
        DisparoRadialUnico(cantBalasRadial, 1f);
        sombra.transform.SetParent(transform);
        sombra.SetActive(false);
    }
}
