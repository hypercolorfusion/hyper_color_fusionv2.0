using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Caparazon : Enemigo
{
    [Header("Caparazón")]
    public float velocidadgiro;


    void Start()
    {
        InstancearBarraVida();
        ActualizarColor();
        SetCanMove(true);
        SetLookAt(false);
        ActualizarStats(true); //Cambiar a falso
        //transform.Rotate(Vector3.forward * Random.Range(0f, 360f));
    }

    // Update is called once per frame
    void Update()
    {
        eje.Rotate(Vector3.forward * Time.deltaTime * velocidadgiro);

        MovimientoRecto();
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.transform.tag == "Pared")
        {
            GiroChoquePared(other.transform.localPosition, Mathf.RoundToInt(transform.eulerAngles.z));
        }
        else if (other.transform.tag == "Hueco" || other.transform.tag == "Jugador" || other.transform.tag == "Enemigo")
            transform.Rotate(Vector3.forward * 180);
    }
}
