using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TortugaArcoiris : Enemigo
{

    [Header("Tortuga Arcoiris")]
    public GameObject caparazon;
    public float velocidadGiro;
    public float tempRotacion;
    public float duracionRotacion;

    public float tempCambioColor;
    public int cantBalas;
    public int cantCaparazones;
    private bool girando;



    // Start is called before the first frame update
    void Start()
    {
        InstancearBarraVida();
        ActualizarColor();
        SetCanMove(true);
        SetLookAt(true);
        ActualizarStats(false);
        StartCoroutine(CambiarColor());
        StartCoroutine(GiroRapido());
        StartCoroutine(FixedDisparoRadialRandom(cantBalas, fireRate));
        cantCaparazones = (nivel + 3) * 3;
    }

    // Update is called once per frame
    void Update()
    {
        if (!girando)
            MovimientoRecto();
        else
        {
            transform.Rotate(Vector3.forward * Time.deltaTime * velocidadGiro * velocidadBonus);
        }
        if (muerto)
        {
            if (ControladorJuego.tutorial &&
             !ControladorJuego.sistemaMisiones.VerificarMisionCompletadaTutorial("CAMBIO_COLOR_NEGRO") &&
             !ControladorJuego.sistemaMisiones.VerificarMisionInicializadaTutorial("CAMBIO_COLOR_NEGRO"))
            {
                ControladorJuego.sistemaMisiones.IniciarMisionTutorial("CAMBIO_COLOR_NEGRO");
                ControladorJuego.sistemaMagiaColor.DesbloquearMagiaNegra();
                ControladorJuego.sistemaExperiencia.SubirNivelNegro();
                ControladorJuego.sistemaMensajes.MostarTexto("TUTORIAL", "DERROTA_TORTUGA", "AYUDA", true);
            }
            ControladorJuego.sistemaOleadas.enemigosObjetivo.Remove(gameObject);
            Destroy(gameObject);
        }
    }


    IEnumerator CambiarColor()
    {
        int _random = 0;
        while (true)
        {
            yield return new WaitForSeconds(tempCambioColor);
            _random = Random.Range(0, 8);
            magiaColor = (MagiaColor)_random;
            ActualizarColor();
        }
    }

    IEnumerator GiroRapido()
    {
        while (true)
        {
            yield return new WaitForSeconds(tempRotacion);
            girando = true;
            SetLookAt(false);
            yield return new WaitForSeconds(duracionRotacion);
            GameObject _caparazon;
            for (int i = 0; i < cantCaparazones; i++)
            {
                yield return new WaitForSeconds(0.1f);
                _caparazon = Instantiate(caparazon, eje.position, eje.rotation);
                ControladorJuego.sistemaOleadas.AgregarEnemigosObjetivos(_caparazon);
            }
            girando = false;
            SetLookAt(true);
        }

    }
    IEnumerator FixedDisparoRadialRandom(float _cantidad, float _velocidadAtaque)
    {
        float _angulo = 360f / cantBalas;
        while (true)
        {
            if (girando)
            {
                for (int i = 0; i < cantBalas; i++)
                {
                    eje.Rotate(0f, 0f, _angulo, Space.Self);
                    ControladorJuego.sistemaPool.DispararBalaEnemiga(spawnBala.position, eje.rotation, magiaColor, ataque, 0.5f);
                }
            }
            yield return new WaitForSeconds(_velocidadAtaque);
        }
    }
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.transform.tag == "Pared" || other.transform.tag == "Hueco" || (other.transform.tag == "Jugador"))
        {
            if (other.transform.tag == "Pared") GiroChoquePared(other.transform.localPosition, Mathf.RoundToInt(transform.eulerAngles.z));
            else transform.Rotate(Vector3.forward * 180);
            eje.rotation = transform.rotation;
            velocidadBonus = velocidadBonus * 1.01f;
        }
    }
}
