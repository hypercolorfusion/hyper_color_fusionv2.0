﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unidad : Origen
{
    [Header("Basico")]
    #region Variables de Vida
    public int resistenciaMax;
    public int resistencia = 50;
    #endregion

    #region Variables Ataque

    public int ataque = 2;
    public float defensa = 1;
    public float speed = 1;
    public float fireRate = 2;
    public MagiaColor magiaColor;
    #endregion
}

