using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : MecanismoOculto
{
    [Header("NPC")]
    public string codigo;
    public string subcodigo;
    public string nombre;
    public bool enRango = false;
    public bool automatico = false;
    // Start is called before the first frame update
    void Start()
    {
        if (!oculto && automatico)
        {
            ControladorJuego.sistemaMensajes.MostarTexto(codigo, subcodigo, nombre, automatico);
            oculto = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (forzarAparicion)
            gameObject.SetActive(true);

        if (!automatico && enRango && !ControladorJuego.adminJuego.juegoPausado && Input.GetKeyDown(KeyCode.E))
            ControladorJuego.sistemaMensajes.MostarTexto(codigo, subcodigo, nombre, automatico);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Jugador") enRango = true;
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Jugador") enRango = false;
    }

}
