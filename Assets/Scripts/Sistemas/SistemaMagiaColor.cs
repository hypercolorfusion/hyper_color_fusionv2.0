using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SistemaMagiaColor : MonoBehaviour
{
    // Start is called before the first frame update
    public Color[] colores;
    void Awake()
    {
        if (ControladorJuego.adminJuego.ObtenerSistemaMagiaColor() == null)
        {
            ControladorJuego.adminJuego.SetearSistemaMagiaColor(this);
            DontDestroyOnLoad(transform.parent);
        }
        else
            Destroy(gameObject);
    }

    #region Uso de Magia Color

    public MagiaColor ObtenerMagiaColorJugador()
    {
        return ControladorJuego.jugadorAcceso.magiaColor;
    }
    public MagiaColor ObtenerMagiaColorOpuestoJugador()
    {
        switch (ControladorJuego.jugadorAcceso.magiaColor)
        {

            case MagiaColor.blanco: return MagiaColor.negro;
            case MagiaColor.rojo: return MagiaColor.verde;
            case MagiaColor.azul: return MagiaColor.naranja;

            case MagiaColor.amarillo: return MagiaColor.morado;
            case MagiaColor.morado: return MagiaColor.amarillo;
            case MagiaColor.naranja: return MagiaColor.azul;
            case MagiaColor.verde: return MagiaColor.rojo;
            case MagiaColor.negro: return MagiaColor.blanco;
            default: Debug.LogError("WTF!!!"); return MagiaColor.sinCOLOR;
        }
    }

    public MagiaColor ObtenerMagiaColorOpuesto(MagiaColor _color)
    {
        switch (_color)
        {
            case MagiaColor.blanco: return MagiaColor.negro;
            case MagiaColor.rojo: return MagiaColor.verde;
            case MagiaColor.azul: return MagiaColor.naranja;

            case MagiaColor.amarillo: return MagiaColor.morado;
            case MagiaColor.morado: return MagiaColor.amarillo;
            case MagiaColor.naranja: return MagiaColor.azul;
            case MagiaColor.verde: return MagiaColor.rojo;
            case MagiaColor.negro: return MagiaColor.blanco;
            default: Debug.LogError("WTF!!!"); return MagiaColor.sinCOLOR;
        }
    }

    public MagiaColor ObtenerMagiaColorComplementario(MagiaColor _color, bool _siguiente)
    {
        if (_siguiente)
        {
            switch (_color)
            {
                case MagiaColor.rojo: return MagiaColor.morado;
                case MagiaColor.azul: return MagiaColor.verde;
                case MagiaColor.amarillo: return MagiaColor.naranja;
                case MagiaColor.morado: return MagiaColor.azul;
                case MagiaColor.naranja: return MagiaColor.rojo;
                case MagiaColor.verde: return MagiaColor.amarillo;
                default: Debug.LogError("WTF!!!"); return MagiaColor.sinCOLOR;
            }
        }
        else
        {
            switch (_color)
            {
                case MagiaColor.rojo: return MagiaColor.naranja;
                case MagiaColor.azul: return MagiaColor.morado;
                case MagiaColor.amarillo: return MagiaColor.verde;
                case MagiaColor.morado: return MagiaColor.rojo;
                case MagiaColor.naranja: return MagiaColor.amarillo;
                case MagiaColor.verde: return MagiaColor.azul;
                default: Debug.LogError("WTF!!!"); return MagiaColor.sinCOLOR;
            }
        }
    }

    public Color ObtenerColor(MagiaColor _color)
    {
        return colores[(int)_color];
    }

    public void BloquearMagiaAbsoluta()
    {
        ControladorJuego.magiaRoja = ControladorJuego.magiaAmarilla = ControladorJuego.magiaAzul = ControladorJuego.magiaMorada
         = ControladorJuego.magiaNaranja = ControladorJuego.magiaNegra = ControladorJuego.magiaVerde = false;
    }

    public void DesbloquarMagiaAbsoluta()
    {
        ControladorJuego.magiaRoja = ControladorJuego.magiaAmarilla = ControladorJuego.magiaAzul = ControladorJuego.magiaMorada
                 = ControladorJuego.magiaNaranja = ControladorJuego.magiaNegra = ControladorJuego.magiaVerde = true;
    }

    public void DesbloquearMagiaRoja() { ControladorJuego.magiaRoja = true; }
    public void DesbloquearMagiaAzul() { ControladorJuego.magiaAzul = true; }
    public void DesbloquearMagiaAmarilla() { ControladorJuego.magiaAmarilla = true; }
    public void DesbloquearMagiaMorada() { ControladorJuego.magiaMorada = true; }
    public void DesbloquearMagiaNaranja() { ControladorJuego.magiaNaranja = true; }
    public void DesbloquearMagiaVerde() { ControladorJuego.magiaVerde = true; }
    public void DesbloquearMagiaNegra() { ControladorJuego.magiaNegra = true; }


    #endregion

    #region Calculo Daño

    public int CalculoAtaqueJugador(int _ataque, MagiaColor _colorAtaque, MagiaColor _colorDefenza)
    {
        //Mismo color lo daña mucho
        if (_colorAtaque == _colorDefenza) return _ataque * 3;

        //Color opuesto lo cura mucho
        if (_colorAtaque == ObtenerMagiaColorOpuesto(_colorDefenza)) return _ataque * -2;

        if (_colorDefenza != MagiaColor.blanco)
        {
            //Color complementario del opuesto lo cura poco
            if (_colorDefenza == MagiaColor.negro || _colorAtaque == ObtenerMagiaColorComplementario(ObtenerMagiaColorOpuesto(_colorDefenza), true) || _colorAtaque == ObtenerMagiaColorComplementario(ObtenerMagiaColorOpuesto(_colorDefenza), false))
                return _ataque * -1;
        }
        //Color complementario
        return _ataque;

        /* Lo daña Color Opuesto
        
        if (_colorAtaque == _colorDefenza)
            _ataque = _ataque * -2;
        else
        {
            switch (_colorAtaque)
            {
                case MagiaColor.blanco:
                    if (_colorDefenza == MagiaColor.negro)
                        _ataque = _ataque * 4;
                    else
                        _ataque = _ataque / 2;
                    break;

                case MagiaColor.rojo:
                    if (_colorDefenza == MagiaColor.verde)
                        _ataque = _ataque * 3;
                    else if (_colorDefenza != MagiaColor.azul && _colorDefenza != MagiaColor.amarillo)
                        _ataque = _ataque * -1;
                    break;

                case MagiaColor.azul:
                    if (_colorDefenza == MagiaColor.naranja)
                        _ataque = _ataque * 3;
                    else if (_colorDefenza != MagiaColor.rojo && _colorDefenza != MagiaColor.amarillo)
                        _ataque = _ataque * -1;
                    break;

                case MagiaColor.amarillo:
                    if (_colorDefenza == MagiaColor.morado)
                        _ataque = _ataque * 2;
                    else if (_colorDefenza != MagiaColor.rojo && _colorDefenza != MagiaColor.azul)
                        _ataque = _ataque * -1;
                    break;

                case MagiaColor.morado:
                    if (_colorDefenza == MagiaColor.amarillo)
                        _ataque = _ataque * 3;
                    else if (_colorDefenza != MagiaColor.naranja && _colorDefenza != MagiaColor.verde)
                        _ataque = _ataque * -1;
                    break;

                case MagiaColor.naranja:
                    if (_colorDefenza == MagiaColor.azul)
                        _ataque = _ataque * 3;
                    else if (_colorDefenza != MagiaColor.verde && _colorDefenza != MagiaColor.morado)
                        _ataque = _ataque * -1;
                    break;

                case MagiaColor.verde:
                    if (_colorDefenza == MagiaColor.rojo)
                        _ataque = _ataque * 3;
                    else if (_colorDefenza != MagiaColor.naranja && _colorDefenza != MagiaColor.morado)
                        _ataque = _ataque * -1;
                    break;

                case MagiaColor.negro:
                    _ataque = _ataque / 2;
                    if (_colorDefenza == MagiaColor.blanco)
                        _ataque = _ataque * 4;
                    break;
            }
        }
        */
    }


    public int CalculoAtaqueEnemigo(int _ataque, MagiaColor _colorAtaque, bool _colision)
    {
        MagiaColor _colorDefenza = ControladorJuego.jugadorAcceso.magiaColor;

        //Mismo color lo daña mucho
        if (_colorAtaque == _colorDefenza) return _ataque * 2;

        //Color opuesto lo cura mucho
        if (_colorAtaque == ObtenerMagiaColorOpuesto(_colorDefenza)) return _ataque * (_colision ? 1 : 0);

        if (_colorDefenza != MagiaColor.blanco)
        {
            if (_colorDefenza == MagiaColor.negro)
            {
                return _ataque / 2;
            }
            //Color complementario del opuesto lo cura poco
            if (_colorAtaque == ObtenerMagiaColorComplementario(ObtenerMagiaColorOpuesto(_colorDefenza), true) || _colorAtaque == ObtenerMagiaColorComplementario(ObtenerMagiaColorOpuesto(_colorDefenza), false))
                return _ataque * (_colision ? 1 : 0);

        }
        //Color complementario lo daña poco
        return _ataque;

        /*

            if (_colorAtaque == _colorDefenza)
                _ataque = _colision ? 1 : 0;
            else
            {
                switch (_colorAtaque)
                {
                    case MagiaColor.rojo:
                        if (_colorDefenza == MagiaColor.naranja || _colorDefenza == MagiaColor.morado)
                            _ataque = _colision ? 1 : 0;
                        break;
                    case MagiaColor.azul:
                        if (_colorDefenza == MagiaColor.verde || _colorDefenza == MagiaColor.morado)
                            _ataque = _colision ? 1 : 0;
                        break;
                    case MagiaColor.amarillo:
                        if (_colorDefenza == MagiaColor.verde || _colorDefenza == MagiaColor.naranja)
                            _ataque = _colision ? 1 : 0;
                        break;
                    case MagiaColor.morado:
                        if (_colorDefenza == MagiaColor.rojo || _colorDefenza == MagiaColor.azul)
                            _ataque = _colision ? 1 : 0;
                        break;
                    case MagiaColor.naranja:
                        if (_colorDefenza == MagiaColor.rojo || _colorDefenza == MagiaColor.amarillo)
                            _ataque = _colision ? 1 : 0;
                        break;
                    case MagiaColor.verde:
                        if (_colorDefenza == MagiaColor.amarillo || _colorDefenza == MagiaColor.azul)
                            _ataque = _colision ? 1 : 0;
                        break;
                }
            }
            return _ataque;
        */
    }
    #endregion
}

public enum MagiaColor
{
    blanco,
    rojo,
    azul,
    amarillo,
    morado,
    naranja,
    verde,
    negro,
    sinCOLOR,
}


