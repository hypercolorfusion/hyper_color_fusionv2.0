using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SistemaArea : MonoBehaviour
{
    public List<Area> areasGeneradas;
    public GameObject area;
    public GameObject areaGrabada;//Prefab con bools en portales para setearse manualmente /Info de carga
    public Transform organizadorArea;
    public int caminosDisponibles = 1;

    void Awake()
    {
        if (ControladorJuego.adminJuego.ObtenerSistemaArea() == null)
        {
            ControladorJuego.adminJuego.SetearSistemaArea(this);
            DontDestroyOnLoad(transform.parent);
        }
        else Destroy(gameObject);

    }

    public void ReiniciarLista()
    {
        areasGeneradas.Clear();
        Debug.Log("Se destruyeron las areas generadas");
    }
    public int ObtenerAreasGeneradas()
    {
        return areasGeneradas.Count;
    }
    public Area ObtenerAreaSegunIDJugador()
    {
        foreach (Area _areasEncontradas in ControladorJuego.sistemaArea.areasGeneradas)
        {
            if (_areasEncontradas.ID == ControladorJuego.sistemaArea.ObtenerID_AreaJugadorActual())
            {
                _areasEncontradas.gameObject.SetActive(true);
                return _areasEncontradas;
            }
        }
        Debug.LogWarning("NO SE ENCONTRÓ AREA");
        return null;
    }
    public Area ObtenerAreaSegunID(Vector2 _id)
    {
        foreach (Area _areasEncontradas in ControladorJuego.sistemaArea.areasGeneradas)
        {
            if (_areasEncontradas.ID == _id)
            {
                _areasEncontradas.gameObject.SetActive(true);
                return _areasEncontradas;
            }
        }
        Debug.LogWarning("NO SE ENCONTRÓ AREA");
        return null;
    }

    public Portal ObtenerPortalSegunAreaId(Vector2 _id, PortalDir _direccion)
    {
        foreach (Portal _portal in ObtenerAreaSegunID(_id).portales)
            if (_direccion == _portal.direccion) return _portal;
        Debug.LogError("No se encontró ningun Portal");
        return null;
    }

    public int contAreas() { return areasGeneradas.Count; }
    public void AgregarArea(Area _area) { areasGeneradas.Add(_area); }
    public void SetearID_AreaJugadorActual(Vector2 _ID) { ControladorJuego.areaIDActual = _ID; }
    public Vector2 ObtenerID_AreaJugadorActual() { return ControladorJuego.areaIDActual; }

    public void VerificarPosicionArea(Vector2 _ID, Portal _portal)
    {
        for (int i = 0; i < areasGeneradas.Count; i++)
        {
            if (areasGeneradas[i].ID == _ID)
            {
                _portal.BloquearPortal();
                switch (_portal.direccion)
                {
                    case PortalDir.der:
                        foreach (Portal _subPortal in areasGeneradas[i].portales)
                        {
                            if (_subPortal.direccion == PortalDir.izq)
                            {
                                _portal.destino = _subPortal.transform;
                                _subPortal.destino = _portal.transform;
                                _portal.VerificarColor();
                            }
                        }
                        break;
                    case PortalDir.izq:
                        foreach (Portal _subPortal in areasGeneradas[i].portales)
                        {
                            if (_subPortal.direccion == PortalDir.der)
                            {
                                _portal.destino = _subPortal.transform;
                                _subPortal.destino = _portal.transform;
                                _portal.VerificarColor();
                            }
                        }
                        break;
                    case PortalDir.arr:
                        foreach (Portal _subPortal in areasGeneradas[i].portales)
                        {
                            if (_subPortal.direccion == PortalDir.abj)
                            {
                                _portal.destino = _subPortal.transform;
                                _subPortal.destino = _portal.transform;
                                _portal.VerificarColor();
                            }
                        }
                        break;
                    case PortalDir.abj:
                        foreach (Portal _subPortal in areasGeneradas[i].portales)
                        {
                            if (_subPortal.direccion == PortalDir.arr)
                            {
                                _portal.destino = _subPortal.transform;
                                _subPortal.destino = _portal.transform;
                                _portal.VerificarColor();
                            }
                        }
                        break;
                }
            }
        }
    }
}
