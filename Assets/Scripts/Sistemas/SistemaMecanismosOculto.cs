using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SistemaMecanismosOculto : MonoBehaviour
{
    public Mecanismos[] mecanismosTutorial;
    public Mecanismos[] mecanismos;
    public Mecanismos[] altares;


    [HideInInspector]
    public List<GameObject> mecanismosGenerados = new List<GameObject>();
    [HideInInspector]
    public List<GameObject> altaresgenerados = new List<GameObject>();


    void Awake()
    {
        if (ControladorJuego.adminJuego.ObtenerSistemaMecanismoOculto() == null)
        {
            ControladorJuego.adminJuego.SetearSistemaMecanismoOculto(this);
            DontDestroyOnLoad(transform.parent);
        }
        else
            Destroy(gameObject);
    }
    // Start is called before the first frame update

    public void InicializarMecanismos()
    {
        for (int i = 0; i < mecanismosTutorial.Length; i++)
        {
            GameObject _nuevoMecanismo = Instantiate(mecanismosTutorial[i].objeto).gameObject;
            _nuevoMecanismo.transform.SetParent(ControladorJuego.sistemaArea.ObtenerAreaSegunID(mecanismosTutorial[i].area_ID).transform);
            _nuevoMecanismo.transform.localPosition = mecanismosTutorial[i].posicion;
            _nuevoMecanismo.name = mecanismosTutorial[i].nombre;

            if (mecanismosTutorial[i].oculto) _nuevoMecanismo.SetActive(false);
            if (_nuevoMecanismo.GetComponent<MecanismoOculto>())
                _nuevoMecanismo.GetComponent<MecanismoOculto>().oculto = mecanismosTutorial[i].oculto;

            if (_nuevoMecanismo.GetComponent<ActivadorObjeto>())
                _nuevoMecanismo.GetComponent<ActivadorObjeto>().Asignar();

            mecanismosGenerados.Add(_nuevoMecanismo);
        }

        for (int i = 0; i < mecanismos.Length; i++)
        {
            GameObject _nuevoMecanismo = Instantiate(mecanismos[i].objeto).gameObject;
            _nuevoMecanismo.transform.SetParent(ControladorJuego.sistemaArea.ObtenerAreaSegunID(mecanismos[i].area_ID).transform);
            _nuevoMecanismo.transform.localPosition = mecanismos[i].posicion;
            mecanismos[i].ID = i;
            _nuevoMecanismo.name = mecanismos[i].nombre + " " + mecanismos[i].ID;

            if (mecanismosTutorial[i].oculto) _nuevoMecanismo.SetActive(false);
            if (_nuevoMecanismo.GetComponent<MecanismoOculto>())
                _nuevoMecanismo.GetComponent<MecanismoOculto>().oculto = mecanismos[i].oculto;
            if (_nuevoMecanismo.GetComponent<ActivadorObjeto>())
                _nuevoMecanismo.GetComponent<ActivadorObjeto>().Asignar();

            mecanismosGenerados.Add(_nuevoMecanismo);
        }

        for (int i = 0; i < altares.Length; i++)
        {
            GameObject _nuevoAltar = Instantiate(altares[i].objeto).gameObject;
            _nuevoAltar.transform.SetParent(ControladorJuego.sistemaArea.ObtenerAreaSegunID(altares[i].area_ID).transform);
            _nuevoAltar.transform.localPosition = altares[i].posicion;
            altares[i].ID = i;
            _nuevoAltar.name = altares[i].nombre + " " + altares[i].ID;

            if (altares[i].oculto) _nuevoAltar.GetComponent<MecanismoOculto>().oculto = true;
            altaresgenerados.Add(_nuevoAltar);
        }
    }
    public void RecargaMecanismosGenerados()
    {
        for (int i = 0; i < mecanismosGenerados.Count; i++)
        {
            if (mecanismosGenerados[i])
                Destroy(mecanismosGenerados[i]);
        }
        for (int i = 0; i < altaresgenerados.Count; i++)
        {
            if (altaresgenerados[i])
                Destroy(altaresgenerados[i]);
        }
        mecanismosGenerados.Clear();
        altaresgenerados.Clear();
    }

    public Mecanismos ObtenerMecanismo(string _nombre)
    {
        foreach (Mecanismos _mecanismo in mecanismosTutorial)
        {
            if (_mecanismo.nombre == _nombre)
                return _mecanismo;
        }
        foreach (Mecanismos _mecanismo in mecanismos)
        {
            if (_mecanismo.nombre == _nombre)
                return _mecanismo;
        }
        Debug.Log(_nombre);
        Debug.LogError("No se ha encontrado mecanismo");
        return mecanismosTutorial[0];
    }

}

[System.Serializable]
public struct Mecanismos
{
    public string nombre;
    public GameObject objeto;
    public bool oculto;
    //[HideInInspector]
    public bool forzarAparicion;
    [HideInInspector]
    public int ID;
    public Vector2 area_ID;
    public Vector3 posicion;
}
