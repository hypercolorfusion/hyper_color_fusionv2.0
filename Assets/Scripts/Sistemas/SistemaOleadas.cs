using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SistemaOleadas : MonoBehaviour
{
    public Oleadas[] oleadaEnemigos;
    public Vector3 ubicacion;
    public bool activo;
    public List<GameObject> enemigosObjetivo;

    void Awake()
    {
        if (ControladorJuego.adminJuego.ObtenersistemaOleada() == null)
        {
            ControladorJuego.adminJuego.SetearSistemaOleada(this);
            DontDestroyOnLoad(transform.parent);
        }
        else Destroy(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        if (!activo) return;

        if (enemigosObjetivo.Count <= 0) DesactivarOleada();
    }

    public void ActivarOleada(Portal _portalUsado)
    {
        //Activa oledas opuestas al portal el cual el jugado entró
        Area _areaActual = ControladorJuego.sistemaArea.ObtenerAreaSegunIDJugador();
        if (!_areaActual.bloquearArea)
        {
            ubicacion = new Vector3(_areaActual.ID.x, _areaActual.ID.y, 0f);
            foreach (Portal _portalesEncontrados in _areaActual.portales) _portalesEncontrados.Desactivar();
            SeleccionarColorOleada(_portalUsado, _areaActual);
            activo = true;
        }
    }

    public void ActivarOleada(MagiaColor _color)
    {
        //Activa oledas en cada portal de la habitación (Generalmente lo activa un NPC en un habitación)
        Area _areaActual = ControladorJuego.sistemaArea.ObtenerAreaSegunIDJugador();
        if (!_areaActual.bloquearArea)
        {
            ubicacion = new Vector3(_areaActual.ID.x, _areaActual.ID.y, 0f);
            foreach (Portal _portalesEncontrados in _areaActual.portales) _portalesEncontrados.Desactivar();
            SelecionarColorOleadaCentro(_color, _areaActual);
            activo = true;
        }
    }

    public void DesactivarOleada()
    {
        if (ControladorJuego.sistemaMisiones.tutorial)
        {
            if (ControladorJuego.sistemaMisiones.VerificarMisionInicializadaTutorial("PRIMERA_OLEADA"))
                ControladorJuego.sistemaMisiones.CompletarMisionTutorial("PRIMERA_OLEADA");

            if (ControladorJuego.sistemaMisiones.VerificarMisionInicializadaTutorial("CAMBIO_COLOR_AZUL"))
                ControladorJuego.sistemaMisiones.CompletarMisionTutorial("CAMBIO_COLOR_AZUL");

            if (ControladorJuego.sistemaMisiones.VerificarMisionCompletadaTutorial("CAMBIO_COLOR_ROJO") && !ControladorJuego.sistemaMisiones.VerificarMisionCompletadaTutorial("CAMBIO_COLOR_AZUL"))
            {
                ControladorJuego.sistemaMisiones.IniciarMisionTutorial("CAMBIO_COLOR_AZUL");
                ControladorJuego.sistemaMagiaColor.DesbloquearMagiaAzul();
                ControladorJuego.sistemaExperiencia.SubirNivelAzul();
                ControladorJuego.sistemaMensajes.MostarTexto("TUTORIAL", "COLOR_AZUL", "AYUDA", true);
                ControladorJuego.adminUi.MostrarColorPlayer();
            }
            //CAMBIO_COLOR_MORADO
        }
        ControladorJuego.sistemaPool.ForzarOcultarElementos();
        foreach (Portal _portalesEncontrados in ControladorJuego.sistemaArea.ObtenerAreaSegunIDJugador().portales) _portalesEncontrados.ActivarPortal();
        activo = false;
    }
    public void EliminarOleada()
    {
        for (int i = 0; i < enemigosObjetivo.Count; i++)
        {
            if (enemigosObjetivo[i]) Destroy(enemigosObjetivo[i]);
        }
        enemigosObjetivo.Clear();
        ControladorJuego.sistemaPool.ForzarOcultarElementos();
        activo = false;
    }

    private void SeleccionarColorOleada(Portal _portalUsado, Area _areaActual)
    {
        int _probabilidad = Random.Range(0, 101);
        Oleada((int)_portalUsado.magiaColor, _probabilidad, _areaActual, _portalUsado.direccion, false);
    }


    private void SelecionarColorOleadaCentro(MagiaColor _color, Area _areaActual)
    {
        int _probabilidad = Random.Range(0, 101);
        Oleada((int)_color, _probabilidad, _areaActual, PortalDir.abj, true);
    }


    private void Oleada(int colorOleada, int _probabilidad, Area _areaActual, PortalDir _entrada, bool _centro)
    {
        int _nivelJugadorObtenido = ControladorJuego.sistemaExperiencia.nivelJugador;
        int _selectorGrupo;
        int _selectorOleada;

        if (ControladorJuego.tutorial)
        {
            _selectorGrupo = 0;
            if (colorOleada == 0 && ControladorJuego.magiaNaranja)
                _selectorOleada = 1;
            else
                _selectorOleada = 0;
        }
        else
        {
            _selectorGrupo = _nivelJugadorObtenido / 10;
            _selectorOleada = Random.Range(0, oleadaEnemigos[colorOleada].grupo[_selectorGrupo].oleada.Length);
        }

        OleadaDireccion _componente = oleadaEnemigos[colorOleada].grupo[_selectorGrupo].oleada[_selectorOleada].direccion[(int)PortalDir.centro];

        if (_componente.activo)
            for (int i = 0; i < _componente.enemigos.Length; i++)
                CrearEnemigo(_componente.enemigos[i], _areaActual.transform, _areaActual);


        foreach (Portal _portalEncotnrado in _areaActual.portales)
        {
            if (_portalEncotnrado.ObtenerPortalOpuesto() != _entrada || _centro)
            {
                _componente = oleadaEnemigos[colorOleada].grupo[_selectorGrupo].oleada[_selectorOleada].direccion[(int)_portalEncotnrado.direccion];
                if (_componente.activo)
                    for (int i = 0; i < _componente.enemigos.Length; i++)
                        CrearEnemigo(_componente.enemigos[i], _portalEncotnrado.transform, _areaActual);
            }
        }
    }

    public void CrearEnemigo(GameObject _enemigo, Transform ubicacion, Area _areaActual)
    {
        _enemigo = Instantiate(_enemigo, ubicacion.position, transform.rotation);
        AgregarEnemigosObjetivos(_enemigo, _areaActual);
    }

    public void AgregarEnemigosObjetivos(GameObject _enemigo, Area _areaActual)
    {
        enemigosObjetivo.Add(_enemigo);
        ControladorJuego.sistemaArea.ObtenerAreaSegunIDJugador();
        _enemigo.transform.SetParent(_areaActual.transform);
    }

    public void AgregarEnemigosObjetivos(GameObject _enemigo)
    {
        _enemigo.name = _enemigo.name + enemigosObjetivo.Count;
        enemigosObjetivo.Add(_enemigo);
        ControladorJuego.sistemaArea.ObtenerAreaSegunIDJugador();
        _enemigo.transform.SetParent(ControladorJuego.sistemaArea.ObtenerAreaSegunIDJugador().transform);
    }
}

[System.Serializable]
public struct Oleadas
{
    public string nombre;
    public SubGrupoOleada[] grupo;
}
[System.Serializable]
public struct SubGrupoOleada
{
    public string nombre;
    public NombreOleada[] oleada;
}

[System.Serializable]
public struct NombreOleada
{
    public string nombre;
    public OleadaDireccion[] direccion;
}

[System.Serializable]
public struct OleadaDireccion
{
    public string nombre;
    public bool activo;
    public GameObject[] enemigos;
}


