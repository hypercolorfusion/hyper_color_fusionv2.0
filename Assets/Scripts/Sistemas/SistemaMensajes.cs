using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class SistemaMensajes : MonoBehaviour
{
    public LibreriaFrase[] libreria;
    public GameObject panelTexto;
    public Text dialogo;
    public Text instrucciones;
    public Text nombre;
    public int codigo;
    public int subCodigo;
    public int indiceTexto = 0;

    void Awake()
    {
        if (ControladorJuego.adminUi.ObtenerSistemaMensajes() == null)
        {
            ControladorJuego.adminUi.SetearSistemaMensajes(this);
            DontDestroyOnLoad(transform.parent);
        }
        else Destroy(gameObject);
    }

    public void SiguienteMensaje()
    {
        indiceTexto++;

        if (indiceTexto < libreria[codigo].frases[subCodigo].cantFrases.Length)
        {
            dialogo.text = libreria[codigo].frases[subCodigo].cantFrases[indiceTexto].frase;
            if (libreria[codigo].frases[subCodigo].cantFrases[indiceTexto].activador)
                libreria[codigo].frases[subCodigo].cantFrases[indiceTexto].objetoActivar.SetActive(true);
        }
        else
        {
            libreria[codigo].frases[subCodigo].hablado = true;
            CerrarTexto();
        }
    }


    public void MostarTexto(string _codigo, string _subCodigo, string _nombre, bool _automatico)
    {
        nombre.text = _nombre + ":";
        indiceTexto = 0;
        for (int i = 0; i < libreria.Length; i++)
        {

            if (libreria[i].codigo == _codigo)
            {
                codigo = i;
                for (int j = 0; j < libreria[i].frases.Length; j++) if (libreria[i].frases[j].subCodigo == _subCodigo) subCodigo = j;
            }
        }


        bool _hablado = libreria[codigo].frases[subCodigo].hablado;
        if (libreria[codigo].frases[subCodigo].unicaVez && _hablado) return;

        if (_hablado) indiceTexto = libreria[codigo].frases[subCodigo].cantFrases.Length - 2;
        else indiceTexto = -1;

        if (_automatico) SiguienteMensaje();
        panelTexto.SetActive(true);
        ControladorJuego.adminUi.pausaPorMensaje = true;
        ControladorJuego.adminJuego.PauseGame();
    }

    public void AsignarActivador(string _codigo, string _subCodigo, int _indice, GameObject _objeto)
    {
        for (int i = 0; i < libreria.Length; i++)
            if (libreria[i].codigo == _codigo)
                for (int j = 0; j < libreria[i].frases.Length; j++)
                    if (libreria[i].frases[j].subCodigo == _subCodigo)
                        libreria[i].frases[j].cantFrases[_indice].objetoActivar = _objeto;
    }

    public void CerrarTexto()
    {
        ControladorJuego.adminUi.pausaPorMensaje = false;
        panelTexto.SetActive(false);
        ControladorJuego.adminJuego.ResumeGame();
    }
}

[System.Serializable]
public struct LibreriaFrase
{
    public string codigo;
    public LibreriaSubFase[] frases;
}


[System.Serializable]
public struct LibreriaSubFase
{
    public string subCodigo;
    public bool hablado;
    public bool unicaVez;
    public Frase[] cantFrases;

}

[System.Serializable]
public struct Frase
{
    [TextArea(3, 10)]
    public string frase;
    public bool activador;
    [HideInInspector]
    public GameObject objetoActivar;

}
