using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SistemaGuardado : MonoBehaviour
{

    public int slot;
    public bool cargado = false;

    //public GameObject areaGuarada;


    void Awake()
    {
        if (ControladorJuego.adminData.ObtenersistemaGuardado() == null)
        {
            ControladorJuego.adminData.SetearSistemaGuardado(this);
            DontDestroyOnLoad(transform.parent); //Revisar esto, con solo un sistema es suficiente
        }
        else Destroy(gameObject);
    }

    public void CargarJuego()
    {
        if (slot == 0)
        {
            Debug.LogWarning("Debido a que no se ha escogido un slot, se usará el slot numero 3 para cualquier cargado");
            slot = 3;
        }
        Debug.Log("Cargando el slot: " + slot);
        cargado = true;
        ControladorJuego.sistemaMecanismosOculto.RecargaMecanismosGenerados();
        ControladorJuego.sistemaOleadas.EliminarOleada();
        ControladorJuego.sistemaPool.ForzarOcultarBarradeVida();
        CargarJugador();
        CargarAreasGeneradas();
        CargarMisiones();
        CargarMensajesLeidos();
        CargarMecanismos();
        ControladorJuego.jugadorAcceso.InicializarJugador();
    }

    public void GuardarPartida()
    {
        if (slot == 0)
        {
            Debug.LogWarning("Debido a que no se ha escogido un slot, se usará el slot numero 3 para cualquier guardado");
            slot = 3;
        }
        if (!ControladorJuego.sistemaGuardado.VerificarSlot(slot)) GuardarSot(slot);
        GuardarJugador();
        GuardarAreasGeneradas();
        GuardarMisiones();
        GuardarMensajesLeidos();
        GuardarMecanismos();
    }

    #region Areas
    public void GuardarAreasGeneradas()
    {
        Dictionary<string, bool> _dicAreasGeneradasEstados = new Dictionary<string, bool>();
        Dictionary<string, float> _dicAreasGeneradasValores = new Dictionary<string, float>();

        _dicAreasGeneradasValores.Add(slot + "AreasGeneradas", ControladorJuego.sistemaArea.areasGeneradas.Count);

        for (int a = 0; a < ControladorJuego.sistemaArea.areasGeneradas.Count; a++)
        {
            _dicAreasGeneradasValores.Add(slot + "Area.x" + a, ControladorJuego.sistemaArea.areasGeneradas[a].ID.x);
            _dicAreasGeneradasValores.Add(slot + "Area.y" + a, ControladorJuego.sistemaArea.areasGeneradas[a].ID.y);

            _dicAreasGeneradasValores.Add(slot + "Area" + a + "CantHuecos", ControladorJuego.sistemaArea.areasGeneradas[a].huecos.Count);

            _dicAreasGeneradasEstados.Add(slot + "Area" + a + "activo", ControladorJuego.sistemaArea.areasGeneradas[a].gameObject.activeSelf);
            _dicAreasGeneradasEstados.Add(slot + "Area" + a + "GenerarHueco", ControladorJuego.sistemaArea.areasGeneradas[a].generarHueco);
            _dicAreasGeneradasEstados.Add(slot + "Area" + a + "BloquearArea", ControladorJuego.sistemaArea.areasGeneradas[a].bloquearArea);
            _dicAreasGeneradasEstados.Add(slot + "Area" + a + "UnicaVez", ControladorJuego.sistemaArea.areasGeneradas[a].unicaVez);

            for (int h = 0; h < ControladorJuego.sistemaArea.areasGeneradas[a].huecos.Count; h++)
            {

                _dicAreasGeneradasValores.Add(slot + "Area" + a + "HuecoPos.x" + h, ControladorJuego.sistemaArea.areasGeneradas[a].huecos[h].transform.position.x);
                _dicAreasGeneradasValores.Add(slot + "Area" + a + "HuecoPos.y" + h, ControladorJuego.sistemaArea.areasGeneradas[a].huecos[h].transform.position.y);
                _dicAreasGeneradasValores.Add(slot + "Area" + a + "HuecoPos.z" + h, ControladorJuego.sistemaArea.areasGeneradas[a].huecos[h].transform.position.z);
                _dicAreasGeneradasValores.Add(slot + "Area" + a + "HuecoEscala.x" + h, ControladorJuego.sistemaArea.areasGeneradas[a].huecos[h].transform.localScale.x);
                _dicAreasGeneradasValores.Add(slot + "Area" + a + "HuecoEscala.y" + h, ControladorJuego.sistemaArea.areasGeneradas[a].huecos[h].transform.localScale.y);
                _dicAreasGeneradasValores.Add(slot + "Area" + a + "HuecoEscala.z" + h, ControladorJuego.sistemaArea.areasGeneradas[a].huecos[h].transform.localScale.z);
                _dicAreasGeneradasValores.Add(slot + "Area" + a + "HuecoRot.x" + h, ControladorJuego.sistemaArea.areasGeneradas[a].huecos[h].transform.rotation.x);
                _dicAreasGeneradasValores.Add(slot + "Area" + a + "HuecoRot.y" + h, ControladorJuego.sistemaArea.areasGeneradas[a].huecos[h].transform.rotation.y);
                _dicAreasGeneradasValores.Add(slot + "Area" + a + "HuecoRot.z" + h, ControladorJuego.sistemaArea.areasGeneradas[a].huecos[h].transform.rotation.z);
                _dicAreasGeneradasValores.Add(slot + "Area" + a + "HuecoRot.w" + h, ControladorJuego.sistemaArea.areasGeneradas[a].huecos[h].transform.rotation.w);
            }
            for (int p = 0; p < ControladorJuego.sistemaArea.areasGeneradas[a].portales.Count; p++)
            {
                _dicAreasGeneradasEstados.Add(slot + "Area" + a + "PortalBloqueado" + p, ControladorJuego.sistemaArea.areasGeneradas[a].portales[p].bloqueado);
                _dicAreasGeneradasValores.Add(slot + "Area" + a + "PortalColor" + p, (int)ControladorJuego.sistemaArea.areasGeneradas[a].portales[p].magiaColor);
                _dicAreasGeneradasEstados.Add(slot + "Area" + a + "PortalConeccion" + p, ControladorJuego.sistemaArea.areasGeneradas[a].portales[p].destino);
            }
        }
        _dicAreasGeneradasValores.Add(slot + "AreaGuardada.x", ControladorJuego.sistemaArea.ObtenerID_AreaJugadorActual().x);
        _dicAreasGeneradasValores.Add(slot + "AreaGuardada.y", ControladorJuego.sistemaArea.ObtenerID_AreaJugadorActual().y);

        _dicAreasGeneradasEstados.Add(slot + "Mapa Interactuable", ControladorJuego.sistemaMapa.mapaInteractuable);

        ES3.Save<Dictionary<string, bool>>(slot + "DiccionarioAreasGeneradasEstados", _dicAreasGeneradasEstados);
        ES3.Save<Dictionary<string, float>>(slot + "DiccionarioAreasGeneradasValores", _dicAreasGeneradasValores);


        /*
        ES3.Save<int>(slot + "AreasGeneradas", ControladorJuego.sistemaArea.areasGeneradas.Count);
        for (int a = 0; a < ControladorJuego.sistemaArea.areasGeneradas.Count; a++)
        {
            ES3.Save<Vector2>(slot + "Area" + a, ControladorJuego.sistemaArea.areasGeneradas[a].ID);
            ES3.Save<int>(slot + "Area" + a + "CantHuecos", ControladorJuego.sistemaArea.areasGeneradas[a].huecos.Count);

            ES3.Save<bool>(slot + "Area" + a + "activo", ControladorJuego.sistemaArea.areasGeneradas[a].gameObject.activeSelf);
            ES3.Save<bool>(slot + "Area" + a + "GenerarHueco", ControladorJuego.sistemaArea.areasGeneradas[a].generarHueco);
            ES3.Save<bool>(slot + "Area" + a + "BloquearArea", ControladorJuego.sistemaArea.areasGeneradas[a].bloquearArea);
            ES3.Save<bool>(slot + "Area" + a + "UnicaVez", ControladorJuego.sistemaArea.areasGeneradas[a].unicaVez);

            for (int h = 0; h < ControladorJuego.sistemaArea.areasGeneradas[a].huecos.Count; h++)
            {
                ES3.Save<Vector3>(slot + "Area" + a + "HuecoPos" + h, ControladorJuego.sistemaArea.areasGeneradas[a].huecos[h].transform.position);
                ES3.Save<Vector3>(slot + "Area" + a + "HuecoEscala" + h, ControladorJuego.sistemaArea.areasGeneradas[a].huecos[h].transform.localScale);
                ES3.Save<Quaternion>(slot + "Area" + a + "HuecoRot" + h, ControladorJuego.sistemaArea.areasGeneradas[a].huecos[h].transform.rotation);
            }
            for (int p = 0; p < ControladorJuego.sistemaArea.areasGeneradas[a].portales.Count; p++)
            {
                ES3.Save<bool>(slot + "Area" + a + "PortalBloqueado" + p, ControladorJuego.sistemaArea.areasGeneradas[a].portales[p].bloqueado);
                ES3.Save<int>(slot + "Area" + a + "PortalColor" + p, (int)ControladorJuego.sistemaArea.areasGeneradas[a].portales[p].magiaColor);
                ES3.Save<bool>(slot + "Area" + a + "PortalConeccion" + p, ControladorJuego.sistemaArea.areasGeneradas[a].portales[p].destino);
            }
        }
        ES3.Save<Vector2>(slot + "AreaGuardada", ControladorJuego.sistemaArea.ObtenerID_AreaJugadorActual());
        ES3.Save<bool>(slot + "Mapa Interactuable", ControladorJuego.sistemaMapa.mapaInteractuable);
        */
        Debug.Log("Se ha guardado correctamente la información del mapa");

    }

    public void CargarAreasGeneradas()
    {

        Dictionary<string, bool> _dicAreasGeneradasEstados = ES3.Load<Dictionary<string, bool>>(slot + "DiccionarioAreasGeneradasEstados");
        Dictionary<string, float> _dicAreasGeneradasValores = ES3.Load<Dictionary<string, float>>(slot + "DiccionarioAreasGeneradasValores");

        //Borrar areas existentes y limpiar lista
        ControladorJuego.sistemaMapa.EliminarMapa();
        foreach (Area _area in ControladorJuego.sistemaArea.areasGeneradas)
        {
            if (_area.gameObject) Destroy(_area.gameObject);
        }
        ControladorJuego.sistemaArea.ReiniciarLista();
        //Generar nueas areas y portales

        for (int a = 0; a < _dicAreasGeneradasValores[slot + "AreasGeneradas"]; a++)
        {
            GameObject _areaCreada = Instantiate(ControladorJuego.sistemaArea.areaGrabada);
            Area _componente = _areaCreada.GetComponent<Area>();
            int _cantHuecos = (int)_dicAreasGeneradasValores[slot + "Area" + a + "CantHuecos"];

            _componente.InicializadorEstados(_dicAreasGeneradasEstados[slot + "Area" + a + "activo"],
           _dicAreasGeneradasEstados[slot + "Area" + a + "GenerarHueco"],
            //ES3.Load<bool>(slot + "Area" + a + "ForzarInicializador"),
            _dicAreasGeneradasEstados[slot + "Area" + a + "BloquearArea"],
            _dicAreasGeneradasEstados[slot + "Area" + a + "UnicaVez"]);

            if (_cantHuecos >= 1)
            {
                Vector3[] _huecoPos = new Vector3[_cantHuecos];
                Vector3[] _huecoEscala = new Vector3[_cantHuecos];
                Quaternion[] _huecoRot = new Quaternion[_cantHuecos];
                for (int h = 0; h < _cantHuecos; h++)
                {
                    _huecoPos[h] = new Vector3(_dicAreasGeneradasValores[slot + "Area" + a + "HuecoPos.x" + h],
                    _dicAreasGeneradasValores[slot + "Area" + a + "HuecoPos.y" + h],
                    _dicAreasGeneradasValores[slot + "Area" + a + "HuecoPos.z" + h]);
                    _huecoEscala[h] = new Vector3(_dicAreasGeneradasValores[slot + "Area" + a + "HuecoEscala.x" + h],
                    _dicAreasGeneradasValores[slot + "Area" + a + "HuecoEscala.y" + h],
                    _dicAreasGeneradasValores[slot + "Area" + a + "HuecoEscala.z" + h]);
                    _huecoRot[h] = new Quaternion(_dicAreasGeneradasValores[slot + "Area" + a + "HuecoRot.x" + h],
                    _dicAreasGeneradasValores[slot + "Area" + a + "HuecoRot.y" + h],
                    _dicAreasGeneradasValores[slot + "Area" + a + "HuecoRot.z" + h],
                    _dicAreasGeneradasValores[slot + "Area" + a + "HuecoRot.w" + h]);
                }

                _componente.InicializadorManual(new Vector2(_dicAreasGeneradasValores[slot + "Area.x" + a],
                _dicAreasGeneradasValores[slot + "Area.y" + a]), _cantHuecos, _huecoEscala, _huecoPos, _huecoRot);
            }
            else
            {
                _componente.InicializadorManual(new Vector2(_dicAreasGeneradasValores[slot + "Area.x" + a],
                _dicAreasGeneradasValores[slot + "Area.y" + a]));
            }

            for (int p = 0; p < _componente.portales.Count; p++)
            {
                _componente.portales[p].AsignarValoresManualemnte(_dicAreasGeneradasEstados[slot + "Area" + a + "PortalBloqueado" + p],
                (int)_dicAreasGeneradasValores[slot + "Area" + a + "PortalColor" + p], _dicAreasGeneradasEstados[slot + "Area" + a + "PortalConeccion" + p]);
            }
        }
        ControladorJuego.sistemaArea.SetearID_AreaJugadorActual(
        new Vector2(_dicAreasGeneradasValores[slot + "AreaGuardada.x"], _dicAreasGeneradasValores[slot + "AreaGuardada.y"]));

        ControladorJuego.sistemaMapa.ActualizarMapaInteractuable(_dicAreasGeneradasEstados[slot + "Mapa Interactuable"]);

        /*
        //Borrar areas existentes y limpiar lista
        ControladorJuego.sistemaMapa.EliminarMapa();
        foreach (Area _area in ControladorJuego.sistemaArea.areasGeneradas)
        {
            if (_area.gameObject) Destroy(_area.gameObject);
        }
        ControladorJuego.sistemaArea.ReiniciarLista();
        //Generar nueas areas y portales

        for (int a = 0; a < ES3.Load<int>(slot + "AreasGeneradas"); a++)
        {
            GameObject _areaCreada = Instantiate(ControladorJuego.sistemaArea.areaGrabada);
            Area _componente = _areaCreada.GetComponent<Area>();
            int _cantHuecos = ES3.Load<int>(slot + "Area" + a + "CantHuecos");

            _componente.InicializadorEstados(ES3.Load<bool>(slot + "Area" + a + "activo"),
            ES3.Load<bool>(slot + "Area" + a + "GenerarHueco"),
            //ES3.Load<bool>(slot + "Area" + a + "ForzarInicializador"),
            ES3.Load<bool>(slot + "Area" + a + "BloquearArea"),
            ES3.Load<bool>(slot + "Area" + a + "UnicaVez"));

            if (_cantHuecos >= 1)
            {
                Vector3[] _huecoPos = new Vector3[_cantHuecos];
                Vector3[] _huecoEscala = new Vector3[_cantHuecos];
                Quaternion[] _huecoRot = new Quaternion[_cantHuecos];
                for (int h = 0; h < _cantHuecos; h++)
                {
                    _huecoPos[h] = ES3.Load<Vector3>(slot + "Area" + a + "HuecoPos" + h);
                    _huecoEscala[h] = ES3.Load<Vector3>(slot + "Area" + a + "HuecoEscala" + h);
                    _huecoRot[h] = ES3.Load<Quaternion>(slot + "Area" + a + "HuecoRot" + h);
                }
                _componente.InicializadorManual(ES3.Load<Vector2>(slot + "Area" + a),
                 _cantHuecos, _huecoEscala, _huecoPos, _huecoRot);
            }
            else
            {
                _componente.InicializadorManual(ES3.Load<Vector2>(slot + "Area" + a));
            }

            for (int p = 0; p < _componente.portales.Count; p++)
            {
                _componente.portales[p].AsignarValoresManualemnte(ES3.Load<bool>(slot + "Area" + a + "PortalBloqueado" + p),
                 ES3.Load<int>(slot + "Area" + a + "PortalColor" + p), ES3.Load<bool>(slot + "Area" + a + "PortalConeccion" + p));
            }
        }
        ControladorJuego.sistemaArea.SetearID_AreaJugadorActual(ES3.Load<Vector2>(slot + "AreaGuardada"));
        ControladorJuego.sistemaMapa.ActualizarMapaInteractuable(ES3.Load<bool>(slot + "Mapa Interactuable"));
        */

        Debug.Log("Se ha cargado correctamente la información del mapa");
    }
    #endregion

    #region Jugador
    public void GuardarJugador()
    {
        Dictionary<string, bool> _dicJugadorEstados = new Dictionary<string, bool>();
        Dictionary<string, int> _dicJugadorValores = new Dictionary<string, int>();

        _dicJugadorEstados.Add(slot + "J_UI_Rojo", ControladorJuego.jugadorAcceso.rojo);
        _dicJugadorEstados.Add(slot + "J_UI_Azul", ControladorJuego.jugadorAcceso.azul);
        _dicJugadorEstados.Add(slot + "J_UI_Amarillo", ControladorJuego.jugadorAcceso.amarillo);

        _dicJugadorValores.Add(slot + "J_nivel", ControladorJuego.sistemaExperiencia.nivelJugador);
        _dicJugadorValores.Add(slot + "J_nivelBlanco", ControladorJuego.nivelBlanco);
        _dicJugadorValores.Add(slot + "J_nivelRojo", ControladorJuego.nivelRojo);
        _dicJugadorValores.Add(slot + "J_nivelAzul", ControladorJuego.nivelAzul);
        _dicJugadorValores.Add(slot + "J_nivelAmarillo", ControladorJuego.nivelAmarillo);
        _dicJugadorValores.Add(slot + "J_nivelMorado", ControladorJuego.nivelMorado);
        _dicJugadorValores.Add(slot + "J_nivelVerde", ControladorJuego.nivelVerde);
        _dicJugadorValores.Add(slot + "J_nivelNaranja", ControladorJuego.nivelNaranja);
        _dicJugadorValores.Add(slot + "J_nivelNegro", ControladorJuego.nivelNegro);

        _dicJugadorEstados.Add(slot + "J_MagiaRoja", ControladorJuego.magiaRoja);
        _dicJugadorEstados.Add(slot + "J_MagiaAzul", ControladorJuego.magiaAzul);
        _dicJugadorEstados.Add(slot + "J_MagiaAmarilla", ControladorJuego.magiaAmarilla);
        _dicJugadorEstados.Add(slot + "J_MagiaMorado", ControladorJuego.magiaMorada);
        _dicJugadorEstados.Add(slot + "J_MagiaNaranja", ControladorJuego.magiaNaranja);
        _dicJugadorEstados.Add(slot + "J_MagiaVerde", ControladorJuego.magiaVerde);
        _dicJugadorEstados.Add(slot + "J_MagiaNegro", ControladorJuego.magiaNegra);

        _dicJugadorEstados.Add(slot + "PuedeDisparar", ControladorJuego.puedeDisparar);
        _dicJugadorEstados.Add(slot + "MovimientoSecundario", ControladorJuego.movimientoEspecial);
        _dicJugadorEstados.Add(slot + "PuedeCambiarColorOpuesto", ControladorJuego.puedeCambariColorOpuesto);
        _dicJugadorValores.Add(slot + "J_experiencia", ControladorJuego.sistemaExperiencia.expJugador);
        _dicJugadorEstados.Add(slot + "J_tutorial", ControladorJuego.tutorial);
        _dicJugadorValores.Add(slot + "J_magiaColor", (int)ControladorJuego.jugadorAcceso.magiaColor);

        Debug.Log(_dicJugadorValores.Count);
        ES3.Save<Dictionary<string, bool>>(slot + "DiccionarioJugadorEstados", _dicJugadorEstados);
        ES3.Save<Dictionary<string, int>>(slot + "DiccionarioJugadorValores", _dicJugadorValores);
        ES3.Save<Vector3>(slot + "J_pos", ControladorJuego.jugadorAcceso.transform.position);

        /*

        ES3.Save<bool>(slot + "J_UI_Rojo", ControladorJuego.jugadorAcceso.rojo);
        ES3.Save<bool>(slot + "J_UI_Azul", ControladorJuego.jugadorAcceso.azul);
        ES3.Save<bool>(slot + "J_UI_Amarillo", ControladorJuego.jugadorAcceso.amarillo);

        ES3.Save<int>(slot + "J_nivel", ControladorJuego.sistemaExperiencia.nivelJugador);
        ES3.Save<int>(slot + "J_nivelBlanco", ControladorJuego.nivelBlanco);
        ES3.Save<int>(slot + "J_nivelRojo", ControladorJuego.nivelRojo);
        ES3.Save<int>(slot + "J_nivelAzul", ControladorJuego.nivelAzul);
        ES3.Save<int>(slot + "J_nivelAmarillo", ControladorJuego.nivelAmarillo);
        ES3.Save<int>(slot + "J_nivelMorado", ControladorJuego.nivelMorado);
        ES3.Save<int>(slot + "J_nivelVerde", ControladorJuego.nivelVerde);
        ES3.Save<int>(slot + "J_nivelNaranja", ControladorJuego.nivelNaranja);
        ES3.Save<int>(slot + "J_nivelNegro", ControladorJuego.nivelNegro);

        ES3.Save<bool>(slot + "J_MagiaRoja", ControladorJuego.magiaRoja);
        ES3.Save<bool>(slot + "J_MagiaAzul", ControladorJuego.magiaAzul);
        ES3.Save<bool>(slot + "J_MagiaAmarilla", ControladorJuego.magiaAmarilla);
        ES3.Save<bool>(slot + "J_MagiaMorado", ControladorJuego.magiaMorada);
        ES3.Save<bool>(slot + "J_MagiaNaranja", ControladorJuego.magiaNaranja);
        ES3.Save<bool>(slot + "J_MagiaVerde", ControladorJuego.magiaVerde);
        ES3.Save<bool>(slot + "J_MagiaNegro", ControladorJuego.magiaNegra);

        ES3.Save<bool>(slot + "PuedeDisparar", ControladorJuego.puedeDisparar);
        ES3.Save<bool>(slot + "MovimientoSecundario", ControladorJuego.movimientoEspecial);
        ES3.Save<bool>(slot + "PuedeCambiarColorOpuesto", ControladorJuego.puedeCambariColorOpuesto);


        ES3.Save<int>(slot + "J_experiencia", ControladorJuego.sistemaExperiencia.expJugador);
        ES3.Save<bool>(slot + "J_tutorial", ControladorJuego.tutorial);
        ES3.Save<int>(slot + "J_magiaColor", (int)ControladorJuego.jugadorAcceso.magiaColor);
        ES3.Save<Vector3>(slot + "J_pos", ControladorJuego.jugadorAcceso.transform.position);
        */
        Debug.Log("Se ha guardado correctamente la información del jugador");

    }

    public void CargarJugador()
    {
        Dictionary<string, bool> _dicJugadorEstados = ES3.Load<Dictionary<string, bool>>(slot + "DiccionarioJugadorEstados");
        Dictionary<string, int> _dicJugadorValores = ES3.Load<Dictionary<string, int>>(slot + "DiccionarioJugadorValores");
        ControladorJuego.sistemaExperiencia.ActualizarExperiencia(_dicJugadorValores[slot + "J_experiencia"]);
        ControladorJuego.adminJuego.SeteartTutorial(_dicJugadorEstados[slot + "J_tutorial"]);
        ControladorJuego.jugadorAcceso.SetearMagiasActivadas
        (_dicJugadorEstados[slot + "J_UI_Rojo"], _dicJugadorEstados[slot + "J_UI_Azul"], _dicJugadorEstados[slot + "J_UI_Amarillo"]);


        ControladorJuego.sistemaExperiencia.SetearNivelJugador(_dicJugadorValores[slot + "J_nivel"]);
        ControladorJuego.sistemaExperiencia.SetearNivelesMagia(
        _dicJugadorValores[slot + "J_nivelBlanco"],
        _dicJugadorValores[slot + "J_nivelRojo"],
        _dicJugadorValores[slot + "J_nivelAzul"],
        _dicJugadorValores[slot + "J_nivelAmarillo"],
        _dicJugadorValores[slot + "J_nivelMorado"],
        _dicJugadorValores[slot + "J_nivelNaranja"],
        _dicJugadorValores[slot + "J_nivelVerde"],
        _dicJugadorValores[slot + "J_nivelNegro"]);

        if (_dicJugadorEstados[slot + "J_MagiaRoja"]) ControladorJuego.sistemaMagiaColor.DesbloquearMagiaRoja();
        if (_dicJugadorEstados[slot + "J_MagiaAzul"]) ControladorJuego.sistemaMagiaColor.DesbloquearMagiaAzul();
        if (_dicJugadorEstados[slot + "J_MagiaAmarilla"]) ControladorJuego.sistemaMagiaColor.DesbloquearMagiaAmarilla();
        if (_dicJugadorEstados[slot + "J_MagiaMorado"]) ControladorJuego.sistemaMagiaColor.DesbloquearMagiaMorada();
        if (_dicJugadorEstados[slot + "J_MagiaNaranja"]) ControladorJuego.sistemaMagiaColor.DesbloquearMagiaNaranja();
        if (_dicJugadorEstados[slot + "J_MagiaVerde"]) ControladorJuego.sistemaMagiaColor.DesbloquearMagiaVerde();
        if (_dicJugadorEstados[slot + "J_MagiaNegro"]) ControladorJuego.sistemaMagiaColor.DesbloquearMagiaNegra();

        ControladorJuego.adminJuego.SetearDisparo(_dicJugadorEstados[slot + "PuedeDisparar"]);
        ControladorJuego.adminJuego.SetearMovimiemintoEspecial(_dicJugadorEstados[slot + "MovimientoSecundario"]);
        ControladorJuego.adminJuego.SetearCambioColorOpuesto(_dicJugadorEstados[slot + "PuedeCambiarColorOpuesto"]);

        ControladorJuego.jugadorAcceso.magiaColor = (MagiaColor)_dicJugadorValores[slot + "J_magiaColor"];
        ControladorJuego.jugadorAcceso.transform.position = ES3.Load<Vector3>(slot + "J_pos");

        /*
        ControladorJuego.sistemaExperiencia.ActualizarExperiencia(ES3.Load<int>(slot + "J_experiencia"));
        ControladorJuego.adminJuego.SeteartTutorial(ES3.Load<bool>(slot + "J_tutorial"));
        ControladorJuego.jugadorAcceso.SetearMagiasActivadas
        (ES3.Load<bool>(slot + "J_UI_Rojo"), ES3.Load<bool>(slot + "J_UI_Azul"), ES3.Load<bool>(slot + "J_UI_Amarillo"));

        ControladorJuego.sistemaExperiencia.SetearNivelJugador(ES3.Load<int>(slot + "J_nivel"));
        ControladorJuego.sistemaExperiencia.SetearNivelesMagia(
        ES3.Load<int>(slot + "J_nivelBlanco"),
        ES3.Load<int>(slot + "J_nivelRojo"),
        ES3.Load<int>(slot + "J_nivelAzul"),
        ES3.Load<int>(slot + "J_nivelAmarillo"),
        ES3.Load<int>(slot + "J_nivelMorado"),
        ES3.Load<int>(slot + "J_nivelNaranja"),
        ES3.Load<int>(slot + "J_nivelVerde"),
        ES3.Load<int>(slot + "J_nivelNegro"));

        if (ES3.Load<bool>(slot + "J_MagiaRoja")) ControladorJuego.sistemaMagiaColor.DesbloquearMagiaRoja();
        if (ES3.Load<bool>(slot + "J_MagiaAzul")) ControladorJuego.sistemaMagiaColor.DesbloquearMagiaAzul();
        if (ES3.Load<bool>(slot + "J_MagiaAmarilla")) ControladorJuego.sistemaMagiaColor.DesbloquearMagiaAmarilla();
        if (ES3.Load<bool>(slot + "J_MagiaMorado")) ControladorJuego.sistemaMagiaColor.DesbloquearMagiaMorada();
        if (ES3.Load<bool>(slot + "J_MagiaNaranja")) ControladorJuego.sistemaMagiaColor.DesbloquearMagiaNaranja();
        if (ES3.Load<bool>(slot + "J_MagiaVerde")) ControladorJuego.sistemaMagiaColor.DesbloquearMagiaVerde();
        if (ES3.Load<bool>(slot + "J_MagiaNegro")) ControladorJuego.sistemaMagiaColor.DesbloquearMagiaNegra();

        ControladorJuego.adminJuego.SetearDisparo(ES3.Load<bool>(slot + "PuedeDisparar"));
        ControladorJuego.adminJuego.SetearMovimiemintoEspecial(ES3.Load<bool>(slot + "MovimientoSecundario"));
        ControladorJuego.adminJuego.SetearCambioColorOpuesto(ES3.Load<bool>(slot + "PuedeCambiarColorOpuesto"));

        ControladorJuego.jugadorAcceso.magiaColor = (MagiaColor)ES3.Load<int>(slot + "J_magiaColor");
        ControladorJuego.jugadorAcceso.transform.position = ES3.Load<Vector3>(slot + "J_pos");
        */

        Debug.Log("Se ha cargado correctamente la información del jugador");
    }
    #endregion

    #region Misiones
    private void GuardarMisiones()
    {
        Dictionary<string, bool> _dicMisiones = new Dictionary<string, bool>();


        foreach (Mision _mision in ControladorJuego.sistemaMisiones.misionesTutorial)
        {
            _dicMisiones.Add(slot + "MisionTutorial" + _mision.nombre + "Completada", _mision.completado);
            _dicMisiones.Add(slot + "MisionTutorial" + _mision.nombre + "Inicializada", _mision.inicializada);
        }
        foreach (Mision _mision in ControladorJuego.sistemaMisiones.misiones)
        {
            _dicMisiones.Add(slot + "Mision" + _mision.nombre + "Completada", _mision.completado);
            _dicMisiones.Add(slot + "Mision" + _mision.nombre + "Inicializada", _mision.inicializada);
        }
        ES3.Save<Dictionary<string, bool>>(slot + "DiccionarioMisiones", _dicMisiones);

        /*
       foreach (Mision _mision in ControladorJuego.sistemaMisiones.misionesTutorial)
       {

           ES3.Save<bool>(slot + "MisionTutorial" + _mision.nombre + "Completada", _mision.completado);
           ES3.Save<bool>(slot + "MisionTutorial" + _mision.nombre + "Inicializada", _mision.inicializada);
       }
       foreach (Mision _mision in ControladorJuego.sistemaMisiones.misiones)
       {
           ES3.Save<bool>(slot + "Mision" + _mision.nombre + "Completada", _mision.completado);
           ES3.Save<bool>(slot + "Mision" + _mision.nombre + "Inicializada", _mision.inicializada);
       }
       */
        Debug.Log("Se ha guardado correctamente la información de las misiones");

    }

    private void CargarMisiones()
    {
        Dictionary<string, bool> _dicMisiones = ES3.Load<Dictionary<string, bool>>(slot + "DiccionarioMisiones");


        for (int i = 0; i < ControladorJuego.sistemaMisiones.misionesTutorial.Length; i++)
        {
            ControladorJuego.sistemaMisiones.misionesTutorial[i].completado = _dicMisiones
            [slot + "MisionTutorial" + ControladorJuego.sistemaMisiones.misionesTutorial[i].nombre + "Completada"];
            ControladorJuego.sistemaMisiones.misionesTutorial[i].inicializada = _dicMisiones
            [slot + "MisionTutorial" + ControladorJuego.sistemaMisiones.misionesTutorial[i].nombre + "Inicializada"];
        }
        for (int i = 0; i < ControladorJuego.sistemaMisiones.misiones.Length; i++)
        {
            ControladorJuego.sistemaMisiones.misiones[i].completado = _dicMisiones
            [slot + "Mision" + ControladorJuego.sistemaMisiones.misiones[i].nombre + "Completada"];
            ControladorJuego.sistemaMisiones.misiones[i].inicializada = _dicMisiones
            [slot + "Mision" + ControladorJuego.sistemaMisiones.misiones[i].nombre + "Inicializada"];
        }

        /*
        for (int i = 0; i < ControladorJuego.sistemaMisiones.misionesTutorial.Length; i++)
        {
            ControladorJuego.sistemaMisiones.misionesTutorial[i].completado =
             ES3.Load<bool>(slot + "MisionTutorial" + ControladorJuego.sistemaMisiones.misionesTutorial[i].nombre + "Completada");
            ControladorJuego.sistemaMisiones.misionesTutorial[i].inicializada =
             ES3.Load<bool>(slot + "MisionTutorial" + ControladorJuego.sistemaMisiones.misionesTutorial[i].nombre + "Inicializada");
        }
        for (int i = 0; i < ControladorJuego.sistemaMisiones.misiones.Length; i++)
        {
            ControladorJuego.sistemaMisiones.misiones[i].completado =
             ES3.Load<bool>(slot + "Mision" + ControladorJuego.sistemaMisiones.misiones[i].nombre + "Completada");
            ControladorJuego.sistemaMisiones.misiones[i].inicializada =
             ES3.Load<bool>(slot + "Mision" + ControladorJuego.sistemaMisiones.misiones[i].nombre + "Inicializada");
        }
        */
        Debug.Log("Se ha cargado correctamente la información de las misiones");
    }
    #endregion

    #region Mensajes
    private void GuardarMensajesLeidos()
    {
        Dictionary<string, bool> _dicMensajesLeidos = new Dictionary<string, bool>();


        foreach (LibreriaFrase _libreriaMensajes in ControladorJuego.sistemaMensajes.libreria)
        {
            foreach (LibreriaSubFase _frase in _libreriaMensajes.frases)
            {
                _dicMensajesLeidos.Add(slot + "Frase Hablada:" + _libreriaMensajes.codigo + _frase.subCodigo, _frase.hablado);

                _dicMensajesLeidos.Add(slot + "Frase Unica Vez:" + _libreriaMensajes.codigo + _frase.subCodigo, _frase.unicaVez);

                /*
                ES3.Save<bool>(slot + "Frase Hablada:" + _libreriaMensajes.codigo + _frase.subCodigo, _frase.hablado);
                ES3.Save<bool>(slot + "Frase Unica Vez:" + _libreriaMensajes.codigo + _frase.subCodigo, _frase.unicaVez);
                */
            }
        }
        ES3.Save<Dictionary<string, bool>>(slot + "DiccionarioMensajesLeidos", _dicMensajesLeidos);
        //bool _fake = _cahuana[slot + "Frase Hablada:"];
        //_cahuana.ContainsKey()
        Debug.Log("Se ha guardado correctamente la información de mensajes");
    }

    private void CargarMensajesLeidos()
    {
        Dictionary<string, bool> _dicMensajesLeidos = ES3.Load<Dictionary<string, bool>>(slot + "DiccionarioMensajesLeidos");



        for (int i = 0; i < ControladorJuego.sistemaMensajes.libreria.Length; i++)
        {
            for (int j = 0; j < ControladorJuego.sistemaMensajes.libreria[i].frases.Length; j++)
            {
                ControladorJuego.sistemaMensajes.libreria[i].frases[j].hablado = _dicMensajesLeidos[slot + "Frase Hablada:" + ControladorJuego.sistemaMensajes.libreria[i].codigo +
                 ControladorJuego.sistemaMensajes.libreria[i].frases[j].subCodigo];

                ControladorJuego.sistemaMensajes.libreria[i].frases[j].unicaVez = _dicMensajesLeidos[slot + "Frase Unica Vez:" + ControladorJuego.sistemaMensajes.libreria[i].codigo +
                 ControladorJuego.sistemaMensajes.libreria[i].frases[j].subCodigo];
            }
        }
        /*
        for (int i = 0; i < ControladorJuego.sistemaMensajes.libreria.Length; i++)
        {
            for (int j = 0; j < ControladorJuego.sistemaMensajes.libreria[i].frases.Length; j++)
            {
                ControladorJuego.sistemaMensajes.libreria[i].frases[j].hablado =
                 ES3.Load<bool>(slot + "Frase Hablada:" + ControladorJuego.sistemaMensajes.libreria[i].codigo +
                  ControladorJuego.sistemaMensajes.libreria[i].frases[j].subCodigo);

                ControladorJuego.sistemaMensajes.libreria[i].frases[j].unicaVez =
                 ES3.Load<bool>(slot + "Frase Unica Vez:" + ControladorJuego.sistemaMensajes.libreria[i].codigo +
                  ControladorJuego.sistemaMensajes.libreria[i].frases[j].subCodigo);
            }
        }
        */
        Debug.Log("Se ha cargado correctamente la información de mensajes");
    }
    #endregion

    #region Mecanismos Ocultos
    private void GuardarMecanismos()
    {
        Dictionary<string, float> _dicMecanismo = new Dictionary<string, float>();

        foreach (GameObject _mecanismo in ControladorJuego.sistemaMecanismosOculto.mecanismosGenerados)
        {
            _dicMecanismo.Add(slot + "Oculto" + _mecanismo.name,
             (ControladorJuego.sistemaMecanismosOculto.ObtenerMecanismo(_mecanismo.name).oculto) ? 1f : 0f);
            _dicMecanismo.Add(slot + "ForzarAparicion" + _mecanismo.name, (_mecanismo.activeSelf) ? 1f : 0f);
            _dicMecanismo.Add(slot + "Oculto_ID.x" + _mecanismo.name, _mecanismo.transform.parent.transform.position.x);
            _dicMecanismo.Add(slot + "Oculto_ID.y" + _mecanismo.name, _mecanismo.transform.parent.transform.position.y);
            _dicMecanismo.Add(slot + "Oculto_Pos.x" + _mecanismo.name, _mecanismo.transform.localPosition.x);
            _dicMecanismo.Add(slot + "Oculto_Pos.y" + _mecanismo.name, _mecanismo.transform.localPosition.y);
            _dicMecanismo.Add(slot + "Oculto_Pos.z" + _mecanismo.name, _mecanismo.transform.localPosition.z);
        }
        foreach (GameObject _altares in ControladorJuego.sistemaMecanismosOculto.altaresgenerados)
        {
            _dicMecanismo.Add(slot + "Oculto" + _altares.name, (_altares.activeSelf) ? 0 : 1f);
            _dicMecanismo.Add(slot + "ForzarAparicion" + _altares.name, (_altares.activeSelf) ? 1f : 0);
            _dicMecanismo.Add(slot + "Oculto_ID.x" + _altares.name, _altares.transform.parent.transform.position.x);
            _dicMecanismo.Add(slot + "Oculto_ID.y" + _altares.name, _altares.transform.parent.transform.position.y);
            _dicMecanismo.Add(slot + "Oculto_Pos.x" + _altares.name, _altares.transform.localPosition.x);
            _dicMecanismo.Add(slot + "Oculto_Pos.y" + _altares.name, _altares.transform.localPosition.y);
            _dicMecanismo.Add(slot + "Oculto_Pos.z" + _altares.name, _altares.transform.localPosition.z);
        }
        _dicMecanismo.Add(slot + "CantMecanismosTutorial", ControladorJuego.sistemaMecanismosOculto.mecanismosTutorial.Length);
        _dicMecanismo.Add(slot + "CantMecanismos", ControladorJuego.sistemaMecanismosOculto.mecanismos.Length);
        _dicMecanismo.Add(slot + "CantAltares", ControladorJuego.sistemaMecanismosOculto.altares.Length);

        ES3.Save<Dictionary<string, float>>(slot + "DiccionarioMecanismos", _dicMecanismo);
        /*
        foreach (GameObject _mecanismo in ControladorJuego.sistemaMecanismosOculto.mecanismosGenerados)
        {
            ES3.Save<bool>
            (slot + "Oculto" + _mecanismo.name, ControladorJuego.sistemaMecanismosOculto.ObtenerMecanismo(_mecanismo.name).oculto);
            ES3.Save<bool>(slot + "ForzarAparicion" + _mecanismo.name, _mecanismo.activeSelf);
            ES3.Save<Vector2>(slot + "Oculto_ID" + _mecanismo.name, _mecanismo.transform.parent.transform.position);
            ES3.Save<Vector3>(slot + "Oculto_Pos" + _mecanismo.name, _mecanismo.transform.localPosition);
        }
        foreach (GameObject _mecanismo in ControladorJuego.sistemaMecanismosOculto.altaresgenerados)
        {
            ES3.Save<bool>
            (slot + "Oculto" + _mecanismo.name, !_mecanismo.activeSelf);
            ES3.Save<bool>(slot + "ForzarAparicion" + _mecanismo.name, _mecanismo.activeSelf);
            ES3.Save<Vector2>(slot + "Oculto_ID" + _mecanismo.name, _mecanismo.transform.parent.transform.position);
            ES3.Save<Vector3>(slot + "Oculto_Pos" + _mecanismo.name, _mecanismo.transform.localPosition);
        }
        ES3.Save<int>(slot + "CantMecanismosTutorial", ControladorJuego.sistemaMecanismosOculto.mecanismosTutorial.Length);
        ES3.Save<int>(slot + "CantMecanismos", ControladorJuego.sistemaMecanismosOculto.mecanismos.Length);

        ES3.Save<int>(slot + "CantAltares", ControladorJuego.sistemaMecanismosOculto.altares.Length);
        */
        Debug.Log("Se ha guardado correctamente la información de las misiones");
    }

    private void CargarMecanismos()
    {
        Dictionary<string, float> _dicMensajesLeidos = ES3.Load<Dictionary<string, float>>(slot + "DiccionarioMecanismos");

        int cantMecanismosTutorial = (int)_dicMensajesLeidos[slot + "CantMecanismosTutorial"];
        int cantMecanismos = (int)_dicMensajesLeidos[slot + "CantMecanismos"];
        int cantAltares = (int)_dicMensajesLeidos[slot + "CantAltares"];

        for (int i = 0; i < cantMecanismosTutorial; i++)
        {
            ControladorJuego.sistemaMecanismosOculto.mecanismosTutorial[i].oculto = (_dicMensajesLeidos
            [slot + "Oculto" + ControladorJuego.sistemaMecanismosOculto.mecanismosTutorial[i].nombre] == 1f) ? true : false;
            ControladorJuego.sistemaMecanismosOculto.mecanismosTutorial[i].forzarAparicion = (_dicMensajesLeidos
            [slot + "ForzarAparicion" + ControladorJuego.sistemaMecanismosOculto.mecanismosTutorial[i].nombre] == 1f) ? true : false;
            ControladorJuego.sistemaMecanismosOculto.mecanismosTutorial[i].area_ID = new Vector2
            (_dicMensajesLeidos[slot + "Oculto_ID.x" + ControladorJuego.sistemaMecanismosOculto.mecanismosTutorial[i].nombre],
            _dicMensajesLeidos[slot + "Oculto_ID.y" + ControladorJuego.sistemaMecanismosOculto.mecanismosTutorial[i].nombre]);
            ControladorJuego.sistemaMecanismosOculto.mecanismosTutorial[i].posicion = new Vector3
            (_dicMensajesLeidos[slot + "Oculto_Pos.x" + ControladorJuego.sistemaMecanismosOculto.mecanismosTutorial[i].nombre],
            _dicMensajesLeidos[slot + "Oculto_Pos.y" + ControladorJuego.sistemaMecanismosOculto.mecanismosTutorial[i].nombre],
            _dicMensajesLeidos[slot + "Oculto_Pos.z" + ControladorJuego.sistemaMecanismosOculto.mecanismosTutorial[i].nombre]);
        }

        for (int i = 0; i < cantMecanismos; i++)
        {
            ControladorJuego.sistemaMecanismosOculto.mecanismos[i].oculto = (_dicMensajesLeidos
            [slot + "Oculto" + ControladorJuego.sistemaMecanismosOculto.mecanismos[i].nombre + " " + i] == 1f) ? true : false;
            ControladorJuego.sistemaMecanismosOculto.mecanismos[i].forzarAparicion = (_dicMensajesLeidos
            [slot + "ForzarAparicion" + ControladorJuego.sistemaMecanismosOculto.mecanismos[i].nombre + " " + i] == 1f) ? true : false;
            ControladorJuego.sistemaMecanismosOculto.mecanismos[i].area_ID = new Vector2
            (_dicMensajesLeidos[slot + "Oculto_ID.x" + ControladorJuego.sistemaMecanismosOculto.mecanismos[i].nombre + " " + i],
            _dicMensajesLeidos[slot + "Oculto_ID.y" + ControladorJuego.sistemaMecanismosOculto.mecanismos[i].nombre + " " + i]);
            ControladorJuego.sistemaMecanismosOculto.mecanismos[i].posicion = new Vector3
            (_dicMensajesLeidos[slot + "Oculto_Pos.x" + ControladorJuego.sistemaMecanismosOculto.mecanismos[i].nombre + " " + i],
            _dicMensajesLeidos[slot + "Oculto_Pos.y" + ControladorJuego.sistemaMecanismosOculto.mecanismos[i].nombre + " " + i],
            _dicMensajesLeidos[slot + "Oculto_Pos.z" + ControladorJuego.sistemaMecanismosOculto.mecanismos[i].nombre + " " + i]);
        }

        for (int i = 0; i < cantAltares; i++)
        {
            ControladorJuego.sistemaMecanismosOculto.altares[i].oculto = (_dicMensajesLeidos
            [slot + "Oculto" + ControladorJuego.sistemaMecanismosOculto.altares[i].nombre + " " + i] == 1f) ? true : false;
            ControladorJuego.sistemaMecanismosOculto.altares[i].forzarAparicion = (_dicMensajesLeidos
            [slot + "ForzarAparicion" + ControladorJuego.sistemaMecanismosOculto.altares[i].nombre + " " + i] == 1f) ? true : false;
            ControladorJuego.sistemaMecanismosOculto.altares[i].area_ID = new Vector2
            (_dicMensajesLeidos[slot + "Oculto_ID.x" + ControladorJuego.sistemaMecanismosOculto.altares[i].nombre + " " + i],
            _dicMensajesLeidos[slot + "Oculto_ID.y" + ControladorJuego.sistemaMecanismosOculto.altares[i].nombre + " " + i]);
            ControladorJuego.sistemaMecanismosOculto.altares[i].posicion = new Vector3
            (_dicMensajesLeidos[slot + "Oculto_Pos.x" + ControladorJuego.sistemaMecanismosOculto.altares[i].nombre + " " + i],
            _dicMensajesLeidos[slot + "Oculto_Pos.y" + ControladorJuego.sistemaMecanismosOculto.altares[i].nombre + " " + i],
            _dicMensajesLeidos[slot + "Oculto_Pos.z" + ControladorJuego.sistemaMecanismosOculto.altares[i].nombre + " " + i]);
        }

        /*

        int cantMecanismosTutorial = ES3.Load<int>(slot + "CantMecanismosTutorial");
        int cantMecanismos = ES3.Load<int>(slot + "CantMecanismos");
        int cantAltares = ES3.Load<int>(slot + "CantAltares");

        for (int i = 0; i < cantMecanismosTutorial; i++)
        {
            ControladorJuego.sistemaMecanismosOculto.mecanismosTutorial[i].oculto =
             ES3.Load<bool>(slot + "Oculto" + ControladorJuego.sistemaMecanismosOculto.mecanismosTutorial[i].nombre);

            ControladorJuego.sistemaMecanismosOculto.mecanismosTutorial[i].forzarAparicion =
            ES3.Load<bool>(slot + "ForzarAparicion" + ControladorJuego.sistemaMecanismosOculto.mecanismosTutorial[i].nombre);

            ControladorJuego.sistemaMecanismosOculto.mecanismosTutorial[i].area_ID =
             ES3.Load<Vector2>(slot + "Oculto_ID" + ControladorJuego.sistemaMecanismosOculto.mecanismosTutorial[i].nombre);

            ControladorJuego.sistemaMecanismosOculto.mecanismosTutorial[i].posicion =
            ES3.Load<Vector3>(slot + "Oculto_Pos" + ControladorJuego.sistemaMecanismosOculto.mecanismosTutorial[i].nombre);
        }

        for (int i = 0; i < cantMecanismos; i++)
        {
            ControladorJuego.sistemaMecanismosOculto.mecanismos[i].oculto =
             ES3.Load<bool>(slot + "Oculto" + ControladorJuego.sistemaMecanismosOculto.mecanismos[i].nombre + " " + i);

            ControladorJuego.sistemaMecanismosOculto.mecanismos[i].forzarAparicion =
            ES3.Load<bool>(slot + "ForzarAparicion" + ControladorJuego.sistemaMecanismosOculto.mecanismos[i].nombre + " " + i);

            ControladorJuego.sistemaMecanismosOculto.mecanismos[i].area_ID =
             ES3.Load<Vector2>(slot + "Oculto_ID" + ControladorJuego.sistemaMecanismosOculto.mecanismos[i].nombre + " " + i);

            ControladorJuego.sistemaMecanismosOculto.mecanismos[i].posicion =
             ES3.Load<Vector3>(slot + "Oculto_Pos" + ControladorJuego.sistemaMecanismosOculto.mecanismos[i].nombre + " " + i);
        }

        for (int i = 0; i < cantAltares; i++)
        {
            ControladorJuego.sistemaMecanismosOculto.altares[i].oculto =
             ES3.Load<bool>(slot + "Oculto" + ControladorJuego.sistemaMecanismosOculto.altares[i].nombre + " " + i);

            ControladorJuego.sistemaMecanismosOculto.altares[i].forzarAparicion =
            ES3.Load<bool>(slot + "ForzarAparicion" + ControladorJuego.sistemaMecanismosOculto.altares[i].nombre + " " + i);

            ControladorJuego.sistemaMecanismosOculto.altares[i].area_ID =
             ES3.Load<Vector2>(slot + "Oculto_ID" + ControladorJuego.sistemaMecanismosOculto.altares[i].nombre + " " + i);

            ControladorJuego.sistemaMecanismosOculto.altares[i].posicion =
             ES3.Load<Vector3>(slot + "Oculto_Pos" + ControladorJuego.sistemaMecanismosOculto.altares[i].nombre + " " + i);
        }
        */


        ControladorJuego.sistemaMecanismosOculto.InicializarMecanismos();
        Debug.Log("Se ha cargado correctamente la información de las misiones");
    }
    #endregion


    #region Slots
    public void GuardarSot(int _value)
    {
        ES3.Save<int>("Slot" + _value, _value);
        Debug.Log("Se ha guardado correctamente le slot" + _value);
    }

    public bool VerificarSlot(int _valor)
    {
        return ES3.KeyExists("Slot" + _valor);
    }

    public void BorrarSlot(int _value)
    {
        /*
        #region Borrado de MAPA
        int _areasGeneradas = ES3.Load<int>(_value + "AreasGeneradas");
        ES3.DeleteKey(_value + "AreasGeneradas");
        for (int a = 0; a < _areasGeneradas; a++)
        {
            int _cantHuecos = ES3.Load<int>(_value + "Area" + a + "CantHuecos");
            ES3.DeleteKey(_value + "Area" + a + "CantHuecos");
            if (_cantHuecos >= 1)
            {
                for (int h = 0; h < _cantHuecos; h++)
                {
                    ES3.DeleteKey(_value + "Area" + a + "HuecoPos" + h);
                    ES3.DeleteKey(_value + "Area" + a + "HuecoEscala" + h);
                    ES3.DeleteKey(_value + "Area" + a + "HuecoRot" + h);
                }
            }

            ES3.DeleteKey(_value + "Area" + a);
            ES3.DeleteKey(_value + "Area" + a + "activo");
            ES3.DeleteKey(_value + "Area" + a + "GenerarHueco");
            ES3.DeleteKey(_value + "Area" + a + "ForzarInicializador");
            ES3.DeleteKey(_value + "Area" + a + "BloquearArea");
            ES3.DeleteKey(_value + "Area" + a + "UnicaVez");

            for (int p = 0; p < 4; p++) //4 por que siempre hay 4 portales
            {
                ES3.DeleteKey(_value + "Area" + a + "PortalBloqueado" + p);
                ES3.DeleteKey(_value + "Area" + a + "PortalColor" + p);
                ES3.DeleteKey(_value + "Area" + a + "PortalConeccion" + p);
            }
        }

        ES3.DeleteKey(slot + "AreaGuardada");
        ES3.DeleteKey(slot + "Mapa Interactuable");

        #endregion
        #region Borrado de Jugador
        ES3.DeleteKey(_value + "J_nivel");

        ES3.DeleteKey(_value + "J_UI_Rojo");
        ES3.DeleteKey(_value + "J_UI_Azul");
        ES3.DeleteKey(_value + "J_UI_Amarillo");

        ES3.DeleteKey(_value + "J_nivelBlanco");
        ES3.DeleteKey(_value + "J_nivelRojo");
        ES3.DeleteKey(_value + "J_nivelAzul");
        ES3.DeleteKey(_value + "J_nivelAmarillo");
        ES3.DeleteKey(_value + "J_nivelMorado");
        ES3.DeleteKey(_value + "J_nivelVerde");
        ES3.DeleteKey(_value + "J_nivelNaranja");
        ES3.DeleteKey(_value + "J_nivelNegro");

        ES3.DeleteKey(_value + "J_MagiaRoja");
        ES3.DeleteKey(_value + "J_MagiaAzul");
        ES3.DeleteKey(_value + "J_MagiaAmarilla");
        ES3.DeleteKey(_value + "J_MagiaMorado");
        ES3.DeleteKey(_value + "J_MagiaVerde");
        ES3.DeleteKey(_value + "J_MagiaNaranja");
        ES3.DeleteKey(_value + "J_MagiaNegro");

        ES3.DeleteKey(_value + "PuedeDisparar");
        ES3.DeleteKey(_value + "MovimientoSecundario");
        ES3.DeleteKey(_value + "PuedeCambiarColorOpuesto");

        ES3.DeleteKey(_value + "J_experiencia");
        ES3.DeleteKey(_value + "J_tutorial");
        ES3.DeleteKey(_value + "J_magiaColor");
        ES3.DeleteKey(_value + "J_pos");

        #endregion

        #region Borrado de Misiones
        foreach (Mision _mision in ControladorJuego.sistemaMisiones.misionesTutorial)
        {
            ES3.DeleteKey(_value + "MisionTutorial" + _mision.nombre + "Completada");
            ES3.DeleteKey(_value + "MisionTutorial" + _mision.nombre + "Inicializada");
        }
        foreach (Mision _mision in ControladorJuego.sistemaMisiones.misiones)
        {
            ES3.DeleteKey(_value + "MisionTutorial" + _mision.nombre + "Completada");
            ES3.DeleteKey(_value + "MisionTutorial" + _mision.nombre + "Inicializada");
        }
        #endregion

        #region Borrado de Mensajes
        foreach (LibreriaFrase _libreriaMensajes in ControladorJuego.sistemaMensajes.libreria)
        {
            foreach (LibreriaSubFase _frase in _libreriaMensajes.frases)
            {
                ES3.DeleteKey(_value + "Frase Hablada:" + _libreriaMensajes.codigo + _frase.subCodigo);
                ES3.DeleteKey(_value + "Frase Unica Vez:" + _libreriaMensajes.codigo + _frase.subCodigo);
            }
        }
        #endregion

        #region Borrado de Mecanismos
        foreach (GameObject _mecanismo in ControladorJuego.sistemaMecanismosOculto.mecanismosGenerados)
        {
            ES3.DeleteKey(_value + "Oculto" + _mecanismo.name);
            ES3.DeleteKey(_value + "ForzarAparicion" + _mecanismo.name);
            ES3.DeleteKey(_value + "Oculto_ID" + _mecanismo.name);
            ES3.DeleteKey(_value + "Oculto_Pos" + _mecanismo.name);
        }

        foreach (GameObject _altares in ControladorJuego.sistemaMecanismosOculto.altaresgenerados)
        {
            ES3.DeleteKey(_value + "Oculto" + _altares.name);
            ES3.DeleteKey(_value + "ForzarAparicion" + _altares.name);
            ES3.DeleteKey(_value + "Oculto_ID" + _altares.name);
            ES3.DeleteKey(_value + "Oculto_Pos" + _altares.name);
        }

        ES3.DeleteKey(_value + "CantMecanismosTutorial");
        ES3.DeleteKey(_value + "CantMecanismos");
        ES3.DeleteKey(_value + "CantAltares");
        #endregion
        */

        ES3.DeleteKey(_value + "J_pos");
        ES3.DeleteKey(_value + "DiccionarioMecanismos");
        ES3.DeleteKey(_value + "DiccionarioMensajesLeidos");
        ES3.DeleteKey(_value + "DiccionarioMisiones");
        ES3.DeleteKey(_value + "DiccionarioJugadorEstados");
        ES3.DeleteKey(_value + "DiccionarioJugadorValores");
        ES3.DeleteKey(_value + "DiccionarioAreasGeneradasEstados");
        ES3.DeleteKey(_value + "DiccionarioAreasGeneradasValores");

        #region Borrado del Slot
        ES3.DeleteKey("Slot" + _value);
        #endregion

        Debug.Log("Se ha eliminado correctamente le slot");

    }
    #endregion

    public void BorrarTodosLosDatos()
    {
        ES3.DeleteFile();
        Debug.Log("Se ha borrado correctamente toda la data del juego");
    }

}
