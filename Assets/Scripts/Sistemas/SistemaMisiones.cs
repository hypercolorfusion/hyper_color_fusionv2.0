using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SistemaMisiones : MonoBehaviour
{
    // Start is called before the first frame update

    public bool tutorial;
    public Mision[] misionesTutorial;
    public Mision[] misiones;


    void Awake()
    {
        if (ControladorJuego.adminJuego.ObtenerSistemaMisiones() == null)
        {
            ControladorJuego.adminJuego.SetearSistemaMisiones(this);
            DontDestroyOnLoad(transform.parent);
        }
        else Destroy(gameObject);
    }

    public void ComenzarJuego() { tutorial = true; }
    public void TerminarTutorial() { tutorial = false; }

    #region Tutorial
    public void CompletarMisionTutorial(string _nombre)
    {
        if (_nombre == "DASH")
        {
            ControladorJuego.sistemaOleadas.ActivarOleada(MagiaColor.azul);
        }
        CompletarMisionTutorial(BuscarMisionTutorial(_nombre));
    }
    private void CompletarMisionTutorial(int _indice)
    {
        misionesTutorial[_indice].completado = true;
        ControladorJuego.sistemaExperiencia.GanarExperiencia(misionesTutorial[_indice].recompensa);
        ControladorJuego.jugadorAcceso.Curar(ControladorJuego.resistenciaBase);
        CancelarMisionTutorial(_indice); //Solamente quita el estado de inicializado a falso
        ControladorJuego.sistemaMensajes.MostarTexto("MISIONES", misionesTutorial[_indice].nombre, "Ayuda ", true);


    }

    public void IniciarMisionTutorial(string _nombre)
    {
        if (_nombre == "CAMBIO_COLOR_ROJO" || _nombre == "CAMBIO_COLOR_AMARILLO")
        {
            ControladorJuego.adminUi.MostrarColorPlayer();
        }
        else if (_nombre == "MAPA")
        {
            Debug.Log("Completaste la mision Mapa");
            ControladorJuego.sistemaMagiaColor.DesbloquearMagiaAmarilla();
            ControladorJuego.sistemaExperiencia.SubirNivelAmarillo();
            ControladorJuego.sistemaMapa.ActualizarMapaInteractuable(true);
            IniciarMisionTutorial("CAMBIO_COLOR_AMARILLO");
        }
        else if (_nombre == "DASH")
        {
            ControladorJuego.adminJuego.SetearMovimiemintoEspecial(true);
            ControladorJuego.sistemaMensajes.MostarTexto("TUTORIAL", "DASH", "AYUDA", true);
        }
        else if (_nombre == "TECLA_Q")
        {
            ControladorJuego.adminJuego.SetearCambioColorOpuesto(true);
            ControladorJuego.sistemaMensajes.MostarTexto("TUTORIAL", "TECLA_Q", "AYUDA", true);

        }

        if (VerificarMisionCompletadaTutorial(_nombre)) Debug.Log("Has intendado iniciar una mision ya finalizada");
        else IniciarMisionTutorial(BuscarMisionTutorial(_nombre));
    }
    private void IniciarMisionTutorial(int _indice) { misionesTutorial[_indice].inicializada = true; }

    public void CancelarMisionTutorial(string _nombre) { CancelarMisionTutorial(BuscarMisionTutorial(_nombre)); }
    public void CancelarMisionTutorial(int _indice) { misionesTutorial[_indice].inicializada = false; }


    public bool VerificarMisionInicializadaTutorial(string _nombre)
    {
        if (VerificarMisionCompletadaTutorial(_nombre)) return false;
        return VerificarMisionInicializadaTutorial(BuscarMisionTutorial(_nombre));
    }
    private bool VerificarMisionInicializadaTutorial(int _indice) { return misionesTutorial[_indice].inicializada; }

    public bool VerificarMisionCompletadaTutorial(string _nombre) { return VerificarMisionCompletadaTutorial(BuscarMisionTutorial(_nombre)); }
    private bool VerificarMisionCompletadaTutorial(int _indice) { return misionesTutorial[_indice].completado; }

    private int BuscarMisionTutorial(string _nombre)
    {
        for (int i = 0; i < misionesTutorial.Length; i++)
        {
            if (misionesTutorial[i].nombre == _nombre)
                return i;
        }
        Debug.LogError("La mision no se ha encontrado");
        return -1;//
    }
    #endregion

    #region Gameplay
    public void CompletarMision(string _nombre) { CompletarMision(BuscarMision(_nombre)); }
    private void CompletarMision(int _indice)
    {
        misiones[_indice].completado = true;
        ControladorJuego.sistemaExperiencia.GanarExperiencia(misiones[_indice].recompensa);
        ControladorJuego.jugadorAcceso.Curar(ControladorJuego.resistenciaBase);
        CancelarMision(_indice); //Solamente quita el estado de inicializado a falso
        ControladorJuego.sistemaMensajes.MostarTexto("MISIONES", misiones[_indice].nombre, "Ayuda ", true);
    }

    public void IniciarMision(string _nombre)
    {
        if (VerificarMisionCompletada(_nombre)) Debug.Log("Has intendado iniciar una mision ya finalizada");
        else IniciarMision(BuscarMision(_nombre));
    }
    private void IniciarMision(int _indice) { misiones[_indice].inicializada = true; }

    public void CancelarMision(string _nombre) { CancelarMision(BuscarMision(_nombre)); }
    public void CancelarMision(int _indice) { misiones[_indice].inicializada = false; }

    public bool VerificarMisionInicializada(string _nombre)
    {
        if (VerificarMisionCompletada(_nombre))
        {
            Debug.LogWarning("Has intendado verificar una mision que ya ha finalizado anteriormente");
            return false;
        }
        return VerificarMisionInicializada(BuscarMision(_nombre));
    }
    private bool VerificarMisionInicializada(int _indice) { return misiones[_indice].inicializada; }

    public bool VerificarMisionCompletada(string _nombre) { return VerificarMisionCompletada(BuscarMision(_nombre)); }
    private bool VerificarMisionCompletada(int _indice) { return misiones[_indice].completado; }

    private int BuscarMision(string _nombre)
    {
        for (int i = 0; i < misiones.Length; i++)
        {
            if (misiones[i].nombre == _nombre)
                return i;
        }
        Debug.LogError("La mision no se ha encontrado");
        return -1;//
    }
    #endregion


    public void MostrarInfoMision(int _indice)
    {
        //Mostrar misiones[_indice].info en SistemaMensajes
    }
}

[System.Serializable]
public struct Mision
{
    public string nombre; //Nombre de la mision
    [TextArea(3, 10)]
    public string info; //Pistas o información sobre la misión. Algo para que se pueda ver luego o saber que se hace o cual es el objetivo
    public bool inicializada;
    public bool completado;
    public int recompensa;
    public string recompensaInfo;

}
