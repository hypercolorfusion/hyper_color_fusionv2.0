using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SistemaExperiencia : MonoBehaviour
{

    public int nivelJugador = 1;
    public int expJugador = 0;


    // Start is called before the first frame update

    void Awake()
    {
        if (ControladorJuego.adminJuego.ObtenerSistemaExperiencia() == null)
        {
            ControladorJuego.adminJuego.SetearSistemaExperiencia(this);
            DontDestroyOnLoad(transform.parent);
        }
        else
            Destroy(gameObject);
    }

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    #region Uso de Niveles
    public void ResetearNivelesMagia()
    {
        ControladorJuego.nivelBlanco = ControladorJuego.nivelAmarillo = ControladorJuego.nivelAzul = ControladorJuego.nivelMorado =
        ControladorJuego.nivelNaranja = ControladorJuego.nivelNegro = ControladorJuego.nivelRojo = ControladorJuego.nivelVerde = 1;
        ControladorJuego.jugadorAcceso.VerificarEstadosPorColor();
        ControladorJuego.sistemaExperiencia.expJugador = 0;
    }

    public void SetearNivelesMagia(int _blanco, int _rojo, int _azul, int _amarillo, int _morado, int _naranja, int _verde, int _negro)
    {
        ControladorJuego.nivelBlanco = _blanco;

        ControladorJuego.nivelRojo = _rojo;
        ControladorJuego.nivelAzul = _azul;
        ControladorJuego.nivelAmarillo = _amarillo;

        ControladorJuego.nivelMorado = _morado;
        ControladorJuego.nivelNaranja = _naranja;
        ControladorJuego.nivelVerde = _verde;

        ControladorJuego.nivelNegro = _negro;
        VerificarAtributosJugador();
        ControladorJuego.jugadorAcceso.VerificarEstadosPorColor();
    }

    public void ResetearNivelJugador()
    {
        ControladorJuego.sistemaExperiencia.nivelJugador = 1;
        VerificarAtributosJugador();
        ControladorJuego.jugadorAcceso.VerificarEstadosPorColor();
    }

    public void SetearNivelJugador(int _value)
    {
        ControladorJuego.sistemaExperiencia.nivelJugador = _value;
        VerificarAtributosJugador();
    }
    public void ObtenerValoresSegunNivelJugador()
    {
        ControladorJuego.jugadorAcceso.ataque = ControladorJuego.ataqueBase;
        ControladorJuego.jugadorAcceso.speed = ControladorJuego.speedBase;
        ControladorJuego.jugadorAcceso.fireRate = ControladorJuego.fireRateBase;
        ControladorJuego.jugadorAcceso.defensa = ControladorJuego.defensaBase;
        ControladorJuego.jugadorAcceso.resistenciaMax = ControladorJuego.resistenciaBase;
        if (ControladorJuego.jugadorAcceso.resistencia >= ControladorJuego.resistenciaBase)
            ControladorJuego.jugadorAcceso.resistencia = ControladorJuego.resistenciaBase;
        ControladorJuego.jugadorAcceso.VerificarEstadosPorColor();
    }

    public void ActualizarExperiencia(int _valor)
    {
        ControladorJuego.sistemaExperiencia.expJugador = _valor;
    }

    public void GanarExperiencia(int _valor)
    {
        ControladorJuego.sistemaExperiencia.expJugador += _valor;
        int _valorNecesario = 0;

        _valorNecesario += ControladorJuego.sistemaExperiencia.nivelJugador * ControladorJuego.sistemaExperiencia.nivelJugador * 25 * ControladorJuego.sistemaExperiencia.nivelJugador;

        if (ControladorJuego.sistemaExperiencia.nivelJugador <= 10) _valorNecesario = Mathf.RoundToInt(_valorNecesario * 0.5f);
        else if (ControladorJuego.sistemaExperiencia.nivelJugador <= 20) _valorNecesario = Mathf.RoundToInt(_valorNecesario);
        else if (ControladorJuego.sistemaExperiencia.nivelJugador <= 30) _valorNecesario = Mathf.RoundToInt(_valorNecesario * 1.2f);
        else if (ControladorJuego.sistemaExperiencia.nivelJugador <= 40) _valorNecesario = Mathf.RoundToInt(_valorNecesario * 1.5f);
        else if (ControladorJuego.sistemaExperiencia.nivelJugador <= 48) _valorNecesario = Mathf.RoundToInt(_valorNecesario * 2f);
        else if (ControladorJuego.sistemaExperiencia.nivelJugador == 49) _valorNecesario = 6000000;


        if (ControladorJuego.sistemaExperiencia.expJugador >= _valorNecesario && ControladorJuego.sistemaExperiencia.nivelJugador <= 49)
        {
            SubirNivelJugador();
            GanarExperiencia(0); //Verifica si ha subido varios niveles al mismo tiempo
        }
    }


    public void SubirNivelJugador()
    {
        ControladorJuego.sistemaExperiencia.nivelJugador += 1;
        Debug.Log("Has subido de nivel");
        VerificarAtributosJugador();
    }
    public void VerificarAtributosJugador()
    {
        switch (ControladorJuego.sistemaExperiencia.nivelJugador)
        {
            case 1:
                ControladorJuego.ataqueBase = 1;
                ControladorJuego.defensaBase = 1;
                ControladorJuego.speedBase = 4;
                ControladorJuego.fireRateBase = 0.45f;
                ControladorJuego.resistenciaBase = 10;
                break;
            case 2:
                ControladorJuego.ataqueBase = 1;
                ControladorJuego.defensaBase = 1.1f;
                ControladorJuego.speedBase = 4.1f;
                ControladorJuego.fireRateBase = 0.4f;
                ControladorJuego.resistenciaBase = 10;

                break;
            case 3:
                ControladorJuego.ataqueBase = 1;
                ControladorJuego.defensaBase = 1.1f;
                ControladorJuego.speedBase = 4.3f;
                ControladorJuego.fireRateBase = 0.38f;
                ControladorJuego.resistenciaBase = 12;
                break;
            case 4:
                ControladorJuego.ataqueBase = 1;
                ControladorJuego.defensaBase = 1.1f;
                ControladorJuego.speedBase = 4.5f;
                ControladorJuego.fireRateBase = 0.35f;
                ControladorJuego.resistenciaBase = 12;
                break;
            case 5:
                ControladorJuego.ataqueBase = 2;
                ControladorJuego.defensaBase = 1.1f;
                ControladorJuego.speedBase = 4.5f;
                ControladorJuego.fireRateBase = 0.35f;
                ControladorJuego.resistenciaBase = 15;
                break;
            case 6:
                ControladorJuego.ataqueBase = 2;
                ControladorJuego.defensaBase = 1.2f;
                ControladorJuego.speedBase = 4.5f;
                ControladorJuego.fireRateBase = 0.32f;
                ControladorJuego.resistenciaBase = 15;
                break;
            case 7:
                ControladorJuego.ataqueBase = 2;
                ControladorJuego.defensaBase = 1.2f;
                ControladorJuego.speedBase = 4.5f;
                ControladorJuego.fireRateBase = 0.30f;
                ControladorJuego.resistenciaBase = 15;
                break;
            case 8:
                ControladorJuego.ataqueBase = 2;
                ControladorJuego.defensaBase = 1.3f;
                ControladorJuego.speedBase = 4.8f;
                ControladorJuego.fireRateBase = 0.30f;
                ControladorJuego.resistenciaBase = 18;
                break;
            case 9:
                ControladorJuego.ataqueBase = 2;
                ControladorJuego.defensaBase = 1.4f;
                ControladorJuego.speedBase = 4.9f;
                ControladorJuego.fireRateBase = 0.30f;
                ControladorJuego.resistenciaBase = 18;
                break;
            case 10:
                ControladorJuego.ataqueBase = 2;
                ControladorJuego.defensaBase = 1.5f;
                ControladorJuego.speedBase = 5f;
                ControladorJuego.fireRateBase = 0.25f;
                ControladorJuego.resistenciaBase = 20;
                break;
            case 11:
                ControladorJuego.ataqueBase = 2;
                ControladorJuego.defensaBase = 1.5f;
                ControladorJuego.speedBase = 5.2f;
                ControladorJuego.fireRateBase = 0.25f;
                ControladorJuego.resistenciaBase = 20;
                break;

        }
        ObtenerValoresSegunNivelJugador();
    }

    public void SubirNivelBlanco() { ControladorJuego.nivelBlanco += 1; if (ControladorJuego.nivelBlanco > 6) { ControladorJuego.nivelBlanco = 6; } ControladorJuego.jugadorAcceso.VerificarEstadosPorColor(); }
    public void SubirNivelRojo() { ControladorJuego.nivelRojo += 1; if (ControladorJuego.nivelRojo > 6) { ControladorJuego.nivelRojo = 6; } ControladorJuego.jugadorAcceso.VerificarEstadosPorColor(); }
    public void SubirNivelAzul() { ControladorJuego.nivelAzul += 1; if (ControladorJuego.nivelAzul > 6) { ControladorJuego.nivelAzul = 6; } ControladorJuego.jugadorAcceso.VerificarEstadosPorColor(); }
    public void SubirNivelAmarillo() { ControladorJuego.nivelAmarillo += 1; if (ControladorJuego.nivelAmarillo > 6) { ControladorJuego.nivelAmarillo = 6; } ControladorJuego.jugadorAcceso.VerificarEstadosPorColor(); }
    public void SubirNivelMorado() { ControladorJuego.nivelMorado += 1; if (ControladorJuego.nivelMorado > 6) { ControladorJuego.nivelMorado = 6; } ControladorJuego.jugadorAcceso.VerificarEstadosPorColor(); }
    public void SubirNivelNaranja() { ControladorJuego.nivelNaranja += 1; if (ControladorJuego.nivelNaranja > 6) { ControladorJuego.nivelNaranja = 6; } ControladorJuego.jugadorAcceso.VerificarEstadosPorColor(); }
    public void SubirNivelVerde() { ControladorJuego.nivelVerde += 1; if (ControladorJuego.nivelVerde > 6) { ControladorJuego.nivelVerde = 6; } ControladorJuego.jugadorAcceso.VerificarEstadosPorColor(); }
    public void SubirNivelNegro() { ControladorJuego.nivelNegro += 1; if (ControladorJuego.nivelNegro > 6) { ControladorJuego.nivelNegro = 6; } ControladorJuego.jugadorAcceso.VerificarEstadosPorColor(); }
    #endregion

    /*
    Nivel Jugador

    Nivel 1: 12.5         Nivel 26: 527280
    Nivel 2: 100          Nivel 27: 590490
    Nivel 3: 337.5        Nivel 28: 658560
    Nivel 4: 800          Nivel 29: 731670
    Nivel 5: 1562.5       Nivel 30: 810000
    Nivel 6: 2700         Nivel 31: 1117162.5
    Nivel 7: 4287.5       Nivel 32: 1228800
    Nivel 8: 6400         Nivel 33: 1347637.5
    Nivel 9: 9112.5       Nivel 34: 1473900
    Nivel 10: 12500       Nivel 35: 1607812.5
    Nivel 11: 33275       Nivel 36: 1748600
    Nivel 12: 43200       Nivel 37: 1899487.5
    Nivel 13: 54925       Nivel 38: 2057700
    Nivel 14: 68600       Nivel 39: 2224462.5
    Nivel 15: 84375       Nivel 40: 2400000
    Nivel 16: 102400      Nivel 41: 3446050
    Nivel 17: 122825      Nivel 42: 3704400
    Nivel 18: 145800      Nivel 43: 3975350
    Nivel 19: 171475      Nivel 44: 4259200
    Nivel 20: 200000      Nivel 45: 4556250
    Nivel 21: 277830      Nivel 46: 4866800
    Nivel 22: 319440      Nivel 47: 5191150
    Nivel 23: 365010      Nivel 48: 5529600
    Nivel 24: 414720      Nivel 49: 6000000
    Nivel 25: 468750      Nivel 50: -------
    */
}
