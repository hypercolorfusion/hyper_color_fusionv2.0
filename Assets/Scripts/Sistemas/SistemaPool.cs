using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SistemaPool : MonoBehaviour
{
    #region Elementos para el Sistema
    public GameObject balaJugador, balaJugadorGrande;
    public GameObject balaEnemiga, balaEnemigaGrande;
    public GameObject feedback;
    public GameObject fuego;
    public GameObject barraVida;
    #endregion

    #region Balas Jugador
    [HideInInspector] public List<GameObject> balaJugadorActiva;
    [HideInInspector] public List<GameObject> balaJugadorInactiva;

    [HideInInspector] public List<GameObject> balaJugadorGrandeActiva;
    [HideInInspector] public List<GameObject> balaJugadorGrandeInactiva;
    #endregion

    #region Balas Enemigas
    private List<GameObject> balaEnemigaActiva = new List<GameObject>();
    private List<GameObject> balaEnemigaInactiva = new List<GameObject>();

    private List<GameObject> balaEnemigaGrandeActiva = new List<GameObject>();
    private List<GameObject> balaEnemigaGrandeInactiva = new List<GameObject>();
    #endregion

    #region FeedBack
    private List<GameObject> feedBackActivo = new List<GameObject>();
    private List<GameObject> feedBackInactivo = new List<GameObject>();
    #endregion

    #region FeedBack
    private List<GameObject> fuegoActivo = new List<GameObject>();
    private List<GameObject> fuegoInactivo = new List<GameObject>();
    #endregion

    #region Barra Vida
    private List<GameObject> barraVidaActivo = new List<GameObject>();
    private List<GameObject> barraVidaInactivo = new List<GameObject>();
    #endregion

    void Awake()
    {
        if (ControladorJuego.adminJuego.ObtenerSistemaPool() == null)
        {
            ControladorJuego.adminJuego.SetearSistemaPool(this);
            DontDestroyOnLoad(transform.parent);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    #region Bala Jugador
    public GameObject DispararJugador(Vector3 _posicion, Quaternion _rotacion)
    {
        GameObject _nuevaBala;
        if (balaJugadorInactiva.Count <= 0)
        {
            _nuevaBala = Instantiate(balaJugador, _posicion, _rotacion);
        }
        else
        {
            _nuevaBala = balaJugadorInactiva[0];
            _nuevaBala.SetActive(true);
            _nuevaBala.transform.position = _posicion;
            _nuevaBala.transform.rotation = _rotacion;
            balaJugadorInactiva.Remove(_nuevaBala);
        }
        _nuevaBala.GetComponent<BalaJugador>().Inicializador();
        _nuevaBala.GetComponent<BalaJugador>().SetearBalaJugador(ControladorJuego.sistemaMagiaColor.ObtenerMagiaColorJugador());
        balaJugadorActiva.Add(_nuevaBala);
        return _nuevaBala;
    }

    public GameObject DispararBalaMoradaJugador(Vector3 _posicion, Quaternion _rotacion, float _valor, bool _derecha)
    {
        GameObject _nuevaBala;
        if (balaJugadorInactiva.Count <= 0)
        {
            _nuevaBala = Instantiate(balaJugador, _posicion, _rotacion);
        }
        else
        {
            _nuevaBala = balaJugadorInactiva[0];
            _nuevaBala.SetActive(true);
            _nuevaBala.transform.position = _posicion;
            _nuevaBala.transform.rotation = _rotacion;
            balaJugadorInactiva.Remove(_nuevaBala);
        }
        _nuevaBala.GetComponent<BalaJugador>().Inicializador();
        _nuevaBala.GetComponent<BalaJugador>().SetearBalaMoradaJugador(ControladorJuego.sistemaMagiaColor.ObtenerMagiaColorJugador(), _valor, _derecha);
        balaJugadorActiva.Add(_nuevaBala);
        return _nuevaBala;
    }

    public GameObject BalaDisparaJugadorBala(Vector3 _posicion, Quaternion _rotacion, MagiaColor _color)
    {
        GameObject _nuevaBala;
        if (balaJugadorInactiva.Count <= 0)
        {
            _nuevaBala = Instantiate(balaJugador, _posicion, _rotacion);
        }
        else
        {
            _nuevaBala = balaJugadorInactiva[0];
            _nuevaBala.SetActive(true);
            _nuevaBala.transform.position = _posicion;
            _nuevaBala.transform.rotation = _rotacion;
            balaJugadorInactiva.Remove(_nuevaBala);
        }
        _nuevaBala.GetComponent<BalaJugador>().Inicializador();
        _nuevaBala.GetComponent<BalaJugador>().SetearBalaJugador(_color);
        balaJugadorActiva.Add(_nuevaBala);
        return _nuevaBala;
    }

    public void OcultarBalaJugador(GameObject _bala)
    {
        balaJugadorActiva.Remove(_bala);
        balaJugadorInactiva.Add(_bala);
    }

    public GameObject DispararJugadorBalaGrande(Vector3 _posicion, Quaternion _rotacion, MagiaColor _color)
    {
        GameObject _nuevaBala;
        if (balaJugadorGrandeInactiva.Count <= 0)
        {
            _nuevaBala = Instantiate(balaJugadorGrande, _posicion, _rotacion);
            _nuevaBala.transform.SetParent(transform);
        }
        else
        {
            _nuevaBala = balaJugadorGrandeInactiva[0];
            _nuevaBala.SetActive(true);
            _nuevaBala.transform.position = _posicion;
            _nuevaBala.transform.rotation = _rotacion;
            balaJugadorGrandeInactiva.Remove(_nuevaBala);
        }
        _nuevaBala.GetComponent<BalaJugadorGrande>().Inicializador();
        _nuevaBala.GetComponent<BalaJugadorGrande>().SetearBalaJugador(_color);
        balaJugadorGrandeActiva.Add(_nuevaBala);
        return _nuevaBala;
    }

    public void OcultarBalaJugadorGrande(GameObject _bala)
    {
        balaJugadorGrandeActiva.Remove(_bala);
        balaJugadorGrandeInactiva.Add(_bala);
    }
    #endregion


    #region Balas Enemigas

    public void DispararBalaEnemiga(Vector3 _posicion, Quaternion _rotacion, MagiaColor _color, int _valueDamage, float _velocidadBonus)
    {

        GameObject _nuevaBala;
        if (balaEnemigaInactiva.Count <= 0)
        {
            _nuevaBala = Instantiate(balaEnemiga, _posicion, _rotacion);
            _nuevaBala.transform.SetParent(transform);
        }
        else
        {
            _nuevaBala = balaEnemigaInactiva[0];
            _nuevaBala.SetActive(true);
            _nuevaBala.transform.position = _posicion;
            _nuevaBala.transform.rotation = _rotacion;
            balaEnemigaInactiva.Remove(_nuevaBala);
        }
        _nuevaBala.GetComponent<BalaEnemiga>().InicializadorBalaNormal();
        _nuevaBala.GetComponent<BalaEnemiga>().SetearBala(_color, _valueDamage, _velocidadBonus);
        balaEnemigaActiva.Add(_nuevaBala);

    }

    public void OcultarBalaEnemigo(GameObject _bala)
    {
        balaEnemigaActiva.Remove(_bala);
        balaEnemigaInactiva.Add(_bala);
    }

    public void DispararBalaGrandeEnemigo(Vector3 _posicion, Quaternion _rotacion, MagiaColor _color, int _valueDamage, float _velocidadBonus)
    {
        GameObject _nuevaBala;
        if (balaEnemigaGrandeInactiva.Count <= 0)
        {
            _nuevaBala = Instantiate(balaEnemigaGrande, _posicion, _rotacion);
            _nuevaBala.transform.SetParent(transform);
        }
        else
        {
            _nuevaBala = balaEnemigaGrandeInactiva[0];
            _nuevaBala.SetActive(true);
            _nuevaBala.transform.position = _posicion;
            _nuevaBala.transform.rotation = _rotacion;
            _nuevaBala.GetComponent<BalaEnemigaGrande>().Inicializador();
            balaEnemigaGrandeInactiva.Remove(_nuevaBala);
        }
        _nuevaBala.GetComponent<BalaEnemigaGrande>().SetearBala(_color, _valueDamage * 3, _velocidadBonus);
        balaEnemigaGrandeActiva.Add(_nuevaBala);
    }

    public void OcultarBalaGrandeEnemigo(GameObject _bala)
    {
        balaEnemigaGrandeActiva.Remove(_bala);
        balaEnemigaGrandeInactiva.Add(_bala);
    }

    public void DispararBalaEnemigoVectorial(Vector3 _posicion, Quaternion _rotacion, MagiaColor _color,
     int _valueDamage, bool _inteligente, float _velocidadBonus, float _tiempoEspera, int _giros, float _duracionBala)
    {
        GameObject _nuevaBala;
        if (balaEnemigaInactiva.Count <= 0)
        { //Crear nueva bala
            _nuevaBala = Instantiate(balaEnemiga, _posicion, _rotacion);
            _nuevaBala.transform.SetParent(transform);
        }
        else
        {
            _nuevaBala = balaEnemigaInactiva[0];
            _nuevaBala.SetActive(true);
            _nuevaBala.transform.position = _posicion;
            _nuevaBala.transform.rotation = _rotacion;
            balaEnemigaInactiva.Remove(_nuevaBala);
        }
        _nuevaBala.GetComponent<BalaEnemiga>().InicializadorBalaVectorial(_tiempoEspera, _giros, _duracionBala);
        _nuevaBala.GetComponent<BalaEnemiga>().SetearBalaVectorial(_color, _valueDamage, _inteligente, _velocidadBonus);
        balaEnemigaActiva.Add(_nuevaBala);
    }

    public void DispararBalaEnemigaTeledirigido(Vector3 _posicion, Quaternion _rotacion, MagiaColor _color,
     int _valueDamage, float _velocidadBonus, float _velocidadRotacion)
    {
        GameObject _nuevaBala;
        if (balaEnemigaInactiva.Count <= 0)
        {
            _nuevaBala = Instantiate(balaEnemiga, _posicion, _rotacion);
            _nuevaBala.transform.SetParent(transform);
        }
        else
        {
            _nuevaBala = balaEnemigaInactiva[0];
            _nuevaBala.SetActive(true);
            _nuevaBala.transform.position = _posicion;
            _nuevaBala.transform.rotation = _rotacion;
            balaEnemigaInactiva.Remove(_nuevaBala);
        }
        _nuevaBala.GetComponent<BalaEnemiga>().InicializadorBalaTeledirigido(_velocidadRotacion);
        _nuevaBala.GetComponent<BalaEnemiga>().SetearBala(_color, _valueDamage, _velocidadBonus);
        balaEnemigaActiva.Add(_nuevaBala);
    }

    public void DispararBalaEnemigoRebote(Vector3 _posicion, Quaternion _rotacion, MagiaColor _color, int _valueDamage, float _velocidadBonus, int _nivel)
    {
        GameObject _nuevaBala;
        if (balaEnemigaInactiva.Count <= 0)
        {
            _nuevaBala = Instantiate(balaEnemiga, _posicion, _rotacion);
            _nuevaBala.transform.SetParent(transform);
        }
        else
        {
            _nuevaBala = balaEnemigaInactiva[0];
            _nuevaBala.SetActive(true);
            _nuevaBala.transform.position = _posicion;
            _nuevaBala.transform.rotation = _rotacion;
            balaEnemigaInactiva.Remove(_nuevaBala);
        }
        _nuevaBala.GetComponent<BalaEnemiga>().InicializadorBalaRebote(_nivel);
        _nuevaBala.GetComponent<BalaEnemiga>().SetearBala(_color, _valueDamage, _velocidadBonus);
        balaEnemigaActiva.Add(_nuevaBala);
    }
    #endregion

    #region ForzarApagado
    public void ForzarOcultarElementos()
    {
        for (int i = 0; i < balaJugadorActiva.Count; i++)
        {
            BalaJugador _balaActual = balaJugadorActiva[i].GetComponent<BalaJugador>();
            _balaActual.forzarDestruccion = true;
            _balaActual.ForzarDestruccion();
            balaJugadorInactiva.Add(balaJugadorActiva[i]);
        }
        balaJugadorActiva.Clear();

        for (int i = 0; i < balaJugadorGrandeActiva.Count; i++)
        {
            BalaJugadorGrande _balaActual = balaJugadorGrandeActiva[i].GetComponent<BalaJugadorGrande>();
            _balaActual.forzarDestruccion = true;
            _balaActual.ForzarDestruccion();
            balaJugadorGrandeInactiva.Add(balaJugadorGrandeActiva[i]);
        }
        balaJugadorGrandeActiva.Clear();

        for (int i = 0; i < balaEnemigaActiva.Count; i++)
        {
            BalaEnemiga _balaActual = balaEnemigaActiva[i].GetComponent<BalaEnemiga>();
            _balaActual.forzarDestruccion = true;
            _balaActual.ForzarDestruccion();
            balaEnemigaInactiva.Add(balaEnemigaActiva[i]);
        }
        balaEnemigaActiva.Clear();

        for (int i = 0; i < balaEnemigaGrandeActiva.Count; i++)
        {
            BalaEnemigaGrande _balaActual = balaEnemigaGrandeActiva[i].GetComponent<BalaEnemigaGrande>();
            _balaActual.forzarDestruccion = true;
            _balaActual.ForzarDestruccion();
            balaEnemigaGrandeInactiva.Add(balaEnemigaGrandeActiva[i]);

        }
        balaEnemigaGrandeActiva.Clear();
    }

    public void ForzarOcultarBarradeVida()
    {
        for (int i = 0; i < barraVidaActivo.Count; i++)
        {
            BarraVida _barraVida = barraVidaActivo[i].GetComponent<BarraVida>();
            _barraVida.ForzarDestruccion();
            barraVidaInactivo.Add(barraVidaActivo[i]);
        }
        barraVidaActivo.Clear();
    }
    #endregion


    #region Feedback

    public void MostrarFeedBackAtaque(Vector3 _posicion, MagiaColor _color, int _valueDamage, bool _jugador)
    {
        GameObject _nuevoFeedBack;
        Quaternion _rotacion = new Quaternion(0, 0, 0, 0);
        _posicion = new Vector3(_posicion.x, _posicion.y, 0.3f);

        if (feedBackInactivo.Count <= 0)
        {
            _nuevoFeedBack = Instantiate(feedback, _posicion, _rotacion);
            _nuevoFeedBack.transform.SetParent(transform);
        }
        else
        {
            _nuevoFeedBack = feedBackInactivo[0];
            _nuevoFeedBack.transform.SetParent(transform);
            _nuevoFeedBack.SetActive(true);
            _nuevoFeedBack.transform.position = _posicion;
            _nuevoFeedBack.transform.rotation = _rotacion;
            _nuevoFeedBack.GetComponent<DamageFeedBack>().Inicializador();
            feedBackInactivo.Remove(_nuevoFeedBack);
        }

        _nuevoFeedBack.GetComponent<DamageFeedBack>().SetearFeedBack(_valueDamage, _color, _jugador);
        feedBackActivo.Add(_nuevoFeedBack);
    }

    public void OcultarFeedBackAtaque(GameObject _feedback)
    {
        feedBackActivo.Remove(_feedback);
        feedBackInactivo.Add(_feedback);
    }

    #endregion

    #region Fuego

    public void MostrarFuego(Vector3 _posicion, bool _jugador)
    {
        GameObject _nuevoFuego;
        Quaternion _rotacion = new Quaternion(0, 0, 0, 0);
        _posicion = new Vector3(_posicion.x, _posicion.y, 0.3f);

        if (fuegoInactivo.Count <= 0)
        {
            _nuevoFuego = Instantiate(fuego, _posicion, _rotacion);
            _nuevoFuego.transform.SetParent(transform);
        }
        else
        {
            _nuevoFuego = fuegoInactivo[0];
            _nuevoFuego.SetActive(true);
            _nuevoFuego.transform.position = _posicion;
            _nuevoFuego.transform.rotation = _rotacion;
            fuegoInactivo.Remove(_nuevoFuego);
        }
        _nuevoFuego.GetComponent<Fuego>().Inicializador(_jugador);
        fuegoActivo.Add(_nuevoFuego);
    }

    public void OcultarFuego(GameObject _fuego)
    {
        fuegoActivo.Remove(_fuego);
        fuegoInactivo.Add(_fuego);
    }
    #endregion


    #region BarraVida
    public void MostrarBarraVida(Vector3 _offSet, Enemigo _padre, float _escala)
    {
        GameObject _nuevaBarraVida;
        Quaternion _rotacion = new Quaternion(0, 0, 0, 0);
        Vector3 _ubicacion = new Vector3(_padre.transform.position.x, _padre.transform.position.y, 0.3f);

        if (barraVidaInactivo.Count <= 0)
        {
            _nuevaBarraVida = Instantiate(barraVida, _ubicacion, _rotacion);
            _nuevaBarraVida.transform.SetParent(transform);
            _nuevaBarraVida.name = "Barra de vida" + barraVidaActivo.Count;
        }
        else
        {
            _nuevaBarraVida = barraVidaInactivo[0];
            _nuevaBarraVida.transform.position = _ubicacion;
            _nuevaBarraVida.transform.rotation = _rotacion;
            barraVidaInactivo.Remove(_nuevaBarraVida);

        }
        _nuevaBarraVida.GetComponent<BarraVida>().Inicializador(_offSet, _padre, _escala);
        barraVidaActivo.Add(_nuevaBarraVida);
    }

    public void OcultarBarraVida(GameObject _barraVida)
    {
        barraVidaActivo.Remove(_barraVida);
        barraVidaInactivo.Add(_barraVida);
        _barraVida.SetActive(false);
    }

    #endregion

}