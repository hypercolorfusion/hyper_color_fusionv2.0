using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class SistemaMapa : MonoBehaviour
{

    public GameObject PanelMapa;
    public GameObject mapa;
    public GameObject mapaGlobal;
    public RectTransform zoom;
    public Transform scroll;

    public GameObject mapaPrefab;
    public GameObject coneccionPrefab;


    public Image arriba, abajo, derecha, izquierda, centro;
    public GameObject coneccionAb, coneccionAr, coneccionDe, coneccionIz;
    public List<GameObject> areasGeneradas;
    public List<GameObject> coneccionesGeneradas;
    public bool mapaInteractuable;



    void Awake()
    {
        ActualizarMapaInteractuable(false);
        if (ControladorJuego.adminUi.ObtenerSistemaMapa() == null)
        {
            ControladorJuego.adminUi.SetearSistemaMapa(this);
            DontDestroyOnLoad(transform.parent);
        }
        else Destroy(gameObject);

    }

    public void ActualizarMapaInteractuable(bool _valor)
    {
        mapaInteractuable = _valor;
    }

    public void EsconderMapa()
    {
        PanelMapa.SetActive(!PanelMapa.activeSelf);
    }

    public void ActualizarMiniMapa(MagiaColor _color, Vector2 _areaID)
    {
        Area _areaEncontrada = ControladorJuego.sistemaArea.ObtenerAreaSegunID(_areaID);
        foreach (Portal _portalEncontrado in _areaEncontrada.portales)
        {
            switch (_portalEncontrado.direccion)
            {
                case PortalDir.der:
                    coneccionDe.GetComponent<Image>().color = ControladorJuego.sistemaMagiaColor.ObtenerColor(_portalEncontrado.magiaColor);
                    coneccionDe.SetActive(!_portalEncontrado.bloqueado);
                    derecha.gameObject.SetActive(_portalEncontrado.destino != null);
                    if (!_portalEncontrado.bloqueado && _portalEncontrado.destino)
                        derecha.color = ControladorJuego.sistemaMagiaColor.ObtenerColor(_portalEncontrado.magiaColor);
                    else
                        derecha.color = ControladorJuego.sistemaMagiaColor.ObtenerColor(MagiaColor.sinCOLOR);
                    break;
                case PortalDir.izq:
                    coneccionIz.GetComponent<Image>().color = ControladorJuego.sistemaMagiaColor.ObtenerColor(_portalEncontrado.magiaColor);
                    coneccionIz.SetActive(!_portalEncontrado.bloqueado);
                    izquierda.gameObject.SetActive(_portalEncontrado.destino != null);
                    if (!_portalEncontrado.bloqueado && _portalEncontrado.destino)
                        izquierda.color = ControladorJuego.sistemaMagiaColor.ObtenerColor(_portalEncontrado.magiaColor);
                    else
                        izquierda.color = ControladorJuego.sistemaMagiaColor.ObtenerColor(MagiaColor.sinCOLOR);
                    break;
                case PortalDir.arr:
                    coneccionAr.GetComponent<Image>().color = ControladorJuego.sistemaMagiaColor.ObtenerColor(_portalEncontrado.magiaColor);
                    coneccionAr.SetActive(!_portalEncontrado.bloqueado);
                    arriba.gameObject.SetActive(_portalEncontrado.destino != null);
                    if (!_portalEncontrado.bloqueado && _portalEncontrado.destino)
                        arriba.color = ControladorJuego.sistemaMagiaColor.ObtenerColor(_portalEncontrado.magiaColor);
                    else
                        arriba.color = ControladorJuego.sistemaMagiaColor.ObtenerColor(MagiaColor.sinCOLOR);
                    break;
                case PortalDir.abj:
                    coneccionAb.GetComponent<Image>().color = ControladorJuego.sistemaMagiaColor.ObtenerColor(_portalEncontrado.magiaColor);
                    coneccionAb.SetActive(!_portalEncontrado.bloqueado);
                    abajo.gameObject.SetActive(_portalEncontrado.destino != null);
                    if (!_portalEncontrado.bloqueado && _portalEncontrado.destino)
                        abajo.color = ControladorJuego.sistemaMagiaColor.ObtenerColor(_portalEncontrado.magiaColor);
                    else
                        abajo.color = ControladorJuego.sistemaMagiaColor.ObtenerColor(MagiaColor.sinCOLOR);
                    break;
            }
        }
        centro.color = ControladorJuego.sistemaMagiaColor.ObtenerColor(_color);
    }

    public void MapaZoom(float _valor)
    {
        _valor = _valor / 10f;
        zoom.localScale = new Vector3(zoom.localScale.x + _valor, zoom.localScale.y + _valor, zoom.localScale.z + _valor);
        if (zoom.localScale.x <= 0.5f) zoom.localScale = Vector3.one * 0.5f;
        else if (zoom.localScale.x >= 3f) zoom.localScale = Vector3.one * 3;
    }

    public void AbrirMapa()
    {
        if (ControladorJuego.sistemaMisiones.tutorial && !mapaInteractuable)
        {
            ControladorJuego.sistemaMensajes.MostarTexto("ERRORES", "MAPA", "AYUDA", true);
            return;
        }

        ControladorJuego.adminUi.pausaPorMapa = true;
        mapaGlobal.SetActive(true);
        mapa.SetActive(false);
        ControladorJuego.adminJuego.PauseGame();
        for (int i = 0; i < ControladorJuego.sistemaArea.areasGeneradas.Count; i++)
        {
            if (ControladorJuego.sistemaArea.ObtenerID_AreaJugadorActual() == ControladorJuego.sistemaArea.areasGeneradas[i].ID)
                areasGeneradas[i].GetComponent<Image>().color = ControladorJuego.sistemaMagiaColor.ObtenerColor(MagiaColor.verde);
            else
                areasGeneradas[i].GetComponent<Image>().color = ControladorJuego.sistemaMagiaColor.ObtenerColor(MagiaColor.sinCOLOR);
        }
    }


    public void ActualizarMapa()
    {
        StartCoroutine(EsperaParaActualizar());
    }

    public IEnumerator EsperaParaActualizar()
    {
        yield return new WaitForSeconds(0.2f);


        for (int i = areasGeneradas.Count; i < ControladorJuego.sistemaArea.areasGeneradas.Count; i++)
        {
            Area _AreaEncontrada = ControladorJuego.sistemaArea.areasGeneradas[i];
            Vector2 _idArea = new Vector2(_AreaEncontrada.ID.x / 50 * 18, _AreaEncontrada.ID.y / 50 * 18);
            GameObject _nuevoMapa = Instantiate(mapaPrefab);
            _nuevoMapa.transform.SetParent(scroll);
            _nuevoMapa.GetComponent<RectTransform>().localPosition = new Vector3(_idArea.x, _idArea.y, 0f);
            areasGeneradas.Add(_nuevoMapa);
            foreach (Portal _portalEncontrado in _AreaEncontrada.portales)
            {
                if (!_portalEncontrado.bloqueado && !_portalEncontrado.destino)
                {
                    GameObject _nuevaConeccion = Instantiate(coneccionPrefab);
                    _nuevaConeccion.transform.SetParent(scroll);
                    _nuevaConeccion.GetComponent<Image>().color = ControladorJuego.sistemaMagiaColor.ObtenerColor(_portalEncontrado.magiaColor);
                    switch (_portalEncontrado.direccion)
                    {
                        case PortalDir.der:
                            _nuevaConeccion.GetComponent<RectTransform>().localPosition = new Vector3(_idArea.x + 9, _idArea.y, 0f);
                            break;
                        case PortalDir.izq:
                            _nuevaConeccion.GetComponent<RectTransform>().localPosition = new Vector3(_idArea.x - 9, _idArea.y, 0f);
                            break;
                        case PortalDir.arr:
                            _nuevaConeccion.GetComponent<RectTransform>().localPosition = new Vector3(_idArea.x, _idArea.y + 9, 0f);
                            _nuevaConeccion.GetComponent<RectTransform>().Rotate(Vector3.forward * 90);
                            break;
                        case PortalDir.abj:
                            _nuevaConeccion.GetComponent<RectTransform>().localPosition = new Vector3(_idArea.x, _idArea.y - 9, 0f);
                            _nuevaConeccion.GetComponent<RectTransform>().Rotate(Vector3.forward * 90);
                            break;
                    }
                    coneccionesGeneradas.Add(_nuevaConeccion);
                }
            }
        }
    }

    public void ForzarGenerarConeccion()
    {
        //Genera Conección en el mapa segun el ID del Área
        for (int i = 0; i < ControladorJuego.sistemaArea.areasGeneradas.Count; i++)
        {
            foreach (Portal _portalEncontrado in ControladorJuego.sistemaArea.areasGeneradas[i].portales)
            {
                if (_portalEncontrado.destino && !_portalEncontrado.bloqueado)
                {
                    Vector3 _verificarArea = ((_portalEncontrado.padre.position / 50 * 18) + (_portalEncontrado.destino.GetComponent<Portal>().padre.position / 50 * 18)) / 2;
                    //Comprobar si ya existe para no generar doble
                    bool _existe = false;
                    foreach (GameObject _coneccion in coneccionesGeneradas)
                    {
                        if (_coneccion.GetComponent<RectTransform>().localPosition == _verificarArea)
                        {
                            _existe = true;
                            break;
                        }
                    }
                    //Genera la nueva conección 
                    if (!_existe)
                    {
                        GameObject _nuevaConeccion = Instantiate(coneccionPrefab);
                        _nuevaConeccion.transform.SetParent(scroll);
                        _nuevaConeccion.transform.name = "Concección forzada";
                        _nuevaConeccion.GetComponent<Image>().color = ControladorJuego.sistemaMagiaColor.ObtenerColor(_portalEncontrado.magiaColor);
                        _nuevaConeccion.GetComponent<RectTransform>().localPosition = _verificarArea;
                        if (_portalEncontrado.padre.position.y / 50 * 18 != _portalEncontrado.destino.GetComponent<Portal>().padre.position.y / 50 * 18)
                            _nuevaConeccion.GetComponent<RectTransform>().Rotate(Vector3.forward * 90);
                        coneccionesGeneradas.Add(_nuevaConeccion);
                    }
                }
            }
        }
        //Actualiza el minimapa del ID área (Si no es es el area del jugador usado esto se debe volver a actualizar el Minimapa aparte)
    }

    public void CerrarMapa()
    {
        ControladorJuego.adminJuego.ResumeGame();
        ControladorJuego.adminUi.pausaPorMapa = false;
        mapa.SetActive(true);
        mapaGlobal.SetActive(false);
        zoom.localScale = Vector3.one;
        if (ControladorJuego.sistemaMisiones.tutorial && mapaInteractuable)
            ControladorJuego.sistemaMisiones.CompletarMisionTutorial("MAPA");
    }

    public void EliminarMapa()
    {
        foreach (GameObject _objeto in areasGeneradas) Destroy(_objeto);
        foreach (GameObject _objeto in coneccionesGeneradas) Destroy(_objeto);

        areasGeneradas.Clear();
        coneccionesGeneradas.Clear();
    }
}
