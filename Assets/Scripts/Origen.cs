﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Origen : MonoBehaviour
{

    #region Metodos para Mirar

    //Apuntar un objeto a otro objeto
    public void LookAt2D(Transform _target, ref Transform _eje)
    {
        Vector3 _dir = _target.position - transform.position;
        float _angle = Mathf.Atan2(_dir.y, _dir.x) * Mathf.Rad2Deg;
        _eje.rotation = Quaternion.AngleAxis(_angle, Vector3.forward);

    }

    //Apuntar un objeto a otro objeto
    public void LookAt2D(Vector3 _target, ref Transform _eje)
    {
        Vector3 _dir = _target - transform.position;
        float _angle = Mathf.Atan2(_dir.y, _dir.x) * Mathf.Rad2Deg;
        _eje.rotation = Quaternion.AngleAxis(_angle, Vector3.forward);
    }

    //Apuntar a otro bojeto
    public void LookAt2D(Vector3 _target)
    {
        Vector3 _dir = _target - transform.position;
        float _angle = Mathf.Atan2(_dir.y, _dir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(_angle, Vector3.forward);
    }

    //Apuntar a otro bojeto
    public void LookAt2D(Vector3 _target, Vector3 _origen)
    {
        Vector3 _dir = _target - _origen;
        float _angle = Mathf.Atan2(_dir.y, _dir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(_angle, Vector3.forward);
    }

    //Apunta hacia donde donde se dirige el jugador.
    public void LookAt2DSmart(Transform _target, ref Transform _eje)
    {
        int _y = ControladorJuego.jugadorAcceso.y;
        int _x = ControladorJuego.jugadorAcceso.x;

        Vector3 _smart = new Vector3(_target.position.x + _x * 3, _target.position.y + _y * 3, _target.position.z);
        Vector3 _dir = _smart - transform.position;
        float _angle = Mathf.Atan2(_dir.y, _dir.x) * Mathf.Rad2Deg;
        _eje.rotation = Quaternion.AngleAxis(_angle, Vector3.forward);
    }
    #endregion
}