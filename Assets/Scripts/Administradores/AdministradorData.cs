﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdministradorData : MonoBehaviour
{

    void Awake()
    {
        if (ControladorJuego.adminData == null)
        {
            ControladorJuego.adminData = this;
            DontDestroyOnLoad(transform.parent.gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }


    #region CheckPoint
    public void SetearCheckPoint(Vector3 _pos)
    {
        ControladorJuego.posCheckPoint = _pos;
    }
    #endregion
    
    #region Slots
    public void SetearSlot(int _valor)
    {
        ControladorJuego.sistemaGuardado.slot = _valor;
    }
    #endregion


    #region Inicializaciín Sistema Guardado
    public SistemaGuardado ObtenersistemaGuardado()
    {
        if (ControladorJuego.sistemaGuardado) return ControladorJuego.sistemaGuardado;
        else return null;
    }
    public void SetearSistemaGuardado(SistemaGuardado _codigo) { ControladorJuego.sistemaGuardado = _codigo; }
    #endregion
}