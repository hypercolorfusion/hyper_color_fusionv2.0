﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdministradorUI : MonoBehaviour
{
    [Header("Paneles UI")]
    [SerializeField] Slider barra_resistencia_jugador; //Slider que muestra la resistencia del jugador

    [Header("Sistema de Dialogo")]
    public bool pausaPorMapa;
    public bool pausaPorMensaje;

    public GameObject colorRojo, colorAzul, colorAmarillo;
    public GameObject usableColorRojo, usableColorAzul, usableColorAmarillo;


    public Image colorJugador;

    public RectTransform posFeedbackUI;


    void Awake() // Ui admin spawnea si no existe.
    {
        if (ControladorJuego.adminUi == null) ControladorJuego.adminUi = this;
        else Destroy(gameObject);
    }

    private void Start()
    {
        transform.SetParent(ControladorJuego.adminJuego.transform.parent);
    }

    #region Jugador
    public void MostrarColorPlayer(bool _rojo, bool _azul, bool _amarillo)
    {//HUD DE COLOR
        colorRojo.SetActive(_rojo);
        colorAzul.SetActive(_azul);
        colorAmarillo.SetActive(_amarillo);
        usableColorRojo.SetActive(ControladorJuego.magiaRoja);
        usableColorAzul.SetActive(ControladorJuego.magiaAzul);
        usableColorAmarillo.SetActive(ControladorJuego.magiaAmarilla);
        colorJugador.color = ControladorJuego.sistemaMagiaColor.ObtenerColor(ControladorJuego.jugadorAcceso.magiaColor);
    }

    public void MostrarColorPlayer()
    {//HUD DE COLOR
        colorRojo.SetActive(ControladorJuego.jugadorAcceso.rojo);
        colorAzul.SetActive(ControladorJuego.jugadorAcceso.azul);
        colorAmarillo.SetActive(ControladorJuego.jugadorAcceso.amarillo);
        usableColorRojo.SetActive(ControladorJuego.magiaRoja);
        usableColorAzul.SetActive(ControladorJuego.magiaAzul);
        usableColorAmarillo.SetActive(ControladorJuego.magiaAmarilla);
        colorJugador.color = ControladorJuego.sistemaMagiaColor.ObtenerColor(ControladorJuego.jugadorAcceso.magiaColor);
    }

    public void AjustarResistencia()
    {
        barra_resistencia_jugador.maxValue = ControladorJuego.jugadorAcceso.resistenciaMax;
        barra_resistencia_jugador.value = ControladorJuego.jugadorAcceso.resistencia;
    }
    #endregion

    public void OcultarPaneles()
    {
        if (ControladorJuego.adminUi.pausaPorMapa)
            ControladorJuego.sistemaMapa.CerrarMapa();

        if (ControladorJuego.adminUi.pausaPorMensaje)
            ControladorJuego.sistemaMensajes.CerrarTexto();
    }


    #region Inicializaciín Sistema Mapa
    public SistemaMapa ObtenerSistemaMapa()
    {
        if (ControladorJuego.sistemaMapa) return ControladorJuego.sistemaMapa;
        else return null;
    }
    public void SetearSistemaMapa(SistemaMapa _codigo) { ControladorJuego.sistemaMapa = _codigo; }
    #endregion

    #region Inicializaciín Sistema Mensajes
    public SistemaMensajes ObtenerSistemaMensajes()
    {
        if (ControladorJuego.sistemaMensajes) return ControladorJuego.sistemaMensajes;
        else return null;
    }
    public void SetearSistemaMensajes(SistemaMensajes _codigo) { ControladorJuego.sistemaMensajes = _codigo; }
    #endregion
}