﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdministradorJuego : MonoBehaviour
{
    // Start is called before the first frame update

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    public bool juegoPausado;
    private Camera camara;

    void Awake()
    {
        if (ControladorJuego.adminJuego == null)
        {
            ControladorJuego.adminJuego = this;
            DontDestroyOnLoad(transform.parent.gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
        juegoPausado = false;
    }

    public void SetearJugador(Jugador _jugador)
    {
        ControladorJuego.jugadorAcceso = _jugador;
        _jugador.feedBackPos.position = Camera.main.ScreenToWorldPoint(new Vector3(ControladorJuego.adminUi.posFeedbackUI.position.x, ControladorJuego.adminUi.posFeedbackUI.position.y, 1));
        ControladorJuego.posFeedBackJugador = _jugador.feedBackPos;
    }

    public void SetearCamara()
    {
        camara = Camera.main;
    }
    public Camera ObtenerCamara()
    {
        if (!camara) SetearCamara();
        return camara;
    }

    #region Jugador

    public float DistanciaJugador(Vector3 _pos) { return Vector3.Distance(_pos, ControladorJuego.jugadorAcceso.transform.position); }

    public bool JugadorEnMovimiento() { return (ControladorJuego.jugadorAcceso.x != 0 || ControladorJuego.jugadorAcceso.y != 0); }
    #endregion



    #region  Estados de Juego

    public void SeteartTutorial(bool _valor)
    {
        ControladorJuego.tutorial = _valor;
    }

    public void PauseGame()
    {
        ControladorJuego.jugadorAcceso.LimpiarMovmiento();
        Time.timeScale = 0f; // Pausa el juego
        juegoPausado = true;
    }

    public void ResumeGame()
    {
        Time.timeScale = 1f; // Reanuda el Juego
        juegoPausado = false;
    }

    public void QuitGame()
    {
        Application.Quit(); // Sal del Juego
    }

    #endregion

    #region Disparo

    public void SetearDisparo(bool _value)
    {
        ControladorJuego.puedeDisparar = _value;
    }

    public void SetearCambioColorOpuesto(bool _value)
    {
        ControladorJuego.puedeCambariColorOpuesto = _value;
    }

    public void SetearMovimiemintoEspecial(bool _value)
    {
        ControladorJuego.movimientoEspecial = _value;
    }

    public void SetearMira(bool _valor)
    {
        ControladorJuego.mostrarMira = _valor;
    }

    #endregion






    //SISTEMAS
    #region Inicializaciín Sistema Oleadas
    public SistemaOleadas ObtenersistemaOleada()
    {
        if (ControladorJuego.sistemaOleadas) return ControladorJuego.sistemaOleadas;
        else return null;
    }
    public void SetearSistemaOleada(SistemaOleadas _codigo) { ControladorJuego.sistemaOleadas = _codigo; }
    #endregion

    #region Inicializaciín Sistema Area

    public SistemaArea ObtenerSistemaArea()
    {
        if (ControladorJuego.sistemaArea) return ControladorJuego.sistemaArea;
        else return null;
    }

    public void SetearSistemaArea(SistemaArea _codigo) { ControladorJuego.sistemaArea = _codigo; }

    #endregion

    #region Inicializaciín Sistema Pool
    public SistemaPool ObtenerSistemaPool()
    {
        if (ControladorJuego.sistemaPool) return ControladorJuego.sistemaPool;
        else return null;
    }
    public void SetearSistemaPool(SistemaPool _codigo) { ControladorJuego.sistemaPool = _codigo; }
    #endregion

    #region Inicializaciín Sistema Experiencia
    public SistemaExperiencia ObtenerSistemaExperiencia()
    {
        if (ControladorJuego.sistemaExperiencia) return ControladorJuego.sistemaExperiencia;
        else return null;
    }
    public void SetearSistemaExperiencia(SistemaExperiencia _codigo) { ControladorJuego.sistemaExperiencia = _codigo; }
    #endregion

    #region Inicializaciín Sistema MagiaColor
    public SistemaMagiaColor ObtenerSistemaMagiaColor()
    {
        if (ControladorJuego.sistemaMagiaColor) return ControladorJuego.sistemaMagiaColor;
        else return null;
    }
    public void SetearSistemaMagiaColor(SistemaMagiaColor _codigo) { ControladorJuego.sistemaMagiaColor = _codigo; }
    #endregion

    #region Inicializaciín Sistema Oleadas
    public SistemaMisiones ObtenerSistemaMisiones()
    {
        if (ControladorJuego.sistemaMisiones) return ControladorJuego.sistemaMisiones;
        else return null;
    }
    public void SetearSistemaMisiones(SistemaMisiones _codigo) { ControladorJuego.sistemaMisiones = _codigo; }
    #endregion

    #region Inicializaciín Sistema MecanismoOculto
    public SistemaMecanismosOculto ObtenerSistemaMecanismoOculto()
    {
        if (ControladorJuego.sistemaMecanismosOculto) return ControladorJuego.sistemaMecanismosOculto;
        else return null;
    }
    public void SetearSistemaMecanismoOculto(SistemaMecanismosOculto _codigo) { ControladorJuego.sistemaMecanismosOculto = _codigo; }
    #endregion

}