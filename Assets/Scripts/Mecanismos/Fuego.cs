using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fuego : MonoBehaviour
{
    private float tiempoDeFuego;
    public bool forzarDestruccion;
    public BoxCollider2D colision;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame

    public void Inicializador(bool jugador)
    {
        StartCoroutine(ApagadoFuego());
        tiempoDeFuego = 0;
        if (jugador)
        {
            transform.localScale = Vector3.one;
            switch (ControladorJuego.nivelRojo)
            {
                case 1: tiempoDeFuego = 1; break;// Esti es prueba
                case 2: tiempoDeFuego = 1.5f; break;
                case 3: tiempoDeFuego = 2; transform.localScale = Vector3.one * 1.2f; break;
                case 4: tiempoDeFuego = 3; transform.localScale = Vector3.one * 1.5f; break;
                case 5: tiempoDeFuego = 4; transform.localScale = Vector3.one * 1.7f; break;
                case 6: tiempoDeFuego = 5; transform.localScale = Vector3.one * 2f; break;
            }
        }
        else
        {
            tiempoDeFuego = 7.5f;
            transform.localScale = Vector3.one * 2f;
        }
        StartCoroutine(ApagarFuego(tiempoDeFuego));
    }

    IEnumerator ApagadoFuego()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.2f);
            colision.enabled = !colision.enabled;
        }

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.tag == "Jugador" && ControladorJuego.jugadorAcceso.magiaColor != MagiaColor.verde)
        {
            Debug.Log("Detectando");
            if (!ControladorJuego.jugadorAcceso.inmune)
                ControladorJuego.jugadorAcceso.GolpeJugador(1);
            ControladorJuego.jugadorAcceso.GetHitJugador(1, MagiaColor.rojo);
            if (ControladorJuego.jugadorAcceso.resistencia <= 0)
                ControladorJuego.jugadorAcceso.GameOver();
        }
        else if (other.transform.tag == "Enemigo" && other.GetComponent<Enemigo>().magiaColor != MagiaColor.verde)
        {
            Enemigo _enemigo = other.GetComponent<Enemigo>();
            if (!_enemigo.inmuneFuego)
                _enemigo.GetHitEnemigo(1, MagiaColor.rojo);

            //Si se quiere que el feedback sea solo del color del enemigo cambiar "colorActual" por "_enemigo.magiColor"

        }
    }


    public void ForzarDestruccion()
    {
        gameObject.SetActive(false);
        transform.position = Vector3.one * 10000f;
    }
    public IEnumerator ApagarFuego(float _tiempo)
    {
        yield return new WaitForSeconds(_tiempo);
        if (!forzarDestruccion)
        {
            gameObject.SetActive(false);
            transform.position = Vector3.one * 10000f;
            ControladorJuego.sistemaPool.OcultarFuego(gameObject);
        }
    }
}
