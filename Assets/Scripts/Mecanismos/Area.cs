using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Area : MonoBehaviour
{
    public bool generarHueco = true;
    public bool forzarInicializador = false;
    public bool bloquearArea = false;
    public bool unicaVez = false;
    public GameObject hueco;
    public List<GameObject> huecos;
    public List<Portal> portales;
    public Vector2 ID;

    // Start is called before the first frame update
    void Start()
    {
        if (forzarInicializador)
            Inicializador();
    }

    public void InicializadorEstados(bool _activo, bool _generarHueco,/* bool _forzarInicializador,*/ bool _bloquarArea, bool _unicaVez)
    {
        gameObject.SetActive(_activo);
        generarHueco = _generarHueco;
        forzarInicializador = false;
        bloquearArea = _bloquarArea;
        unicaVez = _unicaVez;
    }

    public void InicializadorManual(Vector2 _ID, int _catnHuecos, Vector3[] _huecoEscala, Vector3[] _huecoPos, Quaternion[] _huecoRot)
    {
        //Tambien indicar si solo se usa una vez
        transform.position = new Vector3(_ID.x, _ID.y, 0f);
        ID = _ID;
        for (int i = 0; i < _catnHuecos; i++)
        {
            GameObject _nuevoHueco = Instantiate(hueco, _huecoPos[i], _huecoRot[i], transform);
            _nuevoHueco.transform.localScale = _huecoEscala[i];
            huecos.Add(_nuevoHueco);
        }
        transform.name = "Area Guardada " + ID;
        transform.SetParent(ControladorJuego.sistemaArea.organizadorArea);
        ControladorJuego.sistemaArea.AgregarArea(this);
        //foreach (Portal _portal in portales) ControladorJuego.sistemaMapa.ActualizarMiniMapa(_portal, transform.position);
    }

    public void InicializadorManual(Vector2 _ID)
    {
        //Tambien indicar si solo se usa una vez
        transform.position = new Vector3(_ID.x, _ID.y, 0f);
        ID = _ID;
        transform.name = "Area Guardada " + ID;
        transform.SetParent(ControladorJuego.sistemaArea.organizadorArea);
        ControladorJuego.sistemaArea.AgregarArea(this);
        //foreach (Portal _portal in portales) ControladorJuego.sistemaMapa.ActualizarMiniMapa(_portal, transform.position);
    }

    public void Inicializador()
    {
        //Genera Hueco
        if (generarHueco)
            for (int i = 0; i < Random.Range(1, 5); i++)
            {
                GameObject _nuevoHueco = Instantiate(hueco, new Vector3(transform.position.x + Random.Range(-transform.localScale.x / 2, transform.localScale.x / 2),
                transform.position.y + Random.Range(-transform.localScale.y / 2, transform.localScale.y / 2), 0f), transform.rotation);
                EncuadreHueco(_nuevoHueco.transform);
                huecos.Add(_nuevoHueco);
            }
        ID = new Vector2(transform.position.x, transform.position.y);
        transform.name = "Area " + ID;
        transform.SetParent(ControladorJuego.sistemaArea.organizadorArea);
        ControladorJuego.sistemaArea.AgregarArea(this);
    }

    public void EncuadreHueco(Transform _hueco)
    {
        _hueco.localScale = new Vector3(Random.Range(2, 11), Random.Range(2, 11), 1f);
        _hueco.SetParent(transform);
        if (_hueco.transform.localPosition.x >= 0)
            _hueco.localPosition = new Vector3(_hueco.localPosition.x - (_hueco.localScale.x / 2), _hueco.localPosition.y, _hueco.localPosition.z);
        else
            _hueco.localPosition = new Vector3(_hueco.localPosition.x + (_hueco.localScale.x / 2), _hueco.localPosition.y, _hueco.localPosition.z);

        if (_hueco.transform.localPosition.y >= 0)
            _hueco.localPosition = new Vector3(_hueco.localPosition.x, _hueco.localPosition.y - (_hueco.localScale.y / 2), _hueco.localPosition.z);
        else
            _hueco.localPosition = new Vector3(_hueco.localPosition.x, _hueco.localPosition.y + (_hueco.localScale.y / 2), _hueco.localPosition.z);
    }

    public void BloquearArea()
    {
        bloquearArea = true;
    }
}
