using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Altar : MecanismoOculto
{
    public bool enRango = false;
    public string mensajeGuardando;
    public string mensajeGuardado;

    // Start is called before the first frame update

    // Update is called once per frame
    void Update()
    {
        {
            if (enRango && !ControladorJuego.adminJuego.juegoPausado && Input.GetKeyDown(KeyCode.E))
            {
                Debug.Log(mensajeGuardando);
                ControladorJuego.sistemaGuardado.GuardarPartida();
                Debug.Log(mensajeGuardado);
            }
        }
    }


    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Jugador") enRango = true;
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Jugador") enRango = false;
    }
}
