using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PortalDir
{
    der,
    izq,
    arr,
    abj,
    centro
}

public class Portal : MonoBehaviour
{
    public Transform destino;
    public Transform padre;
    public PortalDir direccion;
    public bool bloqueado = false;
    public bool generacionManual = false;
    public bool pintado = false;
    public MagiaColor magiaColor;
    private SpriteRenderer miSprite;
    private void Awake()
    {
        miSprite = GetComponent<SpriteRenderer>();
    }

    // Start is called before the first frame update
    void Start()
    {
        if (generacionManual) return;
        if (bloqueado) Desactivar();
        DesignarColorPortal();
    }

    #region Estados del Portal
    public void BloquearPortal() { bloqueado = true; Desactivar(); }
    public void DesbloquearPortal() { bloqueado = false; ActivarPortal(); }
    public void Desactivar() { gameObject.SetActive(false); }
    public void ActivarPortal() { if (!bloqueado) gameObject.SetActive(true); }
    #endregion

    public void AsignarValoresManualemnte(bool _bloqueado, int _color, bool _coneccion)
    {
        magiaColor = (MagiaColor)_color;
        miSprite.color = ControladorJuego.sistemaMagiaColor.ObtenerColor(magiaColor);

        if (_coneccion)
        {
            //VerificarConecciones(padre.GetComponent<Area>());
            switch (direccion)
            {
                case PortalDir.der:
                    ControladorJuego.sistemaArea.VerificarPosicionArea(new Vector2(padre.position.x + 50, padre.position.y), this); break;
                case PortalDir.izq:
                    ControladorJuego.sistemaArea.VerificarPosicionArea(new Vector2(padre.position.x - 50, padre.position.y), this); break;
                case PortalDir.arr:
                    ControladorJuego.sistemaArea.VerificarPosicionArea(new Vector2(padre.position.x, padre.position.y + 50), this); break;
                case PortalDir.abj:
                    ControladorJuego.sistemaArea.VerificarPosicionArea(new Vector2(padre.position.x, padre.position.y - 50), this); break;
            }
            pintado = true;
        }

        if (_bloqueado) BloquearPortal();
        else DesbloquearPortal();
    }

    public void DesignarColorPortal()
    {
        if (destino) return; //Evita que se vuelva a colorear un portal ya pintado
        #region Condicion para posibilidad de Colores
        if (ControladorJuego.sistemaArea.ObtenerAreasGeneradas() <= 2) magiaColor = MagiaColor.blanco;
        else magiaColor = (MagiaColor)Random.Range(0, 8);
        #endregion
        miSprite.color = ControladorJuego.sistemaMagiaColor.ObtenerColor(magiaColor);
        pintado = true;
    }
    public void VerificarColor()
    {
        //Vericia si se 
        Portal _portalDestino = destino.GetComponent<Portal>();
        if (_portalDestino.pintado)
        {
            magiaColor = _portalDestino.magiaColor;
            miSprite.color = ControladorJuego.sistemaMagiaColor.ObtenerColor(magiaColor);
            pintado = true;
        }
        else
        {
            _portalDestino.magiaColor = magiaColor;
            _portalDestino.miSprite.color = ControladorJuego.sistemaMagiaColor.ObtenerColor(magiaColor);
            _portalDestino.pintado = true;
        }
    }

    public bool UsarPortal()
    {

        if (ControladorJuego.sistemaMagiaColor.ObtenerMagiaColorJugador() != magiaColor)
        {
            ControladorJuego.sistemaMensajes.MostarTexto("ERRORES", "PORTAL", "AYUDA", true);
            return false;
        }

        if (!destino)
        {
            //Segun el portal usado se desgina la generación del nuevo Área
            Vector3 _dirCreacion;

            switch (direccion)
            {
                case PortalDir.der: _dirCreacion = new Vector3(padre.position.x + 50, padre.position.y, padre.position.z); break;
                case PortalDir.izq: _dirCreacion = new Vector3(padre.position.x - 50, padre.position.y, padre.position.z); break;
                case PortalDir.arr: _dirCreacion = new Vector3(padre.position.x, padre.position.y + 50, padre.position.z); break;
                case PortalDir.abj: _dirCreacion = new Vector3(padre.position.x, padre.position.y - 50, padre.position.z); break;
                default: _dirCreacion = Vector3.zero; break;
            }


            // Se crea el nuevo Área
            GameObject _areaCreada = Instantiate(ControladorJuego.sistemaArea.area, _dirCreacion, padre.rotation);
            Area _componente = _areaCreada.GetComponent<Area>();
            _componente.Inicializador();

            // Verificar que portales se apagarán
            foreach (Portal _portal in _componente.portales)
            {
                int _random = Random.Range(0, 3);
                if (_random == 0)
                {
                    switch (direccion)
                    {
                        case PortalDir.der: if (_portal.direccion != PortalDir.izq) _portal.BloquearPortal(); break;
                        case PortalDir.izq: if (_portal.direccion != PortalDir.der) _portal.BloquearPortal(); break;
                        case PortalDir.arr: if (_portal.direccion != PortalDir.abj) _portal.BloquearPortal(); break;
                        case PortalDir.abj: if (_portal.direccion != PortalDir.arr) _portal.BloquearPortal(); break;
                    }
                }
            }

            //Verificación de areas ya existentes y establecer conecciones con estas.
            switch (direccion)
            {
                case PortalDir.der:
                    foreach (Portal _portal in _componente.portales)
                    {
                        if (_portal.direccion == PortalDir.izq)
                        {
                            _portal.destino = transform;
                            destino = _portal.transform;
                            VerificarColor();
                        }
                        else
                        {
                            switch (_portal.direccion)
                            {
                                case PortalDir.der:
                                    ControladorJuego.sistemaArea.VerificarPosicionArea(new Vector2(padre.position.x + 100,
                                     padre.position.y), _portal);
                                    break;
                                case PortalDir.arr:
                                    ControladorJuego.sistemaArea.VerificarPosicionArea(new Vector2(padre.position.x + 50,
                                     padre.position.y + 50), _portal);
                                    break;
                                case PortalDir.abj:
                                    ControladorJuego.sistemaArea.VerificarPosicionArea(new Vector2(padre.position.x + 50,
                                     padre.position.y - 50), _portal);
                                    break;
                            }
                        }
                    }
                    break;
                case PortalDir.izq:
                    foreach (Portal _portal in _componente.portales)
                    {
                        if (_portal.direccion == PortalDir.der)
                        {
                            _portal.destino = transform;
                            destino = _portal.transform;
                            VerificarColor();
                        }
                        else
                        {
                            switch (_portal.direccion)
                            {
                                case PortalDir.izq:
                                    ControladorJuego.sistemaArea.VerificarPosicionArea(new Vector2(padre.position.x - 100,
                                     padre.position.y), _portal);
                                    break;
                                case PortalDir.arr:
                                    ControladorJuego.sistemaArea.VerificarPosicionArea(new Vector2(padre.position.x - 50,
                                     padre.position.y + 50), _portal);
                                    break;
                                case PortalDir.abj:
                                    ControladorJuego.sistemaArea.VerificarPosicionArea(new Vector2(padre.position.x - 50,
                                     padre.position.y - 50), _portal);
                                    break;
                            }
                        }
                    }
                    break;
                case PortalDir.arr:
                    foreach (Portal _portal in _componente.portales)
                    {
                        if (_portal.direccion == PortalDir.abj)
                        {
                            _portal.destino = transform;
                            destino = _portal.transform;
                            VerificarColor();
                        }
                        else
                        {
                            switch (_portal.direccion)
                            {
                                case PortalDir.der:
                                    ControladorJuego.sistemaArea.VerificarPosicionArea(new Vector2(padre.position.x + 50,
                                     padre.position.y + 50), _portal);
                                    break;
                                case PortalDir.izq:
                                    ControladorJuego.sistemaArea.VerificarPosicionArea(new Vector2(padre.position.x - 50,
                                     padre.position.y + 50), _portal);
                                    break;
                                case PortalDir.arr:
                                    ControladorJuego.sistemaArea.VerificarPosicionArea(new Vector2(padre.position.x,
                                      padre.position.y + 100), _portal);
                                    break;
                            }
                        }
                    }
                    break;
                case PortalDir.abj:
                    foreach (Portal _portal in _componente.portales)
                    {
                        if (_portal.direccion == PortalDir.arr)
                        {
                            _portal.destino = transform;
                            destino = _portal.transform;
                            VerificarColor();
                        }
                        else
                        {
                            switch (_portal.direccion)
                            {
                                case PortalDir.der:
                                    ControladorJuego.sistemaArea.VerificarPosicionArea(new Vector2(padre.position.x + 50,
                                     padre.position.y - 50), _portal);
                                    break;
                                case PortalDir.izq:
                                    ControladorJuego.sistemaArea.VerificarPosicionArea(new Vector2(padre.position.x - 50,
                                     padre.position.y - 50), _portal);
                                    break;
                                case PortalDir.abj:
                                    ControladorJuego.sistemaArea.VerificarPosicionArea(new Vector2(padre.position.x,
                                     padre.position.y - 100), _portal);
                                    break;
                            }
                        }
                    }
                    break;
            }

            //Verificar conecciones exitosas
            if (destino)
            {
                foreach (Portal _portal in _componente.portales)
                {
                    if (_portal.destino)
                    {
                        if (_portal.bloqueado)
                        {
                            if (!_portal.destino.GetComponent<Portal>().bloqueado)
                            {
                                //Obliga contectar 2 portales donde deben estar conectados
                                _portal.DesbloquearPortal();
                                VerificarColor();
                            }
                        }
                        else
                        {
                            if (_portal.destino.GetComponent<Portal>().bloqueado)
                            {
                                //Obliga a que no se genere un portal donde no puede existir una conección 
                                _portal.BloquearPortal();
                                VerificarColor();
                            }
                        }
                    }
                }
            }
            ControladorJuego.sistemaMapa.ActualizarMapa();

        }
        //StopAllCoroutines();
        //Dar chance de que portal detecte hueco para eliminarlo en caso de colision
        ControladorJuego.sistemaMapa.ActualizarMiniMapa(magiaColor, destino.parent.transform.position);
        StartCoroutine(Teletransporte());
        return true;
    }

    public PortalDir ObtenerPortalOpuesto()
    {
        switch (direccion)
        {
            case PortalDir.der: return PortalDir.izq;
            case PortalDir.izq: return PortalDir.der;
            case PortalDir.arr: return PortalDir.abj;
            case PortalDir.abj: return PortalDir.arr;
            default: Debug.Log("Error portal no encotnrado"); return PortalDir.der;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {

        if (other.transform.tag == "Jugador")
            ControladorJuego.jugadorAcceso.portalActual = this;
        else if (other.transform.tag == "Hueco")
        {
            other.gameObject.GetComponentInParent<Area>().huecos.Remove(other.gameObject); //Añadir huecosguardados a est alista
            Destroy(other.gameObject);
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.transform.tag == "Jugador") ControladorJuego.jugadorAcceso.portalActual = null;
    }

    public IEnumerator Teletransporte()
    {
        yield return new WaitForSeconds(0.1f);
        ControladorJuego.jugadorAcceso.transform.position = destino.position;
        ControladorJuego.sistemaPool.ForzarOcultarElementos();
        padre.gameObject.SetActive(false);
    }

}
