using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DesbloqueadorMagia : MonoBehaviour
{
    public MagiaColor color;
    private bool segundaActivacion;
    private bool unicaVez = false;

    // Start is called before the first frame update
    void Start()
    {
        segundaActivacion = true;
    }

    void Update()
    {
        if (gameObject.activeSelf && segundaActivacion && !unicaVez)
        {
            int _indice = (int)color;
            switch (_indice)
            {
                case 1: ControladorJuego.sistemaMagiaColor.DesbloquearMagiaRoja(); break;
                case 2: ControladorJuego.sistemaMagiaColor.DesbloquearMagiaAzul(); break;
                case 3: ControladorJuego.sistemaMagiaColor.DesbloquearMagiaAmarilla(); break;
                case 4: ControladorJuego.sistemaMagiaColor.DesbloquearMagiaMorada(); break;
                case 5: ControladorJuego.sistemaMagiaColor.DesbloquearMagiaNaranja(); break;
                case 6: ControladorJuego.sistemaMagiaColor.DesbloquearMagiaVerde(); break;
                case 7: ControladorJuego.sistemaMagiaColor.DesbloquearMagiaNegra(); break;
                default: Debug.LogError("El indice no coicide"); break;
            }
            StartCoroutine(Desactivar());
            unicaVez = true;
        }
    }

    IEnumerator Desactivar()
    {
        yield return new WaitForSeconds(1f);
        gameObject.SetActive(false);
    }
}
