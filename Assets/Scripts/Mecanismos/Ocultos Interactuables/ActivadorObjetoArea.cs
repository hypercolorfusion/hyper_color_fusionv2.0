using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivadorObjetoArea : MonoBehaviour
{

    public GameObject objeto;
    public string nombreMecanismo_aActivar;


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Jugador")
        {
            if (!objeto)
            {
                foreach (GameObject _objeto in ControladorJuego.sistemaMecanismosOculto.mecanismosGenerados)
                    if (_objeto.name == nombreMecanismo_aActivar) objeto = _objeto;
            }
            objeto.SetActive(true);
            StartCoroutine(Desactivar());
        }
    }

    IEnumerator Desactivar()
    {
        yield return new WaitForSeconds(1f);
        gameObject.SetActive(false);
    }

}
