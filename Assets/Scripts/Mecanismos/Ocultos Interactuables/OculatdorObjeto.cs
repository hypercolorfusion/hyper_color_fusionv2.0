using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OculatdorObjeto : MonoBehaviour
{
    public string nombreMecanismo_aOcultar;
    public GameObject objeto;
    public bool segundaActivacion = false;
    public bool unicaVez = false;


    // Start is called before the first frame update
    void Start()
    {
        if (!objeto)
        {
            foreach (GameObject _objeto in ControladorJuego.sistemaMecanismosOculto.mecanismosGenerados)
                if (_objeto.name == nombreMecanismo_aOcultar) objeto = _objeto;
        }
        segundaActivacion = true;
    }

    void Update()
    {
        if (gameObject.activeSelf && segundaActivacion && !unicaVez)
        {
            StartCoroutine(Desactivar());
            objeto.SetActive(false);
            unicaVez = true;
        }
    }

    IEnumerator Desactivar()
    {
        yield return new WaitForSeconds(1f);
        gameObject.SetActive(false);
    }
}
