using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DesbloqueadorPortal : MonoBehaviour
{
    public Vector2 area_ID;
    public PortalDir direccion;
    public Portal portal;
    private bool segundaActivacion;
    private bool unicaVez = false;

    // Start is called before the first frame update
    void Start()
    {
        segundaActivacion = true;
        if (!portal) portal = ControladorJuego.sistemaArea.ObtenerPortalSegunAreaId(area_ID, direccion);

    }

    void Update()
    {
        if (gameObject.activeSelf && segundaActivacion && !unicaVez)
        {
            portal.DesbloquearPortal();
            ControladorJuego.sistemaMapa.ActualizarMapa();
            ControladorJuego.sistemaMapa.ForzarGenerarConeccion();
            ControladorJuego.sistemaMapa.ActualizarMiniMapa(ControladorJuego.jugadorAcceso.magiaColor, ControladorJuego.sistemaArea.ObtenerID_AreaJugadorActual());
            Debug.Log("Portal desbloqueado");
            StartCoroutine(Desactivar());
            unicaVez = true;
        }
    }

    IEnumerator Desactivar()
    {
        yield return new WaitForSeconds(1f);
        gameObject.SetActive(false);
    }
}
