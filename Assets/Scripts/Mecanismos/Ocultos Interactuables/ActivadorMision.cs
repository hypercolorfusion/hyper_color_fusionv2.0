using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivadorMision : MonoBehaviour
{
    public string mision;
    public bool tutorial = true;
    private bool segundaActivacion;
    private bool unicaVez = false;
    public bool apagarObjeto = false;

    // Start is called before the first frame update
    void Start()
    {
        if (apagarObjeto) gameObject.SetActive(false);
        segundaActivacion = true;
    }

    void Update()
    {
        if (gameObject.activeSelf && segundaActivacion && !unicaVez)
        {
            if (tutorial)
                ControladorJuego.sistemaMisiones.IniciarMisionTutorial(mision);
            else
                ControladorJuego.sistemaMisiones.IniciarMision(mision);

            Debug.Log("Se ha empezado la misión :" + mision);
            StartCoroutine(Desactivar());
            unicaVez = true;
        }
    }

    IEnumerator Desactivar()
    {
        yield return new WaitForSeconds(1f);
        gameObject.SetActive(false);
    }

}
