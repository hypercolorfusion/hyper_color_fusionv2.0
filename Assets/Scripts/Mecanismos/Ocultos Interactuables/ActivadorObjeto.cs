using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivadorObjeto : MonoBehaviour
{
    public string codigo;
    public string subCodigo;
    public int indiceMecanismo;
    public string codigoMecanismo;
    public int numFrase;

    // Start is called before the first frame update
    void Start()
    {
        Asignar();
        gameObject.SetActive(false);
        if (codigoMecanismo != "") //FUERZA APARICION
        {
            switch (indiceMecanismo)
            {

                case 0:
                    for (int i = 0; i < ControladorJuego.sistemaMecanismosOculto.mecanismosTutorial.Length; i++)
                    {
                        if (ControladorJuego.sistemaMecanismosOculto.mecanismosTutorial[i].nombre ==
                         codigoMecanismo && ControladorJuego.sistemaMecanismosOculto.mecanismosTutorial[i].forzarAparicion) gameObject.SetActive(true);
                        else ControladorJuego.sistemaMecanismosOculto.mecanismosTutorial[i].forzarAparicion = true;
                    }

                    break;
                case 1: break;

                case 2:
                    for (int i = 0; i < ControladorJuego.sistemaMecanismosOculto.altares.Length; i++)
                    {
                        if (ControladorJuego.sistemaMecanismosOculto.altares[i].nombre ==
                         codigoMecanismo && ControladorJuego.sistemaMecanismosOculto.altares[i].forzarAparicion) gameObject.SetActive(true);
                        else ControladorJuego.sistemaMecanismosOculto.altares[i].forzarAparicion = true;
                    }
                    break;
            }
        }
    }

    public void Asignar()
    {
        ControladorJuego.sistemaMensajes.AsignarActivador(codigo, subCodigo, numFrase, gameObject);
    }
}
