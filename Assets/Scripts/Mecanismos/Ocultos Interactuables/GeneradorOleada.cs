using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneradorOleada : MonoBehaviour
{
    public MagiaColor color;
    private bool segundaActivacion;
    private bool unicaVez = false;
    // Start is called before the first frame update
    void Start()
    {
        segundaActivacion = true;
    }

    void Update()
    {
        if (gameObject.activeSelf && segundaActivacion && !unicaVez)
        {
            ControladorJuego.sistemaOleadas.ActivarOleada(color);
            ControladorJuego.sistemaMisiones.IniciarMisionTutorial("PRIMERA_OLEADA");
            Debug.Log("Se inició la oleada");
            StartCoroutine(Desactivar());
            unicaVez = true;
        }
    }
    IEnumerator Desactivar()
    {
        yield return new WaitForSeconds(1f);
        gameObject.SetActive(false);
    }


}
