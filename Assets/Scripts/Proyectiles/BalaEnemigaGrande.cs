﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalaEnemigaGrande : Proyectil
{
    [Header("Bala Enemiga Grande")]
    // Start is called before the first frame update
    public float duracion = 5f;
    public int cantBalaRad;
    void Start()
    {
        Inicializador();
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.activeSelf)
            MovimientRecto2D();
    }

    public void Inicializador()
    {
        estrellar = false;
        forzarDestruccion = false;
        StartCoroutine(Explosion());
    }

    public void OcultarBala()
    {
        if (!forzarDestruccion)
        {
            gameObject.SetActive(false);
            transform.position = Vector3.one * 10000f;
            ControladorJuego.sistemaPool.OcultarBalaGrandeEnemigo(gameObject);
        }
    }

    public void ForzarDestruccion()
    {
        gameObject.SetActive(false);
        transform.position = Vector3.one * 10000f;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Jugador")
        {
            ColisionJugador(colorActual, damage);
            OcultarBala();
        }
        else if (other.tag == "Pared")
        {
            if (!estrellar)
            {
                Estrellar();
                float _espacios = 360 / (float)cantBalaRad;
                for (int i = 0; i < cantBalaRad; i++)
                {
                    ControladorJuego.sistemaPool.DispararBalaEnemiga(transform.position, transform.rotation, colorActual, damage / 3, 1f);
                    transform.Rotate(0f, 0f, _espacios, Space.Self);
                }
                OcultarBala();
            }
        }

    }

    IEnumerator Explosion()
    {
        yield return new WaitForSeconds(duracion);
        transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
        float _espacios = 360 / (float)cantBalaRad;
        for (int i = 0; i < cantBalaRad; i++)
        {

            ControladorJuego.sistemaPool.DispararBalaEnemiga(transform.position, transform.rotation, colorActual, damage / 3, 1f);
            transform.Rotate(0f, 0f, _espacios, Space.Self);
        }
        if (!estrellar)
            OcultarBala();
    }
}