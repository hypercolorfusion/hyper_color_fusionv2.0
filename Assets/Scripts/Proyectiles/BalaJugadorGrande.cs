﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalaJugadorGrande : Proyectil
{
    [Header("Bala Jugador Grande")]

    // Start is called before the first frame update
    public int cantBalaRad;
    public bool enCarga = false;
    public float cargando;
    public float duracion = 5f;
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.activeSelf)
            MovimientRecto2D();
        if (colorActual == MagiaColor.negro && enCarga)
        {
            cargando += Time.deltaTime / 2;
            transform.localScale = new Vector3(cargando, 8 * cargando / 3, 1f);
            SetearBala(Mathf.RoundToInt(ControladorJuego.jugadorAcceso.ataque * cargando * 2));

            if (Input.GetMouseButtonUp(0) || cargando >= 0.75 + (ControladorJuego.nivelNegro * 0.25f))
            {
                transform.SetParent(ControladorJuego.sistemaPool.transform);
                SetearBala(3f - cargando);
                foreach (GameObject _enemigos in ControladorJuego.sistemaOleadas.enemigosObjetivo)
                {
                    Enemigo _enemigoAcceso = _enemigos.GetComponent<Enemigo>();
                    _enemigoAcceso.absorviendo = false;
                    if (ControladorJuego.nivelNegro >= 6 && cargando >= 1.5f)
                        _enemigoAcceso.Paralizar();
                }
                enCarga = false;
                transform.SetParent(ControladorJuego.sistemaPool.transform);
                if (ControladorJuego.nivelNegro >= 3)
                    cantBalaRad = ControladorJuego.nivelNegro;
                else
                    cantBalaRad = 0;
                if (cargando >= 1.5f) StartCoroutine(Explosion());
            }
        }
    }

    public void Inicializador()
    {
        transform.SetParent(ControladorJuego.sistemaPool.transform);
        estrellar = forzarDestruccion = enCarga = false;
        cargando = 0.5f;
        transform.localScale = new Vector3(6f, 6f, 1f);
        transform.SetParent(ControladorJuego.sistemaPool.transform);
        if (colorActual == MagiaColor.rojo) StartCoroutine(Explosion());
    }

    public void OcultarBala()
    {
        if (!forzarDestruccion)
        {
            if (colorActual == MagiaColor.rojo)
                ControladorJuego.sistemaPool.MostrarFuego(transform.position, true);
            gameObject.SetActive(false);
            transform.position = Vector3.one * 10000f;
            transform.SetParent(ControladorJuego.sistemaPool.transform);
            ControladorJuego.sistemaPool.OcultarBalaJugadorGrande(gameObject);
        }
    }

    public void SetearBalaJugador(MagiaColor _color)
    {
        int _dañoActual = 0; //A cacular
        SetearBala(_color);
        if (_color == MagiaColor.rojo)
        {
            SetearBala(0.5f);
            if (ControladorJuego.nivelRojo >= 3)
                cantBalaRad = ControladorJuego.nivelRojo;
            else
                cantBalaRad = 0;
            _dañoActual = Mathf.RoundToInt(ControladorJuego.jugadorAcceso.ataque * 4.5f);
            SetearBala(0.75f);

        }
        else if (_color == MagiaColor.negro)
        {
            SetearBala(0f);

            enCarga = true;
            transform.localScale = new Vector3(0.5f, 0.5f, 1f);
            transform.SetParent(ControladorJuego.jugadorAcceso.mira.transform);
        }

        SetearBala(_dañoActual);
        //Debug.Log("Soy de color :" + colorActual);
    }

    public void ForzarDestruccion()
    {
        gameObject.SetActive(false);
        transform.position = Vector3.one * 10000f;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Enemigo")
        {
            ColisionEnemigo(other.GetComponent<Enemigo>(), colorActual, damage);
            OcultarBala();
        }
        else if (other.tag == "Pared")
        {
            if (!estrellar)
            {
                Estrellar();
                float _espacios = 360 / (float)cantBalaRad;
                if (colorActual == MagiaColor.rojo || cargando >= 1.5f)
                {
                    for (int i = 0; i < cantBalaRad; i++)
                    {
                        ControladorJuego.sistemaPool.BalaDisparaJugadorBala(transform.position, transform.rotation, colorActual);
                        transform.Rotate(0f, 0f, _espacios, Space.Self);
                    }
                }
                OcultarBala();
            }
        }
    }

    IEnumerator Explosion()
    {
        yield return new WaitForSeconds(duracion);
        Debug.Log("cataplum");
        if (cantBalaRad != 0)
        {
            transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
            float _espacios = 360 / (float)cantBalaRad;
            for (int i = 0; i < cantBalaRad; i++)
            {
                ControladorJuego.sistemaPool.BalaDisparaJugadorBala(transform.position, transform.rotation, colorActual);
                transform.Rotate(0f, 0f, _espacios, Space.Self);
            }
        }
        if (!estrellar)
            OcultarBala();
    }
}