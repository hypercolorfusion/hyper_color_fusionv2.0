﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Proyectil : Origen
{
    [Header("Basico")]
    public int damage;
    public float velocidad;
    public float velocidadBonus = 1f;
    public float velocidadRotacion = 0.01f;
    [HideInInspector]
    public bool estrellar;
    [HideInInspector]
    public int numRebotes;
    [HideInInspector]
    public int numRebotadas;

    [Header("Enlazables")]
    public MagiaColor colorActual;
    [HideInInspector]
    public bool forzarDestruccion;

    public void MovimientRecto2D()
    {
        transform.Translate(Vector3.right * Time.deltaTime * velocidad * velocidadBonus);
    }

    public void ApuntarJugador()
    {
        //CODIGO ROBADO PAPUS XD
        Vector3 myLocation = transform.position;
        Vector3 targetLocation = ControladorJuego.jugadorAcceso.transform.position;
        myLocation.z = targetLocation.z; // ensure there is no 3D rotation by aligning Z position

        // vector from this object towards the target location
        Vector3 vectorToTarget = (targetLocation - myLocation).normalized;
        // rotate that vector by 90 degrees around the Z axis
        Vector3 rotatedVectorToTarget = Quaternion.Euler(0, 0, 90) * vectorToTarget;

        // get the rotation that points the Z axis forward, and the Y axis 90 degrees away from the target
        // (resulting in the X axis facing the target)
        Quaternion targetRotation = Quaternion.LookRotation(forward: Vector3.forward, upwards: rotatedVectorToTarget);

        // changed this from a lerp to a RotateTowards because you were supplying a "speed" not an interpolation value
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, velocidadRotacion * Time.deltaTime);
    }

    public void GiroPlanta()
    {
        transform.Rotate(Vector3.forward * (Random.Range(-10, 10f)));
    }
    public void SetearBala(MagiaColor _color)
    {
        colorActual = _color;
        GetComponent<SpriteRenderer>().color = ControladorJuego.sistemaMagiaColor.ObtenerColor(colorActual);
    }
    public void SetearBala(int _damageValue)
    {
        damage = _damageValue;
    }
    public void SetearBala(float _velocidadBonus)
    {
        velocidadBonus = _velocidadBonus;
    }
    public void SetearBala(MagiaColor _color, int _damageValue, float _velocidadBonus)
    {
        damage = _damageValue;
        colorActual = _color;
        velocidadBonus = _velocidadBonus;
        GetComponent<SpriteRenderer>().color = ControladorJuego.sistemaMagiaColor.ObtenerColor(colorActual);
    }
    public void SetearBala(int _damageValue, float _velocidadBonus)
    {
        damage = _damageValue;
        velocidadBonus = _velocidadBonus;
    }


    public void ColisionEnemigo(Enemigo _enemigo, MagiaColor _color, int _valor)
    {
        if (_color == MagiaColor.azul && _enemigo.magiaColor != MagiaColor.azul)
        {
            if (ControladorJuego.nivelAzul >= 3) _enemigo.Relantizar();
            if (ControladorJuego.nivelAzul >= 6) _enemigo.Paralizar();
        }

        if (ControladorJuego.nivelAmarillo >= 2 && _color == MagiaColor.amarillo &&
         _enemigo.magiaColor != MagiaColor.amarillo) _enemigo.Relantizar();

        if (!(_color == MagiaColor.amarillo && ControladorJuego.nivelAmarillo >= 3 && numRebotadas < numRebotes)) estrellar = true;

        int _valorAtaque = ControladorJuego.sistemaMagiaColor
            .CalculoAtaqueJugador(_valor, _color, _enemigo.magiaColor);

        _enemigo.GetHitEnemigo(_valorAtaque, colorActual);
    }

    public void ColisionJugador(MagiaColor _color, int _valor)
    {
        estrellar = true;
        int _valorAtaque = ControladorJuego.sistemaMagiaColor
            .CalculoAtaqueEnemigo(_valor, _color, false);

        Jugador _acceso = ControladorJuego.jugadorAcceso;
        if (!_acceso.inmune)
        {
            _acceso.GolpeJugador(_valorAtaque);
            _acceso.GetHitJugador(_valorAtaque, colorActual);

            if (_acceso.resistencia <= 0)
                _acceso.GameOver();
        }
    }

    public void Estrellar()
    {
        estrellar = true;
        //Intanciar humito o algo para indicar que chocó
    }


    public void Rebotar(Vector2 _posPared, int _anguloRedondeado)
    {
        int _anguloRotar = 0;
        int _anguloObjetivo = 0;
        int _anguloRedondeadoTemp = _anguloRedondeado;

        if (_posPared.x != 0)
        {
            //Chocó con la pared de la derecha
            if (_posPared.x > 0)
            {
                if (_anguloRedondeado == 360 || _anguloRedondeado == 0)
                    _anguloRotar = 180;
                else
                {
                    _anguloObjetivo = 180 - _anguloRedondeado;
                    _anguloRotar = _anguloObjetivo - _anguloRedondeado;
                }
            }
            //Chocó con la pared de la izquierda
            else
            {
                if (_anguloRedondeado == 180)
                    _anguloRotar = 180;
                else
                {

                    _anguloRedondeadoTemp = 90 - _anguloRedondeado;
                    _anguloObjetivo = 90 + _anguloRedondeadoTemp;
                    _anguloRotar = _anguloObjetivo - _anguloRedondeado;
                }
            }
        }
        else if (_posPared.y != 0)
        {
            //Chocó con la pared de arriba
            if (_posPared.y > 0)
            {
                if (_anguloRedondeado == 90)
                    _anguloRotar = 180;
                else
                {
                    _anguloRedondeadoTemp = 180 - _anguloRedondeado;
                    _anguloObjetivo = 180 + _anguloRedondeadoTemp;
                    _anguloRotar = _anguloObjetivo - _anguloRedondeado;
                }
            }
            //Chocó con la pared de la abajo
            else
            {
                if (_anguloRedondeado == 270)
                    _anguloRotar = 180;
                else
                {
                    _anguloRedondeadoTemp = 180 - _anguloRedondeado;
                    _anguloObjetivo = 180 + _anguloRedondeadoTemp;
                    _anguloRotar = _anguloObjetivo - _anguloRedondeado;
                }
            }
        }
        transform.Rotate(Vector3.forward * _anguloRotar);
        numRebotadas += 1;
    }
    public void RebotarEnemigo()
    {
        transform.Rotate(Vector3.forward * 180);
        transform.Rotate(Vector3.forward * (Random.Range(-15, 16)));
        numRebotadas += 1;
    }
}