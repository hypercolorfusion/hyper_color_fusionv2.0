﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BalaEnemiga : Proyectil
{
    [Header("Bala Enemiga")]
    public TipoBalaEnemiga tipoBala;

    #region teledirigdo
    private bool recto = false;
    private bool rectoInicial = false;
    #endregion

    #region  Vecotrial
    public bool inteligente;
    public float tiempoEspera;
    public int giros;
    private int giradas;
    private float velocidadTemp;
    private float duracionBala;
    #endregion


    // Update is called once per frame
    void Update()
    {
        if (tipoBala == TipoBalaEnemiga.teledirigido)
        {
            if (rectoInicial)
            {
                MovimientRecto2D();
                return;
            }

            if (!recto)
            {
                ApuntarJugador();
                if (ControladorJuego.adminJuego.DistanciaJugador(transform.position) <= 3) recto = true;
            }
        }
        MovimientRecto2D();
    }

    public void InicializadorBalaNormal()
    {
        tipoBala = TipoBalaEnemiga.normal;
        velocidad = 5f;
        estrellar = forzarDestruccion = false;
    }

    public void InicializadorBalaTeledirigido(float _valorRotacion)
    {
        recto = false;
        tipoBala = TipoBalaEnemiga.teledirigido;
        velocidad = 5f;
        velocidadRotacion = _valorRotacion;
        estrellar = forzarDestruccion = forzarDestruccion = false;
        StartCoroutine(TimepoRectoTeledirigido());
    }

    public void InicializadorBalaRebote(int _nivelJugador)
    {
        tipoBala = TipoBalaEnemiga.rebote;
        velocidad = 5f;
        numRebotadas = 0;
        numRebotes = _nivelJugador + 1;
        estrellar = forzarDestruccion = false;
    }

    public void InicializadorBalaVectorial(float _tiempoEspera, int _giros, float _duracionBala)
    {
        tipoBala = TipoBalaEnemiga.vectorial;
        velocidad = velocidadTemp = 5f;
        estrellar = forzarDestruccion = false;
        giradas = 0;
        giros = _giros;
        tiempoEspera = _tiempoEspera;
        duracionBala = _duracionBala;
        StartCoroutine(GirarBalaVectorial());
    }

    public void SetearBalaVectorial(MagiaColor _color, int _damageValue, bool _inteligente, float _velocidadBonus)
    {
        inteligente = _inteligente;
        SetearBala(_color, _damageValue, _velocidadBonus);
    }

    public void OcultarBala()
    {
        if (!forzarDestruccion)
        {
            gameObject.SetActive(false);
            transform.position = Vector3.one * 10000f;
            ControladorJuego.sistemaPool.OcultarBalaEnemigo(gameObject);
        }
    }

    public void ForzarDestruccion()
    {
        forzarDestruccion = true;
        gameObject.SetActive(false);
        transform.position = Vector3.one * 10000f;
    }

    void OnTriggerEnter2D(Collider2D other)
    {

        if (other.tag == "Jugador")
        {
            ColisionJugador(colorActual, damage); OcultarBala();
        }
        else if (other.tag == "Pared")
        {
            if (tipoBala == TipoBalaEnemiga.rebote)
            {
                if (numRebotadas < numRebotes)
                {
                    Rebotar(other.transform.localPosition, Mathf.RoundToInt(transform.eulerAngles.z));
                }
                else
                {
                    Estrellar(); OcultarBala();
                }
            }
            else
            {
                Estrellar(); OcultarBala();
            }
        }
        else if ((colorActual == MagiaColor.morado || colorActual == MagiaColor.blanco) && other.transform.tag == "BalaJugador" && other.GetComponent<BalaJugador>() && other.GetComponent<BalaJugador>().colorActual == MagiaColor.morado)
        {
            Estrellar(); OcultarBala();
        }
    }


    private IEnumerator TimepoRectoTeledirigido()
    {
        rectoInicial = true;
        yield return new WaitForSeconds(0.5f);
        rectoInicial = false;
    }

    public IEnumerator GirarBalaVectorial()
    {

        float _temporizador = duracionBala / giros;
        Transform _mitransform = transform;

        for (giradas = 0; giradas <= giros * (inteligente ? 2 : 1); giradas++)
        {
            yield return new WaitForSeconds(duracionBala);
            velocidad = 0;
            yield return new WaitForSeconds(tiempoEspera);
            if (inteligente)
            {
                LookAt2D(ControladorJuego.jugadorAcceso.transform, ref _mitransform);
            }
            else
            {
                LookAt2D(new Vector3(transform.position.x + Random.Range(-50, 51),
                    transform.position.y + Random.Range(-50, 51),
                    0), ref _mitransform);
            }
            velocidad = velocidadTemp;
        }
        ForzarDestruccion();
    }
}

public enum TipoBalaEnemiga
{
    normal,
    teledirigido,
    rebote,
    vectorial
}
