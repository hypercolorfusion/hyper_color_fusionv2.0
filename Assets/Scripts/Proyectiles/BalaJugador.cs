﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalaJugador : Proyectil
{
    [Header("Bala Jugador")]
    public bool rotar = false;
    public bool inteligente = false;
    public bool onda = false;
    private float rangoOnda;
    public Transform objetivo;
    private float valorAnillo;
    private Transform miTransform;
    private bool derecha;
    private bool orbitar;

    // Start is called before the first frame update

    void Start()
    {
        miTransform = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!gameObject.activeSelf) return;

        if (inteligente) MovimientoInteligente();
        if (onda) GiroPlanta();

        if (rotar) OrbitarJugador();
        else MovimientRecto2D();


    }

    public void SetearBalaJugador(MagiaColor _color)
    {
        int _dañoActual = ControladorJuego.jugadorAcceso.ataque;//daño default
        float _velocidadBonus = 1f;//velocidadBonus Default 1

        SetearBala(_color);
        switch (colorActual)
        {
            case MagiaColor.rojo:
                _dañoActual = _dañoActual * 2;
                _velocidadBonus = 1.8f;
                if (ControladorJuego.nivelRojo >= 4) StartCoroutine(MecanicaBalaRoja());
                break;
            case MagiaColor.azul:
                _dañoActual = _dañoActual / 2;
                _velocidadBonus = 1f;
                StartCoroutine(MecanicaBalaAzul());
                break;
            case MagiaColor.amarillo:
                numRebotadas = 0;
                _dañoActual = Mathf.RoundToInt(_dañoActual * 2.5f);
                _velocidadBonus = 3f;
                if (ControladorJuego.nivelAmarillo >= 2) numRebotes = ControladorJuego.nivelAmarillo;
                else numRebotes = 0;
                break;
            case MagiaColor.morado:
                //No pasa nada
                break;
            case MagiaColor.naranja:
                _dañoActual = _dañoActual / 2;
                _velocidadBonus = 0.5f + (ControladorJuego.nivelNaranja * 0.5f);
                StartCoroutine(MecanicaBalaNaranja());
                break;
            case MagiaColor.verde:
                _dañoActual = 1;
                _velocidadBonus = 1f + (ControladorJuego.nivelVerde * 0.1f);
                StartCoroutine(MecanicaBalaVerde());
                break;
        }
        SetearBala(_dañoActual, _velocidadBonus);
    }

    public void SetearBalaMoradaJugador(MagiaColor _color, float _valor, bool _derecha)
    {
        derecha = _derecha;
        int _dañoActual = ControladorJuego.jugadorAcceso.ataque;//A cacular
        SetearBala(1f);
        SetearBala(_color);
        StartCoroutine(MecanicaBalaMorada(_valor));
        SetearBala(_dañoActual);
    }


    public void Inicializador()
    {
        estrellar = rotar = forzarDestruccion = onda = inteligente = false;
        transform.SetParent(ControladorJuego.sistemaPool.transform);
        StopAllCoroutines();
        //StartCoroutine(Destruir());
    }

    #region Pool de Balas
    public void OcultarBala()
    {
        if (!forzarDestruccion)
        {
            if (colorActual == MagiaColor.rojo && ControladorJuego.nivelRojo >= 2)
                ControladorJuego.sistemaPool.MostrarFuego(transform.position, true);
            gameObject.SetActive(false);
            transform.position = Vector3.one * 10000f;
            ControladorJuego.sistemaPool.OcultarBalaJugador(gameObject);
        }
    }
    public void ForzarDestruccion()
    {
        if (colorActual == MagiaColor.rojo && ControladorJuego.nivelRojo >= 2)
            ControladorJuego.sistemaPool.MostrarFuego(transform.position, true);
        gameObject.SetActive(false);
        transform.position = Vector3.one * 10000f;
    }

    //No es necesario
    public IEnumerator Destruir()
    {
        if (colorActual == MagiaColor.morado || colorActual == MagiaColor.verde)
        {
            while (ControladorJuego.jugadorAcceso.magiaColor == MagiaColor.morado)
                yield return new WaitForSeconds(20f);
        }


        yield return new WaitForSeconds(20f);
        if (!estrellar)
            OcultarBala();
    }
    #endregion

    #region Colision Balas
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.tag == "Enemigo" && !other.GetComponent<BoxCollider2D>().isTrigger)
        {
            ColisionEnemigo(other.GetComponent<Enemigo>(), colorActual, damage);
            if (colorActual == MagiaColor.amarillo && ControladorJuego.nivelAmarillo >= 2 && numRebotadas < numRebotes) RebotarEnemigo();
            else OcultarBala();
        }
        else if (other.tag == "Pared")
        {
            if (colorActual == MagiaColor.amarillo && ControladorJuego.nivelAmarillo >= 2 && numRebotadas < numRebotes)
            {
                Rebotar(other.transform.localPosition, Mathf.RoundToInt(transform.eulerAngles.z));
            }
            else
            {
                Estrellar();
                OcultarBala();
            }
        }
        else if (other.transform.tag == "Escudo")
        {
            Estrellar();
            OcultarBala();
            other.GetComponent<Escudo>().ColisionBala(transform);
        }
        else if (other.transform.tag == "Jugador")
        {
            if (colorActual == MagiaColor.morado && rotar)
            {
                transform.SetParent(ControladorJuego.sistemaPool.transform);
                Estrellar();
                OcultarBala();
            }
        }
        else if (colorActual == MagiaColor.morado && other.transform.tag == "BalaEnemiga" && (other.transform.GetComponent<Proyectil>().colorActual == MagiaColor.morado || other.transform.GetComponent<Proyectil>().colorActual == MagiaColor.blanco))
        {
            Estrellar();
            OcultarBala();
        }

    }
    #endregion


    public IEnumerator MecanicaBalaRoja()
    {
        yield return new WaitForSeconds(0.2f);

        while (true)
        {
            ControladorJuego.sistemaPool.MostrarFuego(transform.position, true);
            yield return new WaitForSeconds(0.1f);
        }
    }

    public IEnumerator MecanicaBalaAzul()
    {
        if (ControladorJuego.nivelAzul >= 2)
        {
            float factor = 2 - ((6 - ControladorJuego.nivelAzul) / 5);

            while (velocidadBonus >= 0)
            {
                yield return new WaitForSeconds(0.02f);
                SetearBala(velocidadBonus - (0.02f * factor));
            }
            SetearBala(0f);
            yield return new WaitForSeconds(0.5f);

            SetearBala(ControladorJuego.jugadorAcceso.ataque * 3, 5f);
        }
    }

    public IEnumerator MecanicaBalaMorada(float _valor)
    {
        SetearBala(valorAnillo = _valor * 0.85f);
        orbitar = true;
        transform.SetParent(ControladorJuego.jugadorAcceso.transform);

        //Debug.Log(velocidadBonus);
        while (velocidadBonus >= 0)
        {
            yield return new WaitForSeconds(0.02f);
            SetearBala(velocidadBonus - 0.02f);
        }
        SetearBala(0f);

        rotar = true;
    }
    public void OrbitarJugador()
    {
        LookAt2D(ControladorJuego.jugadorAcceso.transform, ref miTransform);
        if (orbitar)
        {
            transform.Translate(transform.up * Time.deltaTime * ControladorJuego.nivelMorado * (valorAnillo * 2.5f) * (derecha ? 1 : -1), Space.World);
            transform.Translate(Vector3.left * Time.deltaTime * ControladorJuego.nivelMorado / 4f);
        }
        else
        {
            transform.Translate(Vector3.left * Time.deltaTime * ControladorJuego.nivelMorado * 2);
        }

        if (Input.GetMouseButtonUp(1) || Input.GetMouseButtonDown(0))
        {
            transform.SetParent(ControladorJuego.sistemaPool.transform);
            orbitar = false;
        }


        if (!(ControladorJuego.jugadorAcceso.magiaColor == MagiaColor.morado ||
         ControladorJuego.jugadorAcceso.magiaColor == MagiaColor.rojo ||
          ControladorJuego.jugadorAcceso.magiaColor == MagiaColor.azul))
        {
            transform.Translate(Vector3.right * Time.deltaTime * ControladorJuego.nivelMorado * 10);
        }
    }

    public IEnumerator MecanicaBalaNaranja()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.02f);
            inteligente = ControladorJuego.sistemaOleadas.activo;
        }
    }


    public void MovimientoInteligente()
    {
        float _distancia = 4 + ControladorJuego.nivelNaranja;
        objetivo = null;
        foreach (GameObject _enemigos in ControladorJuego.sistemaOleadas.enemigosObjetivo)
        {
            if (Vector3.Distance(_enemigos.transform.position, transform.position) < _distancia)
            {
                _distancia = Vector3.Distance(_enemigos.transform.position, transform.position);
                objetivo = _enemigos.transform;
            }
        }
        if (objetivo) LookAt2D(objetivo, ref miTransform);
    }

    public IEnumerator MecanicaBalaVerde()
    {
        yield return new WaitForSeconds(Random.Range(0.1f, 0.75f));
        onda = true;
    }



}