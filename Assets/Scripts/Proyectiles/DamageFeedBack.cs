﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageFeedBack : MonoBehaviour
{

    public float speed;
    public float duracion;
    public TextMesh texto;
    public int orderInlayer;
    public bool movimineto = true;
    // Start is called before the first frame update
    void Start()
    {
        Inicializador();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.up * speed * Time.deltaTime * (movimineto ? 1 : 0.1f));
    }

    public void SetearFeedBack(int _valorAtaque, MagiaColor _color, bool _jugador)
    {
        if (_jugador) transform.SetParent(ControladorJuego.jugadorAcceso.transform);
        movimineto = !_jugador;
        texto.color = ControladorJuego.sistemaMagiaColor.ObtenerColor(_color);
        if (_valorAtaque >= 0)
        {
            texto.text = "- " + _valorAtaque;
        }
        else
        {
            _valorAtaque = Mathf.Abs(_valorAtaque);
            texto.text = "+ " + _valorAtaque;
        }
    }

    public void Inicializador()
    {
        GetComponent<Renderer>().sortingOrder = orderInlayer;
        StartCoroutine(Destruir());
    }

    public void OcultarFeedBack()
    {
        gameObject.SetActive(false);
        transform.position = Vector3.one * 10000f;
        ControladorJuego.sistemaPool.OcultarFeedBackAtaque(gameObject);
    }

    IEnumerator Destruir()
    {
        yield return new WaitForSeconds(duracion);
        OcultarFeedBack();
    }
}