using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class MenuPrincipal : MonoBehaviour
{
    public GameObject organizadorPrincipal;
    public GameObject organizadorJugar;
    public GameObject organizadorOpciones;
    public GameObject organizadorSlot;
    public GameObject confirmacionBorrado;

    public Text[] slots;
    public Text cargarSlot;


    public void BtnJugar()
    {
        organizadorPrincipal.SetActive(false);
        organizadorJugar.SetActive(true);

        for (int i = 0; i < 3; i++)
        {
            if (ControladorJuego.sistemaGuardado.VerificarSlot(i + 1)) slots[i].text = "Slot " + (i + 1);
            else slots[i].text = "Slot " + (i + 1) + " vacío";
        }
    }

    public void BtnOpciones()
    {
        organizadorPrincipal.SetActive(false);
        organizadorOpciones.SetActive(true);
    }

    public void BtnSlot(int _valor)
    {
        ControladorJuego.adminData.SetearSlot(_valor);
        if (ControladorJuego.sistemaGuardado.VerificarSlot(_valor)) cargarSlot.text = "Cargar";
        else cargarSlot.text = "Nuevo";

        organizadorJugar.SetActive(false);
        organizadorSlot.SetActive(true);
    }
    public void BtnComenzarPartida()
    {
        SceneManager.LoadScene(1);
    }

    public void BtnEliminarSlot()
    {
        organizadorSlot.SetActive(false);
        confirmacionBorrado.SetActive(true);
    }
    public void BtnConfirmarEliminadoSlot()
    {
        ControladorJuego.sistemaGuardado.BorrarSlot(ControladorJuego.sistemaGuardado.slot);
        BtnNegarEliminadoSlot();
    }

    public void BtnNegarEliminadoSlot()
    {
        confirmacionBorrado.SetActive(false);
        organizadorSlot.SetActive(true);
    }
    public void BtnEliminarData()
    {
        ControladorJuego.sistemaGuardado.BorrarTodosLosDatos();
    }

    public void BtnRegresarMenuPrincipal()
    {
        organizadorJugar.SetActive(false);
        organizadorSlot.SetActive(false);
        organizadorOpciones.SetActive(false);
        organizadorPrincipal.SetActive(true);
    }

    public void BtnRegresarJugar()
    {
        organizadorSlot.SetActive(false);
        organizadorJugar.SetActive(true);
    }

    public void BtnSalir()
    {
        Application.Quit();
    }
}

